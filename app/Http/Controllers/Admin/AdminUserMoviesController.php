<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\UserMovies;
use App\Http\Requests\CreateUserMoviesRequest;
use App\Http\Requests\UpdateUserMoviesRequest;
use Illuminate\Http\Request;

use App\User;

class AdminUserMoviesController extends Controller
{
    public function index(Request $request)
    {
        $usermovies = UserMovies::with("user")->whereNull('deleted_at')->get();

        return view('adminCustom.index', array('view_page_name' => 'movies/usermovies', ), compact('usermovies'));
    }

    /**
     * Show the form for creating a new usermovies
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $user = User::pluck("name", "id")->prepend('Please select', 0);


        return view('adminCustom.index', array('view_page_name' => 'movies/crud/usermoviescreate', ), compact("user"));
    }

    /**
     * Store a newly created usermovies in storage.
     *
     * @param CreateUserMoviesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateUserMoviesRequest $request)
    {
        $photo_default = ["photo" => "letmeshare/assets/images/no-image-available.png" ];
        $details = json_encode($photo_default);

        // default picture
        $request->request->add(['details' => $details]);

        UserMovies::create($request->all());

        return redirect()->route('admin.usermovies');
    }

    /**
     * Show the form for editing the specified usermovies.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $usermovies = UserMovies::find($id);

        $user = User::pluck('email', 'id')->prepend('Please select', 0);

        return view('adminCustom.index', array('view_page_name' => 'movies/crud/usermoviesedit', ), compact('usermovies', 'user'));
    }

    /**
     * Update the specified usermovies in storage.
     * @param UpdateUserMoviesRequest|Request $request
     *
     * @param  int  $id
     */
    public function update($id, UpdateUserMoviesRequest $request)
    {
        $usermovies = UserMovies::findOrFail($id);

        $usermovies->update($request->all());

        return redirect()->route('admin.usermovies');
    }

    /**
     * Remove the specified usermovies from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        UserMovies::destroy($id);

        return redirect()->route('admin.usermovies');
    }

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        $arr_to_del = explode(',', $request->get('toDelete'));
        if (isset($arr_to_del[0])) {
            foreach ($arr_to_del as $elemId) {
                UserMovies::destroy($elemId);
            }
        } else {
            //MovieCategories::whereNotNull('id')->delete();
        }

        return redirect()->route('admin.usermovies');
    }
}
