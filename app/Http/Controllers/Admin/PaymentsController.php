<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Payment;
use Illuminate\Http\Request;

class PaymentsController extends Controller {

	/**
	 * Display a listing of payments
	 *
	 * @param Request $request
	 *
	 * @return \Illuminate\View\View
	 */
	public function index(Request $request)
	{
		$payments = Payment::all();

		$overall_total = 0;
		foreach ($payments as $payment) {
			if ((string)$payment->status === 'Completed')
				$overall_total += $payment->total;
		}

        return view('adminCustom.index', array('view_page_name' => 'payments/index', ), compact('payments', 'overall_total'));
	}

}