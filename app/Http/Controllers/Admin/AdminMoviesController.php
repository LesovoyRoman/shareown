<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\MovieCategories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateMovieCategoriesRequest;
use App\Http\Requests\UpdateMovieCategoriesRequest;

class AdminMoviesController extends Controller
{
    public function index(Request $request)
    {
        $moviecategories = MovieCategories::all();

        return view('adminCustom.index', array('view_page_name' => 'movies/movies', ), compact('moviecategories'));
    }

    /**
     * Show the form for creating a new moviecategories
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $moviecategories = MovieCategories::pluck("name", "id")->prepend('Please select', 0);

        return view('adminCustom.index', array('view_page_name' => 'movies/crud/moviescreate'), compact("moviecategories"));
    }

    /**
     * Store a newly created moviecategories in storage.
     *
     * @param CreateMovieCategoriesRequest|Request $request
     */
    public function store(CreateMovieCategoriesRequest $request)
    {
        MovieCategories::create($request->all());

        return redirect()->route('admin.movies');
    }

    public function edit($id)
    {
        $moviecategories = MovieCategories::find($id);


        return view('adminCustom.index', array('view_page_name' => 'movies/crud/moviesedit', ), compact('moviecategories'));
    }

    /**
     * Update the specified moviecategories in storage.
     * @param UpdateMovieCategoriesRequest|Request $request
     *
     * @param  int  $id
     */
    public function update($id, UpdateMovieCategoriesRequest $request)
    {
        $moviecategories = MovieCategories::findOrFail($id);

        $moviecategories->update($request->all());

        return redirect()->route('admin.movies');
    }

    /**
     * Remove the specified moviecategories from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        MovieCategories::destroy($id);

        return redirect()->route('admin.movies');
    }

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        $arr_to_del = explode(',', $request->get('toDelete'));
        if (isset($arr_to_del[0])) {
            foreach ($arr_to_del as $elemId) {
                MovieCategories::destroy($elemId);
            }
        } else {
            //MovieCategories::whereNotNull('id')->delete();
        }

        return redirect()->route('admin.movies');
    }
}
