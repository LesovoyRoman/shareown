<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class PaymentMethodsController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
        return view('adminCustom.index', array('view_page_name' => 'paymentmethods/index'));
	}

}