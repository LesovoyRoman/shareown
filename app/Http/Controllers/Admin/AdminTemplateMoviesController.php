<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\MovieCategories;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Redirect;
use Schema;
use App\TemplateMovies;
use App\Http\Requests\CreateTemplateMoviesRequest;
use App\Http\Requests\UpdateTemplateMoviesRequest;
use Illuminate\Http\Request;


class AdminTemplateMoviesController extends Controller
{
    public function index(Request $request)
    {
        $templatemovies = TemplateMovies::all();
        $to_push_categories = [];

        //4to tyt
        foreach ($templatemovies as $templatemovie) {
            if (isset($templatemovie->moviecategory)) {
                $str_categories = '';
                foreach ($templatemovie->moviecategory as $currmoviecategory) {
                    $associations = [$templatemovie->id => $currmoviecategory->name];
                    array_push($to_push_categories, $associations);
                    $str_categories .= $currmoviecategory->name . ', ';
                }
                $templatemovie->setAttribute('movie_category_name', substr($str_categories, 0, -2));
            } else {
                $templatemovie->setAttribute('movie_category_name', '-');
            }
        }

        return view('adminCustom.index', array('view_page_name' => 'movies/templatemovies', ), compact('templatemovies', 'to_push_categories'));
    }

    public function create()
    {
        $movie_categories = MovieCategories::pluck("name", "id");

        return view('adminCustom.index', array('view_page_name' => 'movies/crud/templatemoviescreate'), compact("movie_categories"));
    }

    /**
     * Store a newly created templatemovies in storage.
     *
     * @param CreateTemplateMoviesRequest|Request $request
     */
    public function store(CreateTemplateMoviesRequest $request)
    {
        $data = TemplateMovies::create($request->all());
        for($i=1; $i <= $request['number_categories']; $i++) {
            if (isset($request['current_category_' . $i]) && !empty($request['current_category_' . $i])) {
                DB::table('movie_categories_template_movies')
                    ->insert(['movie_categories_id' => $request['current_category_' . $i], 'template_movies_id' => $data->id]);
            }
        }

        return redirect()->route('admin.templatemovies');
    }

    /**
     * Show the form for editing the specified templatemovies.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $movie_categories = MovieCategories::pluck("name", "id");

        $movie_category = [];
        $arr_ids = [];
        $templatemovies = TemplateMovies::find($id);
        foreach ($templatemovies->moviecategory as $currmoviecategory) {
            array_push($arr_ids, $currmoviecategory->id);
        }

        return view('adminCustom.index', array('view_page_name' => 'movies/crud/templatemoviesedit', ), compact('templatemovies', 'movie_category', 'movie_categories', 'arr_ids'));
    }

    /**
     * Update the specified templatemovies in storage.
     * @param UpdateTemplateMoviesRequest|Request $request
     *
     * @param  int  $id
     */
    public function update($id, UpdateTemplateMoviesRequest $request)
    {
        $templatemovies = TemplateMovies::findOrFail($id);
        $templatemovies->update($request->all());

        // delete
        DB::table('movie_categories_template_movies')
            ->where('template_movies_id', $id)->delete();
        for($i=1; $i <= $request['number_categories']; $i++){
            if(isset($request['current_category_' . $i]) && !empty($request['current_category_' . $i])) {
                // insert
                DB::table('movie_categories_template_movies')
                    ->insert(['movie_categories_id' => $request['current_category_' . $i], 'template_movies_id' => $id]);
            }
        }

        return redirect()->route('admin.templatemovies');
    }

    /**
     * Remove the specified templatemovies from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        TemplateMovies::destroy($id);

        return redirect()->route('admin.templatemovies');
    }

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        $arr_to_del = explode(',', $request->get('toDelete'));
        if (isset($arr_to_del[0])) {
            foreach ($arr_to_del as $elemId) {
                TemplateMovies::destroy($elemId);
            }
        } else {
            //TemplateMovies::whereNotNull('id')->delete();
        }

        return redirect()->route('admin.templatemovies');
    }
}
