<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Payment;
use App\User;
use App\UserMovies;
use App\TemplateMovies;

class AdminController extends Controller
{
    function __construct()
    {
    }

    public function index(Request $request)
    {
        $payments = Payment::all();

        $usermoviesCount = UserMovies::with("user")->whereNull('deleted_at')->get()->count();

        $templatemoviesCount = TemplateMovies::all()->count();

        $usersCount = User::all()->count();

        $overall_total = 0;
        // count entire amount of money that users paid ever
        foreach ($payments as $payment) {
            if ((string)$payment->status === 'Completed')
                $overall_total += $payment->total;
        }

        return view('adminCustom.index', array('view_page_name' => 'dashboard'), compact('payments', 'overall_total', 'usermoviesCount', 'templatemoviesCount', 'usersCount'));

    }
}