<?php

namespace App\Http\Controllers;

use App\Payment;
use App\TemplateMovies;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Srmklive\PayPal\Services\ExpressCheckout;
use Closure;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class PaypalController extends Controller
{
	/** @var ExpressCheckout */
	protected $provider;

	/**
	 * PaypalController constructor.
	 */
	public function __construct()
	{
		$this->middleware('auth');
		$this->provider = new ExpressCheckout();
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 * @throws \Exception
	 */
	public function expressCheckout(Request $request)
	{

		/**
		 * Template movie id.
		 */
        $data = Session::get('data_create');
        $template_movie_id = $data['template_movie_id'];

		if (!$template_movie_id) {
			Log::error('Unable to get template movie id!');
			return redirect('/#create-movie')->with(['code' => 'danger', 'message' => 'Пожалуйста, выберите фильм ']);
		}

		/**
		 * Get the cart data.
		 */
		$cart = $this->getCart($template_movie_id);

		/**
		 * Create new payment instance.
		 */
		$payment = new Payment();
		$payment->user_id = $request->user()->id;
		$payment->description = $cart['invoice_description'];
		$payment->total = $cart['total'];
		$payment->template_movie_id = $template_movie_id;
		$payment->save();

		/**
		 * Store invoice (payment) id in cart data in order to process it further.
		 */
		$cart['invoice_id'] = config('paypal.invoice_prefix') . '_' . $payment->id;

		/**
		 * Send a request to PayPal.
		 * PayPal should respond with an array of data, it should contain a link to PayPal's payment system.
		 */
		$response = $this->provider->setExpressCheckout($cart);

		/**
		 * If there is no link redirect back with error message
		 */
		if (!$response['paypal_link']) {
			Log::error('PayPal incorrect response: ' . print_r($response, true));
			return redirect('/#create-movie')->with(['code' => 'danger', 'message' => 'Возникла ошибка при оплате через PayPal!']);
		}

		/**
		 * Redirect to PayPal.
		 * After payment is done, PayPal will redirect us back to $this->expressCheckoutSuccess().
		 */

        return redirect()->to($response['paypal_link']);
	}

	/**
	 * Processes response from PayPal after user confirms the payment.
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \Exception
	 */
	public function expressCheckoutSuccess(Request $request)
	{
		$token = $request->get('token');
		$PayerID = $request->get('PayerID');
		/**
		 * Get payment details, because PayPal initially returns only token.
		 */
		$response = $this->provider->getExpressCheckoutDetails($token);

		/**
		 * If response ACK value is not SUCCESS or SUCCESSWITHWARNING, redirect back with error.
		 */
		if (!in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
			Log::error('Response ACK is not SUCCESS or SUCCESSWITHWARNING: ' . print_r($response, true));
			return redirect('/#create-movie')->with(['code' => 'danger', 'message' => 'Возникла ошибка при оплате через PayPal!']);
		}

		/**
		 * Get payment id.
		 */
		$payment_id = explode('_', $response['INVNUM'])[1];

		$payment = Payment::find($payment_id);

		// get cart data
		$cart = $this->getCart($payment->template_movie_id);
		$cart['invoice_id'] = $payment_id;

		/**
		 * Perform transaction on PayPal and get the payment status.
		 */
		$payment_status = $this->provider->doExpressCheckoutPayment($cart, $token, $PayerID);
		$status = isset($payment_status['PAYMENTINFO_0_PAYMENTSTATUS']) ? $payment_status['PAYMENTINFO_0_PAYMENTSTATUS'] : NULL;

		if (!$status) {
			Log::error('PAYMENTINFO_0_PAYMENTSTATUS is not returned: ' . print_r($payment_status, true));
			return redirect('/#create-movie')->with(['code' => 'danger', 'message' => 'Возникла ошибка при оплате через PayPal!']);
		}

		$payment->status = $status;
		$payment->save();

		if (!$payment->complete)
			return redirect('/#create-movie')->with(['code' => 'danger', 'message' => 'Возникла ошибка при оплате через PayPal! Пожалуйста, свяжитесь с администратором, чтобы подробнее узнать о проблеме.']);

		$request->session()->put('template_movies.'.$payment->template_movie_id.'.payment_confirmed', true);

        $createMovie = new MoviesController();
        return $createMovie->processing($request);
        /* @todo not sure */
	}

	private function getCart($template_movie_id)
	{
		/**
		 * Retrieve template movie data.
		 */
		$template_movie = TemplateMovies::find($template_movie_id);

		return [
			'items' => [
				[
					'name' => $template_movie->title,
					'price' => $template_movie->price,
					'qty' => 1
				]
			],
			'return_url' => url('/paypal/express-checkout-success'),
			'invoice_description' => "Film creation",
			'cancel_url' => url('/#create-movie'),
			'total' => $template_movie->price, // = price * quantity
		];
	}
}
