<?php

namespace App\Http\Controllers;

use App\Payment;
use App\TemplateMovies;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\MovieCategories;

class HomeController extends Controller
{
    public function __construct()
    {

    }

    public function index(Request $request)
    {
        $data['template_movies'] = [];
        $template_movies = TemplateMovies::all();

        // get movies which were paid
        foreach ($template_movies as $template_movie) {
            // @todo letmeshare: improve detection method whether user paid for a movie or not. consider middleware
            if ($request->user()) {
                $pending_user_payment = Payment::where([
                    'user_id' => $request->user()->id,
                    'template_movie_id' => $template_movie['id'],
                    'status' => 'Pending'
                ])->first();

                // Storing flag that indicates that payment is confirmed
                if ($pending_user_payment)
                    $request->session()->put('template_movies.'.$template_movie['id'].'.payment_confirmed', true);
            }
            $template_movie['categories'] = [];
            $temp_arr_cats = [];
            foreach ($template_movie->moviecategory as $currmoviecategory) {
                array_push($temp_arr_cats, $currmoviecategory->id);
            }


            $template_movie['categories'] = $temp_arr_cats;

            $data['template_movies'][] = [
                'movie_id' => $template_movie['id'],
                'title' => $template_movie['title'],
                'description' => $template_movie['description'],
                'path_image' => $template_movie['path_image'],
                'link_demo' => $template_movie['link_demo'],
                'price' => $template_movie['price'],
                'categories' => $temp_arr_cats
            ];
        }

        $movie_categories = MovieCategories::pluck("name", "id");

        return view('letmeshare.home', $data, compact('movie_categories'));
    }
}
