<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ShareMovieMail;
use App\TemplateMovies;
use App\UserMovies;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Mockery\Exception;
use Illuminate\Support\Facades\Session;
use \YandexMoney\API;

class MoviesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = DB::table('usermovies')->whereNull('deleted_at')->where('user_id', Auth::id())->get(); // gets current user row

        foreach ($movies as $movie) {
            $category_name = DB::table('moviecategories')->where('id', $movie->movie_category_id)->value('name') ?: 'All';

            $movie->category_name = $category_name;

            if (!empty($movie->details)) {
                $movie->details = json_decode($movie->details);
            }
        }

        // get first film of current user
        $first_film = DB::table('usermovies')->where('user_id', Auth::id())->whereNull('deleted_at')->orderBy('id', 'ASC')->value('link_youtube');

        // set up link to watch movie from youtube
        if(isset($first_film) && !empty($first_film)) {

            preg_match('/(http:\/\/|https:\/\/|)?(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(&\S+)?/', $first_film, $matches, PREG_OFFSET_CAPTURE);
            $data['first_film_link_parts'] = $matches;
            $first_film = trim($data['first_film_link_parts'][6][0]);

            return view('letmeshare.movie.index', compact('movies', 'first_film'));
        } else {
            return view('letmeshare.movie.index', compact('movies'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!$request->input('template'))
            return redirect('/#create-movie');

        $data = [];

        $template_movie_id = $request->input('template');
        $template_movie = TemplateMovies::find($template_movie_id);

        // preparing for creating film and return view
        $data['movie_price'] = $template_movie->price;
        $data['template_movie_id'] = $template_movie_id;
        $data['movie_category_id'] = DB::table('templatemovies')
            ->where('id', $data['template_movie_id'])->value('movie_category_id');
        $data['link_demo'] = DB::table('templatemovies')->where('id', $template_movie_id)->value('link_demo');

        preg_match('/(http:\/\/|https:\/\/|)?(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(&\S+)?/',
            $data['link_demo'], $matches, PREG_OFFSET_CAPTURE);
        $data['group_movie_parts_links'] = $matches;
        $data['link_demo'] = trim($data['group_movie_parts_links'][6][0]);

        return view('letmeshare.movie.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        // if not set logo of image
        if(!isset($data['details']['logo']) && empty($data['details']['logo'])) {
            $data['details']['logo'] = env('CREATE_MOVIE_NO_IMAGE_PATH');
            $data['details']['photo'] = env('CREATE_MOVIE_NO_IMAGE_PATH');
        } else {
            $data['details']['photo'] = $data['details']['logo'];
        }

        if(!isset($data['link_youtube']) || empty($data['link_youtube'])) {
            $data['link_youtube'] = '';
        }

        $movie = UserMovies::create([
            'user_id'             => $data['user_id'],
            'template_movie_id'   => $data['template_movie_id'],
            'title'             => 'title_video_' . $data['user_id'] . $data['template_movie_id'],
            'link_youtube'      => $data['link_youtube'],
            'details'             => json_encode($data['details'])
        ]);

        return redirect('/movies');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $movie_data = UserMovies::find($id);

        // Prepare YouTube link to be shown in frame
        if($movie_data->link_youtube) {
            $link_parts = explode('?v=', $movie_data->link_youtube);

            if (!empty($link_parts[1]))
                $movie_data->link_youtube = 'https://youtube.com/embed/' . $link_parts[1] . '?rel=0&showinfo=0&autoplay=0&loop=0';
        } else {
            return redirect('/');
        }

        // Paid movie was created, so unset payment confirmation flag to forbid repeated usage of payment
        if (session()->get('redirect_to_ready_view'))
            session()->forget('template_movies.' . $movie_data->template_movie_id . '.payment_confirmed');

        return view(session()->get('redirect_to_ready_view') ? 'letmesharemovie.ready' : 'letmeshare.movie.show',
                compact('movie_data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function yandexSuccess(Request $request)
    {
        // yandex credentials
        $client_id = env('YANDEX_CLIENT_ID');
        $redirect_uri = env('REDIRECT_URI_YANDEX_SUCCESS');

        $code = $_GET['code'];

        // get token for getting access token
        $access_token_response = API::getAccessToken($client_id, $code, $redirect_uri, $client_secret=NULL);
        if(property_exists($access_token_response, "error")) {
            // process error
            dump($access_token_response->error);
        }
        $access_token = $access_token_response->access_token;

        $api = new API($access_token);

        // @todo change wallet and amount
        // wallet
        $money_wallet = env('YANDEX_WALLET');
        // amount of money
        $amount_due = env('YANDEX_AMOUNT_MONEY');

        // make request payment
        $request_payment = $api->requestPayment(array(
            "pattern_id" => "p2p",
            "to" => $money_wallet,
            "amount_due" => $amount_due,
            "comment" => 'payment LimVideo comment',
            "message" => 'payment LimVideo message',
            "label" => 'payment LimVideo',
        ));

        // call process payment to finish payment
        $process_payment = $api->processPayment(array(
            "request_id" => $request_payment->request_id,
        ));

        if($process_payment->status == "success") {
            // success payment
            Session::put('template_movies.'.Session::get('template_movie_id').'.payment_confirmed', true);
            return $this->processing($request);
        }

    }

    public function processing(Request $request)
    {
        $template_movie_id = $request->input('template_movie_id');
        $template_movie = TemplateMovies::find($template_movie_id);
        // set variant of payment type
        $idPayment = $request->input('paymentVariant');

        if(!empty(Session::get('data_create'))) {
            $form_data = Session::get('data_create');
        } else {
            $form_data = $request->input();
            $validation_rules = $this->getFormValidationRules($form_data['template_movie_id']);
            $request->validate($validation_rules);
        }

        if(!empty($template_movie_id)) {
            Session::put('template_movie_id', $template_movie_id);
        } else {
            $template_movie_id = Session::get('template_movie_id');
        }

        /*
         * $idPayment == 1 - it's PayPal
         * $idPayment == 2 - it's YandexMoney
         *
         * checking if film costs some money & not paid yet
         * */
        if (!empty($template_movie->price) && $template_movie->price > 0 && !$request->session()
                ->get('template_movies.' . $template_movie_id . '.payment_confirmed') && $idPayment == 1)
        {

            // paypal
            return redirect()->route('paypal.express-checkout', ['template_movie_id'])
                ->with( ['data_create' => $request->input()] );

        }
        else if($idPayment == 2 && !$request->session()
                ->get('template_movies.' . $template_movie_id . '.payment_confirmed') && !empty($template_movie->price))
        {
            // yandex
            $url = API::buildObtainTokenUrl(
                env('YANDEX_CLIENT_ID'),
                env('REDIRECT_URI_YANDEX_SUCCESS'),
                'account-info operation-history payment-p2p'
            );

            // give Session array of data from form
            Session::put('data_create', $request->input());
            return redirect()->away($url);

        } else {

            if(!empty(Session::get('data_create'))) {
                $form_data = Session::get('data_create');
            } else {
                $form_data = $request->input();
            }

            Session::forget('data_create');
            Session::forget('template_movies.'.Session::get('template_movie_id').'.payment_confirmed');

            // If template movie or its category not set, user should not gave reached this page
            if (empty($form_data['template_movie_id'])) {
                return redirect()->to('/');
            }

            // set logo as base64
            if (isset($form_data['imgBase64']) && !empty($form_data['imgBase64'])) {
                $details['logo'] = $this->storeBase64AsImage($form_data['imgBase64'], $request->user()->id);
            }

            // fill out array of data with data from form to use it after
            foreach ($form_data as $key => $data) {
                if (!empty($form_data[$key])) {
                    if (is_array($data)) {
                        foreach ($data as $key_arr => $company_data) {
                            $details[$key_arr] = $company_data;
                        }
                    } else {
                        $details[$key] = $form_data[$key];
                    }
                }
            }

            // checking if photos are set (movie 12)
            if (isset($form_data['photo1']) && !empty($form_data['photo1'])) {
                $details['photo_1'] = $this->storeBase64AsImage($form_data['photo1'], $request->user()->id);
            }

            if (isset($form_data['photo2']) && !empty($form_data['photo2'])) {
                $details['photo_2'] = $this->storeBase64AsImage($form_data['photo2'], $request->user()->id);
            }

            if (isset($form_data['photo3']) && !empty($form_data['photo3'])) {
                $details['photo_3'] = $this->storeBase64AsImage($form_data['photo3'], $request->user()->id);
            }

            // Specific movie details.
            $movie_data = [
                'user_id' => $request->user()->id,
                'template_movie_id' => $form_data['template_movie_id'],
                'movie_category_id' => 0, //$form_data['movie_category_id'],
                'link_youtube' => '', // will be filled after movie is uploaded to youtube
                //'title'             => $details['name'] . __('this film only for you'),
                'details' => $details
            ];

            return view('letmeshare.movie.processing', compact('movie_data'));
        }
        return null;
    }

    protected function getFormValidationRules($template_movie_id)
    {
        switch ($template_movie_id) {
            case 10:
                $validation_rules = [
                    'movie_' . $template_movie_id . '.' . 'company.name'          => 'required|max:40',
                    'movie_' . $template_movie_id . '.' . 'company.phone'         => 'required|max:25',
                    'movie_' . $template_movie_id . '.' . 'company.site'          => 'required|max:60',

                    'movie_' . $template_movie_id . '.' . 'cta'                   => 'required|max:60',
                    'movie_' . $template_movie_id . '.' . 'answer'                => 'required|max:40',
                    'movie_' . $template_movie_id . '.' . 'question'              => 'required|max:40',
                    'movie_' . $template_movie_id . '.' . 'first'                 => 'required|max:40',
                ];
                break;

            case 11:
                $validation_rules = [
                    'movie_' . $template_movie_id . '.' . 'company.name'          => 'required|max:40',
                    'movie_' . $template_movie_id . '.' . 'company.phone'         => 'required|max:25',
                    'movie_' . $template_movie_id . '.' . 'company.site'          => 'required|max:60',
                    'movie_' . $template_movie_id . '.' . 'company.sphere'        => 'required|max:60',

                    'movie_' . $template_movie_id . '.' . 'cta'                   => 'required|max:60',
                ];
                break;

            case 12:
                $validation_rules = [
                    'movie_' . $template_movie_id . '.' . 'company.name'          => 'required|max:40',
                    'movie_' . $template_movie_id . '.' . 'company.phone'         => 'required|max:25',
                    'movie_' . $template_movie_id . '.' . 'company.site'          => 'required|max:60',
                    'movie_' . $template_movie_id . '.' . 'company.sphere'        => 'required|max:40',

                    'movie_' . $template_movie_id . '.' . 'slogan_1'              => 'required|max:50',
                    'movie_' . $template_movie_id . '.' . 'slogan_2'              => 'required|max:50',
                    'movie_' . $template_movie_id . '.' . 'slogan_3'              => 'required|max:50',
                    'movie_' . $template_movie_id . '.' . 'slogan_4'              => 'required|max:50',
                    'movie_' . $template_movie_id . '.' . 'slogan_5'              => 'required|max:50',
                    'movie_' . $template_movie_id . '.' . 'slogan_6'              => 'required|max:50',
                    'movie_' . $template_movie_id . '.' . 'slogan_7'              => 'required|max:50',
                    'movie_' . $template_movie_id . '.' . 'advantage_1'           => 'required|max:50',
                    'movie_' . $template_movie_id . '.' . 'advantage_2'           => 'required|max:50',
                    'movie_' . $template_movie_id . '.' . 'advantage_3'           => 'required|max:50',
                    'movie_' . $template_movie_id . '.' . 'advantage_4'           => 'required|max:50',
                    'movie_' . $template_movie_id . '.' . 'advantage_5'           => 'required|max:50',

                    'movie_' . $template_movie_id . '.' . 'cta'                   => 'required|max:60',
                ];
                break;

            default:
                $validation_rules = [
                    'movie_' . $template_movie_id . '.' . 'company.name'          => 'required|max:40',
                    'movie_' . $template_movie_id . '.' . 'company.phone'         => 'required|max:25',
                    'movie_' . $template_movie_id . '.' . 'company.site'          => 'required|max:60',

                    'movie_' . $template_movie_id . '.' . 'cta'                   => 'required|max:60',
                    'movie_' . $template_movie_id . '.' . 'answer'                => 'required|max:40',
                    'movie_' . $template_movie_id . '.' . 'question'              => 'required|max:40',
                ];
                break;
        }

        return $validation_rules;
    }

    private function storeBase64AsImage($img_base64, $user_id)
    {
        // @todo make possibility to upload different types
        $base64_photo = str_replace('data:image/jpeg;base64,', '', $img_base64);
        $base64_photo = str_replace('data:image/png;base64,', '', $base64_photo);
        $base64_photo = str_replace('data:image/jpg;base64,', '', $base64_photo);
        $base64_photo = str_replace(' ', '+', $base64_photo);
        $photo_name = str_random(40) . '.' . 'png';
        Storage::disk('photos')->put($user_id . '/' . $photo_name, base64_decode($base64_photo));

        return url('storage/app/photos/' . $user_id . '/' . $photo_name);
    }

//    TODO it's not used, because have no calls of this function (maybe make it used later)
    public function jxShareMovieViaEmail(Requests\ShareMovieViaEmailRequest $request)
    {
        $form_data = $request->all();

        try {
            Mail::to($request->email)->send(new ShareMovieMail([
                'movie_url' => url('movies/' . $form_data['movie_id'] . '/show')
            ]));

            $response = [
                'status'    => 200,
                'msg'       => 'Letter successfully sent!'
            ];
        } catch (Exception $e) {
            $response = [
                'status'    => 204,
                'msg'       => 'Check form for mistakes, please!'
            ];

            throw $e;
        }

        return response()->json($response);
    }


    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
