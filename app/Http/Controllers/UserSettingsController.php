<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserSettingsController extends Controller
{
	use ResetsPasswords;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request, $token = null)
	{
		return view('letmeshare.settings.index')->with(
			['user' => Auth::user(), 'email' => Auth::user()->email]
		);
	}

	public function update(Request $request, $id)
	{
		// @todo: add fields validation
		$user = User::findOrFail($id);
		$input = $request->all();
		$input['password'] = Hash::make($input['password']);
		$user->update($input);

		return redirect()->route('settings.index')->withMessage(__('Новые настройки успешно сохранены!'));
	}
}
