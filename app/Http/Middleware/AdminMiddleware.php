<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;

class AdminMiddleware {

    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->type == 'admin') {
            return $next($request);
        }

        return redirect('/');
    }

}
