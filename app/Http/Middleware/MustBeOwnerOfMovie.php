<?php

namespace App\Http\Middleware;

use Closure;

class MustBeOwnerOfMovie
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		// For example, current URL is: /movies/1/edit
		$id = $this->routes('movies');

		// Fetch the movie
		$movie = \App\UserMovies::findOrFail($id);

		if ($movie->user_id == $this->auth()->id) {
			// User is owner, continue
			return $next($request);
		}

		return redirect()->to('/');
    }
}
