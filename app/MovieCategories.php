<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MovieCategories extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'moviecategories';

    protected $fillable = ['name'];

    public static function boot()
    {
        parent::boot();
    }
}
