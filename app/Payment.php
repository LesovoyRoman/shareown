<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';

    protected $fillable = ['user_id', 'description', 'total', 'status', 'template_movie_id'];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function getCompleteAttribute()
    {
        return ($this->status && (string)$this->status === 'Completed') ? true : false;
    }
}
