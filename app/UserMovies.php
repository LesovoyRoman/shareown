<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class UserMovies extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'usermovies';

    protected $fillable = [
        'user_id',
        'template_movie_id',
        'movie_category_id',
        'title',
        'link_youtube',
        'details'
    ];

    public static function boot()
    {
        parent::boot();
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function moviecategory()
    {
        return $this->hasOne('App\MovieCategories', 'id', 'movie_category_id');
    }

    public function templatemovie()
    {
        return $this->hasOne('App\TemplateMovies', 'id', 'template_movie_id');
    }
}
