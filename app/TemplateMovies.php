<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TemplateMovies extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'templatemovies';

    protected $fillable = [
        'title',
        'description',
        'path_image',
        'link_demo',
        'price',
        'movie_category_id'
    ];

    public static function boot()
    {
        parent::boot();
    }

    public function moviecategory()
    {
        return $this->belongsToMany('App\MovieCategories', 'movie_categories_template_movies', 'template_movies_id', 'movie_categories_id');
    }
}
