<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::group(['middleware' => 'admin'], function () {
    Route::get('admin', 'Admin\AdminController@index');

    Route::get('admin/movies', 'Admin\AdminMoviesController@index')->name('admin.movies');
    Route::any('admin/movies/edit/{id}', 'Admin\AdminMoviesController@edit')->name('moviesedit');
    Route::any('admin/movies/create', 'Admin\AdminMoviesController@create')->name('moviescreate');
    Route::any('admin/movies/store', 'Admin\AdminMoviesController@store')->name('moviesstore');
    Route::any('admin/movies/destroy/{id}', ['as' => 'moviesdestroy', 'uses' => 'Admin\AdminMoviesController@destroy']);
    Route::any('admin/movies/update/{id}', ['as' => 'moviesupdate', 'uses' => 'Admin\AdminMoviesController@update']);
    Route::any('admin/movies/massDelete', ['as' => 'moviesmassdelete', 'uses' => 'Admin\AdminMoviesController@massDelete']);

    Route::get('admin/templatemovies', 'Admin\AdminTemplateMoviesController@index')->name('admin.templatemovies');
    Route::any('admin/templatemovies/edit/{id}', 'Admin\AdminTemplateMoviesController@edit')->name('templatemoviesedit');
    Route::any('admin/templatemovies/create', 'Admin\AdminTemplateMoviesController@create')->name('templatemoviescreate');
    Route::any('admin/templatemovies/store', 'Admin\AdminTemplateMoviesController@store')->name('templatemoviesstore');
    Route::any('admin/templatemovies/destroy/{id}', ['as' => 'templatemoviesdestroy', 'uses' => 'Admin\AdminTemplateMoviesController@destroy']);
    Route::any('admin/templatemovies/update/{id}', ['as' => 'templatemoviesupdate', 'uses' => 'Admin\AdminTemplateMoviesController@update']);
    Route::any('admin/templatemovies/massDelete', ['as' => 'templatemoviesmassdelete', 'uses' => 'Admin\AdminTemplateMoviesController@massDelete']);

    Route::get('admin/usermovies', 'Admin\AdminUserMoviesController@index')->name('admin.usermovies');
    Route::any('admin/usermovies/edit/{id}', 'Admin\AdminUserMoviesController@edit')->name('usermoviesedit');
    Route::any('admin/usermovies/create', 'Admin\AdminUserMoviesController@create')->name('usermoviescreate');
    Route::any('admin/usermovies/store', 'Admin\AdminUserMoviesController@store')->name('usermoviesstore');
    Route::any('admin/usermovies/destroy/{id}', ['as' => 'usermoviesdestroy', 'uses' => 'Admin\AdminUserMoviesController@destroy']);
    Route::any('admin/usermovies/update/{id}', ['as' => 'usermoviesupdate', 'uses' => 'Admin\AdminUserMoviesController@update']);
    Route::any('admin/usermovies/massDelete', ['as' => 'usermoviesmassDelete', 'uses' => 'Admin\AdminUserMoviesController@massDelete']);

    Route::any('admin/payments', 'Admin\PaymentsController@index')->name('admin.payments');
    Route::any('admin/users', 'Admin\UsersController@index')->name('admin.users');
});


Route::get('/settings', ['as' => 'settings.index', 'uses' => 'UserSettingsController@index']);
Route::post('/settings/update/{id}', ['as' => 'settings.update', 'uses' => 'UserSettingsController@update']);

// Movies
Route::get('/movies', 'MoviesController@index');
Route::get('/movies/create', 'MoviesController@create');
Route::post('/movies/store', ['as' => 'movies.store', 'uses' => 'MoviesController@store']);
Route::post('/movies/processing', ['as' => 'movies.processing', 'uses' => 'MoviesController@processing']);
Route::get('/movies/processing', function () { return redirect('/'); });

// @todo isn't used, because has no appropriate place to do it, later maybe will be changed
Route::post('/movies/shareViaEmail', ['as' => 'movies.shareViaEmail', 'uses' => 'MoviesController@jxShareMovieViaEmail']);

// Privacy policy
Route::view('/privacy', 'letmeshare.privacy');

// @todo letmeshare: use filesystems
// Images
Route::get('/storage/app/photos/{user_id}/{filename}', function ($user_id, $filename) {
    $path = storage_path() . '/app/photos/' . $user_id . '/' . $filename;

    if(!File::exists($path)) {
        return response()->json(['message' => 'Image not found.'], 404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

// Videos
Route::get('/letmeshare/videos/{filename}', function ($filename) {
    $path = public_path() . '/letmeshare/videos/' . $filename;

    if(!File::exists($path)) {
        return response()->json(['message' => 'Video not found.'], 404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

// PayPal
Route::get('paypal/express-checkout', 'PaypalController@expressCheckout')->name('paypal.express-checkout');
Route::get('paypal/express-checkout-success', 'PaypalController@expressCheckoutSuccess');
Route::post('paypal/notify', 'PaypalController@notify');


// Yandex
Route::get('/yandex-success', 'MoviesController@yandexSuccess')->name('yandex.success');