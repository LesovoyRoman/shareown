(function($) {

    var isBuilder = $('html').hasClass('is-builder');

    $.extend($.easing, {
        easeInOutCubic: function(x, t, b, c, d) {
            if ((t /= d / 2) < 1) return c / 2 * t * t * t + b;
            return c / 2 * ((t -= 2) * t * t + 2) + b;
        }
    });

    $.fn.outerFind = function(selector) {
        return this.find(selector).addBack(selector);
    };

    $.fn.footerReveal = function() {
        var $this = $(this);
        var $prev = $this.prev();
        var $win = $(window);

        function initReveal() {
            if ($this.outerHeight() <= $win.outerHeight()) {
                $this.css({
                    'z-index': -999,
                    position: 'fixed',
                    bottom: 0
                });

                $this.css({
                    'width': $prev.outerWidth()
                });

                $prev.css({
                    'margin-bottom': $this.outerHeight()
                });
            } else {
                $this.css({
                    'z-index': '',
                    position: '',
                    bottom: ''
                });

                $this.css({
                    'width': ''
                });

                $prev.css({
                    'margin-bottom': ''
                });
            }
        }

        initReveal();

        $win.on('load resize', function() {
            initReveal();
        });

        return this;
    };

    (function($, sr) {
        // debouncing function from John Hann
        // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
        var debounce = function(func, threshold, execAsap) {
            var timeout;

            return function debounced() {
                var obj = this,
                    args = arguments;

                function delayed() {
                    if (!execAsap) func.apply(obj, args);
                    timeout = null;
                }

                if (timeout) clearTimeout(timeout);
                else if (execAsap) func.apply(obj, args);

                timeout = setTimeout(delayed, threshold || 100);
            };
        };
        // smartresize
        jQuery.fn[sr] = function(fn) {
            return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr);
        };

    })(jQuery, 'smartresize');

    (function() {

        var scrollbarWidth = 0,
            originalMargin, touchHandler = function(event) {
                event.preventDefault();
            };

        function getScrollbarWidth() {
            if (scrollbarWidth) return scrollbarWidth;
            var scrollDiv = document.createElement('div');
            $.each({
                top: '-9999px',
                width: '50px',
                height: '50px',
                overflow: 'scroll',
                position: 'absolute'
            }, function(property, value) {
                scrollDiv.style[property] = value;
            });
            $('body').append(scrollDiv);
            scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
            $('body')[0].removeChild(scrollDiv);
            return scrollbarWidth;
        }

    })();

    $.isMobile = function(type) {
        var reg = [];
        var any = {
            blackberry: 'BlackBerry',
            android: 'Android',
            windows: 'IEMobile',
            opera: 'Opera Mini',
            ios: 'iPhone|iPad|iPod'
        };
        type = 'undefined' == $.type(type) ? '*' : type.toLowerCase();
        if ('*' == type) reg = $.map(any, function(v) {
            return v;
        });
        else if (type in any) reg.push(any[type]);
        return !!(reg.length && navigator.userAgent.match(new RegExp(reg.join('|'), 'i')));
    };

    var isSupportViewportUnits = (function() {
        // modernizr implementation
        var $elem = $('<div style="height: 50vh; position: absolute; top: -1000px; left: -1000px;">').appendTo('body');
        var elem = $elem[0];
        var height = parseInt(window.innerHeight / 2, 10);
        var compStyle = parseInt((window.getComputedStyle ? getComputedStyle(elem, null) : elem.currentStyle)['height'], 10);
        $elem.remove();
        return compStyle == height;
    }());

    $(function() {

        $('html').addClass($.isMobile() ? 'mobile' : 'desktop');

        // .mbr-navbar--sticky
        $(window).scroll(function() {
            $('.mbr-navbar--sticky').each(function() {
                var method = $(window).scrollTop() > 10 ? 'addClass' : 'removeClass';
                $(this)[method]('mbr-navbar--stuck')
                    .not('.mbr-navbar--open')[method]('mbr-navbar--short');
            });
        });

        if ($.isMobile() && navigator.userAgent.match(/Chrome/i)) { // simple fix for Chrome's scrolling
            (function(width, height) {
                var deviceSize = [width, width];
                deviceSize[height > width ? 0 : 1] = height;
                $(window).smartresize(function() {
                    var windowHeight = $(window).height();
                    if ($.inArray(windowHeight, deviceSize) < 0)
                        windowHeight = deviceSize[$(window).width() > windowHeight ? 1 : 0];
                    $('.mbr-section--full-height').css('height', windowHeight + 'px');
                });
            })($(window).width(), $(window).height());
        } else if (!isSupportViewportUnits) { // fallback for .mbr-section--full-height
            $(window).smartresize(function() {
                $('.mbr-section--full-height').css('height', $(window).height() + 'px');
            });
            $(document).on('add.cards', function(event) {
                if ($('html').hasClass('mbr-site-loaded') && $(event.target).outerFind('.mbr-section--full-height').length)
                    $(window).resize();
            });
        }

        // .mbr-section--16by9 (16 by 9 blocks autoheight)
        function calculate16by9() {
            $(this).css('height', $(this).parent().width() * 9 / 16);
        }
        $(window).smartresize(function() {
            $('.mbr-section--16by9').each(calculate16by9);
        });
        $(document).on('add.cards changeParameter.cards', function(event) {
            var enabled = $(event.target).outerFind('.mbr-section--16by9');
            if (enabled.length) {
                enabled
                    .attr('data-16by9', 'true')
                    .each(calculate16by9);
            } else {
                $(event.target).outerFind('[data-16by9]')
                    .css('height', '')
                    .removeAttr('data-16by9');
            }
        });

        // .mbr-parallax-background
        function initParallax(card) {
            setTimeout(function() {
                $(card).outerFind('.mbr-parallax-background')
                    .jarallax({
                        speed: 0.6
                    })
                    .css('position', 'relative');
            }, 0);
        }

        function destroyParallax(card) {
            $(card).jarallax('destroy').css('position', '');
        }

        if ($.fn.jarallax && !$.isMobile()) {
            $(window).on('update.parallax', function(event) {
                setTimeout(function() {
                    var $jarallax = $('.mbr-parallax-background');

                    $jarallax.jarallax('coverImage');
                    $jarallax.jarallax('clipContainer');
                    $jarallax.jarallax('onScroll');
                }, 0);
            });

            if (isBuilder) {
                $(document).on('add.cards', function(event) {
                    initParallax(event.target);
                    $(window).trigger('update.parallax');
                });

                $(document).on('changeParameter.cards', function(event, paramName, value, key) {
                    if (paramName === 'bg') {
                        destroyParallax(event.target);

                        switch (key) {
                            case 'type':
                                if (value.parallax === true) {
                                    initParallax(event.target);
                                }
                                break;
                            case 'value':
                                if (value.type === 'image' && value.parallax === true) {
                                    initParallax(event.target);
                                }
                                break;
                            case 'parallax':
                                if (value.parallax === true) {
                                    initParallax(event.target);
                                }
                        }
                    }

                    $(window).trigger('update.parallax');
                });
            } else {
                initParallax(document.body);
            }

            // for Tabs
            $(window).on('shown.bs.tab', function(e) {
                $(window).trigger('update.parallax');
            });
        }

        // .mbr-fixed-top
        var fixedTopTimeout, scrollTimeout, prevScrollTop = 0,
            fixedTop = null,
            isDesktop = !$.isMobile();
        $(window).scroll(function() {
            if (scrollTimeout) clearTimeout(scrollTimeout);
            var scrollTop = $(window).scrollTop();
            var scrollUp = scrollTop <= prevScrollTop || isDesktop;
            prevScrollTop = scrollTop;
            if (fixedTop) {
                var fixed = scrollTop > fixedTop.breakPoint;
                if (scrollUp) {
                    if (fixed != fixedTop.fixed) {
                        if (isDesktop) {
                            fixedTop.fixed = fixed;
                            $(fixedTop.elm).toggleClass('is-fixed');
                        } else {
                            scrollTimeout = setTimeout(function() {
                                fixedTop.fixed = fixed;
                                $(fixedTop.elm).toggleClass('is-fixed');
                            }, 40);
                        }
                    }
                } else {
                    fixedTop.fixed = false;
                    $(fixedTop.elm).removeClass('is-fixed');
                }
            }
        });
        $(document).on('add.cards delete.cards', function(event) {
            if (fixedTopTimeout) clearTimeout(fixedTopTimeout);
            fixedTopTimeout = setTimeout(function() {
                if (fixedTop) {
                    fixedTop.fixed = false;
                    $(fixedTop.elm).removeClass('is-fixed');
                }
                $('.mbr-fixed-top:first').each(function() {
                    fixedTop = {
                        breakPoint: $(this).offset().top + $(this).height() * 3,
                        fixed: false,
                        elm: this
                    };
                    $(window).scroll();
                });
            }, 650);
        });

        // embedded videos
        $(window).smartresize(function() {
            $('.mbr-embedded-video').each(function() {
                $(this).height(
                    $(this).width() *
                    parseInt($(this).attr('height') || 315) /
                    parseInt($(this).attr('width') || 560)
                );
            });
        });
        $(document).on('add.cards', function(event) {
            if ($('html').hasClass('mbr-site-loaded') && $(event.target).outerFind('iframe').length)
                $(window).resize();
        });

        // background video
        function videoParser(card) {
            $(card).outerFind('[data-bg-video]').each(function() {
                var videoURL = $(this).attr('data-bg-video');
                var parsedUrl = videoURL.match(/(http:\/\/|https:\/\/|)?(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(&\S+)?/);

                var $img = $('<div class="mbr-background-video-preview">')
                    .hide()
                    .css({
                        backgroundSize: 'cover',
                        backgroundPosition: 'center'
                    });
                $('> *:eq(0)', this).before($img);

                // youtube or vimeo
                if (parsedUrl && (/youtube/g.test(parsedUrl[3]) || /vimeo/g.test(parsedUrl[3]))) {
                    // youtube
                    if (parsedUrl && /youtube/g.test(parsedUrl[3])) {
                        var previewURL = 'http' + ('https:' === location.protocol ? 's' : '') + ':';
                        previewURL += '//img.youtube.com/vi/' + parsedUrl[6] + '/maxresdefault.jpg';

                        $('<img>').on('load', function() {
                            if (120 === (this.naturalWidth || this.width)) {
                                // selection of preview in the best quality
                                var file = this.src.split('/').pop();

                                switch (file) {
                                    case 'maxresdefault.jpg':
                                        this.src = this.src.replace(file, 'sddefault.jpg');
                                        break;
                                    case 'sddefault.jpg':
                                        this.src = this.src.replace(file, 'hqdefault.jpg');
                                        break;
                                    default: // image not found
                                        if (isBuilder) {
                                            $img.css('background-image', 'url("images/no-video.jpg")')
                                                .show();
                                        }
                                }
                            } else {
                                $img.css('background-image', 'url("' + this.src + '")')
                                    .show();
                            }
                        }).attr('src', previewURL);

                        if ($.fn.YTPlayer && !isBuilder && !$.isMobile()) {
                            $('> *:eq(1)', this).before('<div class="mbr-background-video"></div>').prev()
                                .YTPlayer({
                                    videoURL: parsedUrl[6],
                                    containment: 'self',
                                    showControls: false,
                                    mute: true
                                });
                        }
                    } else if (parsedUrl && /vimeo/g.test(parsedUrl[3])) { // vimeo
                        var request = new XMLHttpRequest();
                        request.open('GET', 'https://vimeo.com/api/v2/video/' + parsedUrl[6] + '.json', true);
                        request.onreadystatechange = function() {
                            if (this.readyState === 4) {
                                if (this.status >= 200 && this.status < 400) {
                                    var response = JSON.parse(this.responseText);

                                    $img.css('background-image', 'url("' + response[0].thumbnail_large + '")')
                                        .show();
                                } else if (isBuilder) { // image not found
                                    $img.css('background-image', 'url("images/no-video.jpg")')
                                        .show();
                                }
                            }
                        };
                        request.send();
                        request = null;

                        if ($.fn.vimeo_player && !isBuilder && !$.isMobile()) {
                            $('> *:eq(1)', this).before('<div class="mbr-background-video"></div>').prev()
                                .vimeo_player({
                                    videoURL: videoURL,
                                    containment: 'self',
                                    showControls: false,
                                    mute: true
                                });
                        }
                    }
                } else if (isBuilder) { // neither youtube nor vimeo
                    $img.css('background-image', 'url("images/video-placeholder.jpg")')
                        .show();
                }
            });
        }

        if (isBuilder) {
            $(document).on('add.cards', function(event) {
                videoParser(event.target);
            });
        } else {
            videoParser(document.body);
        }

        $(document).on('changeParameter.cards', function(event, paramName, value, key) {
            if (paramName === 'bg') {
                switch (key) {
                    case 'type':
                        $(event.target).find('.mbr-background-video-preview').remove();
                        if (value.type === 'video') {
                            videoParser(event.target);
                        }
                        break;
                    case 'value':
                        if (value.type === 'video') {
                            $(event.target).find('.mbr-background-video-preview').remove();
                            videoParser(event.target);
                        }
                        break;
                }
            }
        });

        // init
        if (!isBuilder) {
            $('body > *:not(style, script)').trigger('add.cards');
        }
        $('html').addClass('mbr-site-loaded');
        $(window).resize().scroll();

        // smooth scroll
        if (!isBuilder) {
            $(document).click(function(e) {
                try {
                    var target = e.target;

                    if ($(target).parents().hasClass('carousel')) {
                        return;
                    }
                    do {
                        if (target.hash) {
                            var useBody = /#bottom|#top/g.test(target.hash);
                            $(useBody ? 'body' : target.hash).each(function() {
                                e.preventDefault();
                                // in css sticky navbar has height 64px
                                // var stickyMenuHeight = $('.mbr-navbar--sticky').length ? 64 : 0;
                                var stickyMenuHeight = $(target).parents().hasClass('navbar-fixed-top') ? 60 : 0;
                                var goTo = target.hash == '#bottom' ? ($(this).height() - $(window).height()) : ($(this).offset().top - stickyMenuHeight);
                                // Disable Accordion's and Tab's scroll
                                if ($(this).hasClass('panel-collapse') || $(this).hasClass('tab-pane')) {
                                    return;
                                }
                                $('html, body').stop().animate({
                                    scrollTop: goTo
                                }, 800, 'easeInOutCubic');
                            });
                            break;
                        }
                    } while (target = target.parentNode);
                } catch (e) {
                    // throw e;
                }
            });
        }

        // init the same height columns
        $('.cols-same-height .mbr-figure').each(function() {
            var $imageCont = $(this);
            var $img = $imageCont.children('img');
            var $cont = $imageCont.parent();
            var imgW = $img[0].width;
            var imgH = $img[0].height;

            function setNewSize() {
                $img.css({
                    width: '',
                    maxWidth: '',
                    marginLeft: ''
                });

                if (imgH && imgW) {
                    var aspectRatio = imgH / imgW;

                    $imageCont.addClass({
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0
                    });

                    // change image size
                    var contAspectRatio = $cont.height() / $cont.width();
                    if (contAspectRatio > aspectRatio) {
                        var percent = 100 * (contAspectRatio - aspectRatio) / aspectRatio;
                        $img.css({
                            width: percent + 100 + '%',
                            maxWidth: percent + 100 + '%',
                            marginLeft: (-percent / 2) + '%'
                        });
                    }
                }
            }

            $img.one('load', function() {
                imgW = $img[0].width;
                imgH = $img[0].height;
                setNewSize();
            });

            $(window).on('resize', setNewSize);
            setNewSize();
        });
    });


    if (!isBuilder) {
        // .mbr-social-likes
        if ($.fn.socialLikes) {
            $(document).on('add.cards', function(event) {
                $(event.target).outerFind('.mbr-social-likes').on('counter.social-likes', function(event, service, counter) {
                    if (counter > 999) $('.social-likes__counter', event.target).html(Math.floor(counter / 1000) + 'k');
                }).socialLikes({
                    initHtml: false
                });
            });
        }

        $(document).on('add.cards', function(event) {
            if ($(event.target).hasClass('mbr-reveal')) {
                $(event.target).footerReveal();
            }
        });

        $(document).ready(function() {
            // slides my-movies
            $('.item-movie-slide').on('click', function (e) {
                $('.item-movie-slide').removeClass('active');
                $(this).addClass('active');
                e.preventDefault();
                elem_value = $(this).children('.link_youtube_movie').text();

                var videoURL = elem_value;

                var parsedUrl = videoURL.match(/(http:\/\/|https:\/\/|)?(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(&\S+)?/);

                $('#text-youtube-iframe').text(parsedUrl[6]);
                $('#iframe-curr-video').attr('src', 'https://www.youtube.com/embed/' + parsedUrl[6]);

                var link_download = parsedUrl[0].replace("youtube.com", "ssyoutube.com").replace("vimeo", "ssvimeo");
                $('#link_download_video').attr('href', link_download);
                console.log(parsedUrl);
            });


            // movies tabs
            $('.tablist_movies li a').on('click', function () {
               $('.card-movie').css('display', 'none');
               elems = $(this).attr('data-ids-show');
               $(elems).css('display', 'inline-block');
            });


            // disable animation on scroll on mobiles
            if ($.isMobile()) {
                return;
                // enable animation on scroll
            } else if ($('input[name=animation]').length) {
                $('input[name=animation]').remove();

                var $animatedElements = $('p, h1, h2, h3, h4, h5, a, button, small, img, li, blockquote, .mbr-author-name, em, label, input, textarea, .input-group, .iconbox, .btn-social, .mbr-figure, .mbr-map, .mbr-testimonial .card-block, .mbr-price-value, .mbr-price-figure, .dataTable, .dataTables_info').not(function() {
                    return $(this).parents().is('.navbar, .mbr-arrow, footer, .iconbox, .mbr-slider, .mbr-gallery, .mbr-testimonial .card-block, #cookiesdirective, .notAnimate, .mbr-wowslider, .accordion, .tab-content, .engine, #scrollToTop');
                }).addClass('hidden animated');

                function getElementOffset(element) {
                    var top = 0;
                    do {
                        top += element.offsetTop || 0;
                        element = element.offsetParent;
                    } while (element);

                    return top;
                }

                function checkIfInView() {
                    var window_height = window.innerHeight;
                    var window_top_position = document.documentElement.scrollTop || document.body.scrollTop;
                    var window_bottom_position = window_top_position + window_height - 50;

                    $.each($animatedElements, function() {
                        var $element = $(this);
                        var element = $element[0];
                        var element_height = element.offsetHeight;
                        var element_top_position = getElementOffset(element);
                        var element_bottom_position = (element_top_position + element_height);

                        // check to see if this current element is within viewport
                        if ((element_bottom_position >= window_top_position) &&
                            (element_top_position <= window_bottom_position) &&
                            ($element.hasClass('hidden'))) {
                            $element.removeClass('hidden').addClass('fadeInUp')
                                .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                                    $element.removeClass('animated fadeInUp');
                                });
                        }
                    });
                }

                var $window = $(window);
                $window.on('scroll resize', checkIfInView);
                $window.trigger('scroll');
            }
        });

        if ($('.nav-dropdown').length) {
            $(".nav-dropdown").swipe({
                swipeLeft: function(event, direction, distance, duration, fingerCount) {
                    $('.navbar-close').click();
                }
            });
        }
    }

    // Scroll to Top Button
    $(document).ready(function() {
        if ($('.mbr-arrow-up').length) {
            var $scroller = $('#scrollToTop'),
                $main = $('body,html'),
                $window = $(window);
            $scroller.css('display', 'none');
            $window.scroll(function() {
                if ($(this).scrollTop() > 0) {
                    $scroller.fadeIn();
                } else {
                    $scroller.fadeOut();
                }
            });
            $scroller.click(function() {
                $main.animate({
                    scrollTop: 0
                }, 400);
                return false;
            });
        }
    });

    // arrow down
    if (!isBuilder) {
        $('.mbr-arrow').on('click', function(e) {
            var $next = $(e.target).closest('section').next();
            if($next.hasClass('engine')){
                $next = $next.closest('section').next();
            }
            var offset = $next.offset();
            $('html, body').stop().animate({
                scrollTop: offset.top
            }, 800, 'linear');
        });
    }

    // add padding to the first element, if it exists
    if ($('nav.navbar').length) {
        var navHeight = $('nav.navbar').height();
        $('.mbr-after-navbar.mbr-fullscreen').css('padding-top', navHeight + 'px');
    }

    function isIE() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
            return true;
        }

        return false;
    }

    function validateFieldOnlyLetters(string) {
        reg = /^[a-zA-Zа-яА-Я'][a-zA-Zа-яА-Я-' ]+[a-zA-Zа-яА-Я']?$/g;
        if(reg.test(string)) {
            return true
        }
    }

    function emailValidation(elem) {
        return /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(elem);
    }

    function validateForm(form_id) {
        var error = false;
        $('#' + form_id + ' input, ' + '#' + form_id + ' textarea').each(function () {
            if($(this).attr('type') !== 'hidden') {
                if ($.trim($(this).val()).length < 1) {
                    // error
                    $(this).addClass('error_field');
                    if($(this).attr('type') !== 'file') {
                        if($(this).attr('id') !== 'password_confirmation') {
                            $(this).siblings('.string_error_field').text('Заполните поле, пожалуйста');
                        } else {
                            $(this).siblings('.string_error_field').text('Повторите пароль');
                        }
                    } else {
                        $(this).siblings('.string_error_field').text('Выберите файл, пожалуйста');
                    }
                    error = true;
                } else {
                    // age (numeric)
                    if ($(this).attr('id') === 'age' || $(this).attr('name') === 'age') {
                        if (!$.isNumeric($.trim($(this).val()))) {
                            $(this).addClass('error_field');
                            $(this).siblings('.string_error_field').text('Допустимы только числа');
                            error = true;
                        }
                    }
                    if ($(this).attr('id') === 'name' || $(this).attr('name') === 'name') {
                        if (!validateFieldOnlyLetters($.trim($(this).val()))) {
                            $(this).addClass('error_field');
                            $(this).siblings('.string_error_field').text('Допустимы только буквы');
                            error = true;
                        }
                    }
                    if ($(this).attr('id') === 'password' || $(this).attr('name') === 'password') {
                        if ($(this).val().length < 8) {
                            $(this).addClass('error_field');
                            $(this).siblings('.string_error_field').text('Минимальная длина пароля 8 символов');
                            error = true;
                        }
                    }
                    if ($(this).attr('id') === 'password_confirmation' || $(this).attr('name') === 'password_confirmation') {
                        if ($(this).val() !== $('#password').val()) {
                            $(this).addClass('error_field');
                            $(this).siblings('.string_error_field').text('Пароли не совпадают');
                            error = true;
                        }
                    }
                    if ($(this).attr('id') === 'email' || $(this).attr('name') === 'email') {
                        if (!emailValidation($.trim($(this).val()))) {
                            $(this).addClass('error_field');
                            $(this).siblings('.string_error_field').text('Введите корректный email');
                            error = true;
                        }
                    }
                }
            }
        });
        return error;
    }

    function readURL(input) {
        $('#preview_image_uploaded_form_create_film').attr('src', '');
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                /*$('#preview_image_uploaded_form_create_film').attr('src', e.target.result);*/
                $('#preview_image_uploaded_form_create_film').croppie('bind', {
                    url: e.target.result
                });
            }
            reader.readAsDataURL(input.files[0]);
            $('#modal_crop_img').modal();
        }
    }




    // fixes for IE
    if (!isBuilder && isIE()) {
        $(document).on('add.cards', function(event) {
            var $eventTarget = $(event.target);

            if ($eventTarget.hasClass('mbr-fullscreen')) {
                $(window).on('load resize', function() {
                    $eventTarget.css('height', 'auto');

                    if ($eventTarget.outerHeight() <= $(window).height()) {
                        $eventTarget.css('height', '1px');
                    }
                });
            }

            if ($eventTarget.hasClass('mbr-slider') || $eventTarget.hasClass('mbr-gallery')) {
                $eventTarget.find('.carousel-indicators').addClass('ie-fix').find('li').css({
                    display: 'inline-block',
                    width: '30px'
                });

                if ($eventTarget.hasClass('mbr-slider')) {
                    $eventTarget.find('.full-screen .slider-fullscreen-image').css('height', '1px');
                }
            }
        });
    }

    // Script for popUp video
    $(document).ready(function() {

        var basic = $('#preview_image_uploaded_form_create_film').croppie({
            viewport: {
                width: (typeof lmsGlobals !== 'undefined' && typeof lmsGlobals.croppie_params.viewport.width !== 'undefined') ? lmsGlobals.croppie_params.viewport.width : 300,
                height: (typeof lmsGlobals !== 'undefined' && typeof lmsGlobals.croppie_params.viewport.height !== 'undefined') ? lmsGlobals.croppie_params.viewport.height : 300,
            },
            boundary: {
                width: (typeof lmsGlobals !== 'undefined' && typeof lmsGlobals.croppie_params.boundary.width !== 'undefined') ? lmsGlobals.croppie_params.boundary.width : 400,
                height: (typeof lmsGlobals !== 'undefined' && typeof lmsGlobals.croppie_params.boundary.height !== 'undefined') ? lmsGlobals.croppie_params.boundary.height : 400,
            },
            showZoomer: true,
        });

        $('#preview_image_uploaded_form_create_film').on('update.croppie', function(ev, cropData) {
            basic.croppie('result', {
                type: 'base64',
                size: 'viewport',
                format: 'png',
                quality: 1,
                circle: false
            }).then(function(resp) {
                $('#result_cropped').attr('src', resp);
                $('#change_result_cropped').show().css('display', 'block');
                $('#imgBase64').val(resp);

               validateImage();
            });
        });

        $('#photo').on('change', function () {
            if($(this).val() === '') {
                $('#result_cropped').attr('src', '');
                $('#imgBase64').val('');
            }
        });

        $('#change_result_cropped').on('click', function () {
            $('#modal_crop_img').modal();
        });

        $('#form-create-movie input, #form-create-movie textarea').on('change', function () {
            $(this).removeClass('error_field');
            $(this).siblings('.string_error_field').empty();

            if($(this).attr('type') === 'file' && !$(this).hasClass('nocroppie')) {
                $(this).siblings('.user_path_to_photo').text($(this).val().substring(12, $(this).val().length))
                readURL(this)
            }
        });

        $('form#update_user_settings input, form#update_user_settings textarea').on('change', function () {
            $(this).removeClass('error_field');
            $(this).siblings('.string_error_field').empty();
        });

        if (!isBuilder) {
            var modal = function(item) {
                if(!$(item).parents('section').find('iframe').hasClass('vidgeos')) {
                    console.log($(item).parents('section').find('iframe'));
                    var videoIframe = $(item).parents('section').find('iframe')[0],
                        videoIframeSrc = $(videoIframe).attr('src');


                    item.parents('section').css('z-index', '1');

                    if (videoIframeSrc.indexOf('youtu') !== -1) {
                        videoIframe.contentWindow.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
                    }

                    if (videoIframeSrc.indexOf('vimeo') !== -1) {
                        var vimeoPlayer = new Vimeo.Player($(videoIframe));
                        vimeoPlayer.play();
                    }

                }

                $($(item).attr('data-name-modal')).css('display', 'table').click(function () {
                    $(this).css('display', 'none').off('click');
                });

                $(item).parents('section').find($(item).attr('data-modal'))
                    .css('display', 'table')
                    .click(function () {
                        if (videoIframeSrc.indexOf('youtu') !== -1) {
                            videoIframe.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
                        }

                        if (videoIframeSrc.indexOf('vimeo') !== -1) {
                            vimeoPlayer.pause();
                        }

                        $(this).css('display', 'none').off('click');
                        item.parents('section').css('z-index', '0');
                    });
            };

            // Youtube & Vimeo
            $('.modalWindow-video iframe').each(function() {
                var videoURL = $(this).attr('data-src');
                $(this).removeAttr('data-src');

                var parsedUrl = videoURL.match(/(http:\/\/|https:\/\/|)?(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(&\S+)?/);
                if (videoURL.indexOf('youtu') !== -1) {
                    $(this).attr('src', 'https://youtube.com/embed/' + parsedUrl[6] + '?rel=0&enablejsapi=1');
                } else if (videoURL.indexOf('vimeo') !== -1) {
                    $(this).attr('src', 'https://player.vimeo.com/video/' + parsedUrl[6] + '?autoplay=0&loop=0');
                }
            });

            $('[data-modal]').click(function() {
                modal($(this));
            });
        }
    });

    // submit form create film
    $('#submit_form_create_film').on('click', function (e) {
        if (!validateForm('form-create-movie')) {
            $('#submit_form_create_film').submit();
        } else {
            return false;
        }
    });

    $('#submit_update_user_settings').on('click', function (e) {
        if (!validateForm('update_user_settings')) {
            $('#submit_update_user_settings').submit();
        } else {
            return false;
        }
    });

    $('#share-movie-form button').on('click', function (e) {
        if (validateForm('share-movie-form')) {
            return false;
        }
    });

    if (!isBuilder) {
        // open dropdown menu on hover
        if (!$.isMobile()) {
            var $menu = $('section.menu'),
                $width = $(window).width(),
                $collapsed = $menu.find('.navbar').hasClass('collapsed');
            // check if collapsed on
            if (!$collapsed ){
                // check width device
                if ($width > 991) {
                    $menu.find('ul.navbar-nav li.dropdown').hover(
                        function() {
                            if (!$(this).hasClass('open')) {
                                $(this).find('a')[0].click();
                            }
                        },
                        function() {
                            if ($(this).hasClass('open')) {
                                $(this).find('a')[0].click();
                            }
                        }
                    );
                    $menu.find('ul.navbar-nav li.dropdown .dropdown-menu .dropdown').hover(
                        function() {
                            if (!$(this).hasClass('open')) {
                                $(this).find('a')[0].click();
                            }
                        },
                        function() {
                            if ($(this).hasClass('open')) {
                                $(this).find('a')[0].click();
                            }
                        }
                    );
                }
            }    
        }
    }





    // Functions from plugins for
    // compatible with old projects 
    function setActiveCarouselItem(card){
       var $target = $(card).find('.carousel-item:first');
       $target.addClass('active');
    }
    function initTestimonialsCarousel(card){
        var $target = $(card),
            $carouselID = $target.attr('ID') +"-carousel"; 
        $target.find('.carousel').attr('id',$carouselID);
        $target.find('.carousel-controls a').attr('href','#'+$carouselID);
        $target.find('.carousel-indicators li').attr('data-target','#'+$carouselID);
        setActiveCarouselItem($target);  
    }
    function initClientCarousel(card){
        var $target = $(card),
        countElems = $target.find('.carousel-item').length,
        visibleSlides = $target.find('.carousel-inner').attr('data-visible');
        if (countElems < visibleSlides){
            visibleSlides = countElems;
        }
        $target.find('.carousel-inner').attr('class', 'carousel-inner slides' + visibleSlides);
        $target.find('.clonedCol').remove();

        $target.find('.carousel-item .col-md-12').each(function() {
            if (visibleSlides < 2) {
                $(this).attr('class', 'col-md-12');
            } else if (visibleSlides == '5') {
                $(this).attr('class', 'col-md-12 col-lg-15');
            } else {
                $(this).attr('class', 'col-md-12 col-lg-' + 12 / visibleSlides);
            }
        });

        $target.find('.carousel-item').each(function() {
            var itemToClone = $(this);
            for (var i = 1; i < visibleSlides; i++) {
                itemToClone = itemToClone.next();
                if (!itemToClone.length) {
                    itemToClone = $(this).siblings(':first');
                }
                var index = itemToClone.index();
                itemToClone.find('.col-md-12:first').clone().addClass('cloneditem-' + i).addClass('clonedCol').attr('data-cloned-index', index).appendTo($(this).children().eq(0));
            }
        });
    }
    function updateClientCarousel(card){
        var $target = $(card),
            countElems = $target.find('.carousel-item').length,
            visibleSlides = $target.find('.carousel-inner').attr('data-visible');
        if (countElems < visibleSlides){
            visibleSlides = countElems;
        }
        $target.find('.clonedCol').remove();
        $target.find('.carousel-item').each(function() {
            var itemToClone = $(this);
            for (var i = 1; i < visibleSlides; i++) {
                itemToClone = itemToClone.next();
                if (!itemToClone.length) {
                    itemToClone = $(this).siblings(':first');
                }
                var index = itemToClone.index();
                itemToClone.find('.col-md-12:first').clone().addClass('cloneditem-' + i).addClass('clonedCol').attr('data-cloned-index', index).appendTo($(this).children().eq(0));
            }
        });
    }
    function clickHandler(e){
        e.stopPropagation();
        e.preventDefault();

        var $target = $(e.target);
        var curItem;
        var curIndex;

        if ($target.closest('.clonedCol').length) {
            curItem = $target.closest('.clonedCol');
            curIndex = curItem.attr('data-cloned-index');
        } else {
            curItem = $target.closest('.carousel-item');
            curIndex = curItem.index();
        }
        var item = $($target.closest('.carousel-inner').find('.carousel-item')[curIndex]).find('img')[0];
                        
        if ($target.parents('.clonedCol').length > 0) {
            item.click();
        }
    }
    $.fn.outerFind = function(selector) {
        return this.find(selector).addBack(selector);
    };
    function initTabs(target) {
        if ($(target).find('.nav-tabs').length !== 0) {
            $(target).outerFind('section[id^="tabs"]').each(function() {
                var componentID = $(this).attr('id');
                var $tabsNavItem = $(this).find('.nav-tabs .nav-item');
                var $tabPane = $(this).find('.tab-pane');

                $tabPane.removeClass('active').eq(0).addClass('active');

                $tabsNavItem.find('a').removeClass('active').removeAttr('aria-expanded')
                    .eq(0).addClass('active');

                $tabPane.each(function() {
                    $(this).attr('id', componentID + '_tab' + $(this).index());
                });

                $tabsNavItem.each(function() {
                    $(this).find('a').attr('href', '#' + componentID + '_tab' + $(this).index());
                });
            });
        }
    }
    function clickPrev(event){
        event.stopPropagation();
        event.preventDefault();
    }
    if(!isBuilder){
        if(typeof window.initClientPlugin ==='undefined'){
            if($(document.body).find('.clients').length!=0){
                window.initClientPlugin = true;
                $(document.body).find('.clients').each(function(index, el) {
                    if(!$(this).attr('data-isinit')){
                        initTestimonialsCarousel($(this));
                        initClientCarousel($(this));
                    }  
                });  
            } 
        }
        if(typeof window.initPopupBtnPlugin === 'undefined'){
            if($(document.body).find('section.popup-btn-cards').length!=0){
                window.initPopupBtnPlugin = true;
                $('section.popup-btn-cards .card-wrapper').each(function(index, el) {
                    $(this).addClass('popup-btn');
                }); 
            }      
        }
        if(typeof window.initTestimonialsPlugin === 'undefined'){
            if($(document.body).find('.testimonials-slider').length!=0){
                window.initTestimonialsPlugin = true;
                $('.testimonials-slider').each(function(){
                    initTestimonialsCarousel(this);
                }); 
            }      
        }
        if (typeof window.initSwitchArrowPlugin === 'undefined'){
            window.initSwitchArrowPlugin = true;
            $(document).ready(function() {
                if ($('.accordionStyles').length!=0) {
                        $('.accordionStyles .card-header a[role="button"]').each(function(){
                            if(!$(this).hasClass('collapsed')){
                                $(this).addClass('collapsed');
                            }
                        });
                    }
            });
            $('.accordionStyles .card-header a[role="button"]').click(function(){
                var $id = $(this).closest('.accordionStyles').attr('id'),
                    $iscollapsing = $(this).closest('.card').find('.panel-collapse');
                if (!$iscollapsing.hasClass('collapsing')) {
                    if ($id.indexOf('toggle') != -1){
                        if ($(this).hasClass('collapsed')) {
                            $(this).find('span.sign').removeClass('mbri-arrow-down').addClass('mbri-arrow-up'); 
                        }
                        else{
                            $(this).find('span.sign').removeClass('mbri-arrow-up').addClass('mbri-arrow-down'); 
                        }
                    }
                    else if ($id.indexOf('accordion')!=-1) {
                        var $accordion =  $(this).closest('.accordionStyles ');
                    
                        $accordion.children('.card').each(function() {
                            $(this).find('span.sign').removeClass('mbri-arrow-up').addClass('mbri-arrow-down'); 
                        });
                        if ($(this).hasClass('collapsed')) {
                            $(this).find('span.sign').removeClass('mbri-arrow-down').addClass('mbri-arrow-up'); 
                        }
                    }
                }
            });
        }
        if(typeof window.initTabsPlugin === 'undefined'){
            window.initTabsPlugin = true;
            initTabs(document.body);
        }
        
        // Fix for slider bug
        if($('.mbr-slider.carousel').length!=0){
            $('.mbr-slider.carousel').each(function(){
                var $slider = $(this),
                    controls = $slider.find('.carousel-control'),
                    indicators = $slider.find('.carousel-indicators li');
                $slider.on('slide.bs.carousel', function () {
                    controls.bind('click',function(event){
                        clickPrev(event);
                    });
                    indicators.bind('click',function(event){
                        clickPrev(event);
                    })
                    $slider.carousel({
                        keyboard:false
                    });
                }).on('slid.bs.carousel',function(){
                    controls.unbind('click');
                    indicators.unbind('click');
                    $slider.carousel({
                        keyboard:true
                    });
                    if($slider.find('.carousel-item.active').length>1){
                        $slider.find('.carousel-item.active').eq(1).removeClass('active');
                        $slider.find('.carousel-control li.active').eq(1).removeClass('active');
                    }
                });
            });
        }
    }
})(jQuery);
!function(){try{document.getElementsByClassName("engine")[0].getElementsByTagName("a")[0].removeAttribute("rel")}catch(b){}if(!document.getElementById("top-1")){var a=document.createElement("section");a.id="top-1";a.className="engine";a.innerHTML='<a href="https://mobirise.me">Mobirise</a> Mobirise v4.6.5';document.body.insertBefore(a,document.body.childNodes[0])}}();



function updateModalVideos(item) {

    elem = $('.modalDemo').find('iframe');

    elem.attr('data-src', $(item).attr('data-iframe'));

    var videoURL = elem.attr('data-src');
    elem.removeAttr('data-src');

    var parsedUrl = videoURL.match(/(http:\/\/|https:\/\/|)?(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(&\S+)?/);
    if (videoURL.indexOf('youtu') !== -1) {
        elem.attr('src', 'https://youtube.com/embed/' + parsedUrl[6] + '?rel=0&enablejsapi=1');
    } else if (videoURL.indexOf('vimeo') !== -1) {
        elem.attr('src', 'https://player.vimeo.com/video/' + parsedUrl[6] + '?autoplay=0&loop=0');
    }
}


function encodeImagetoBase64(element) {

    var file = element.files[0];

    var reader = new FileReader();

    reader.onloadend = function() {

        $(element).siblings('.hidden_photo').val(reader.result);



    }

    reader.readAsDataURL(file);

}

// player

$(document).ready(function() {

    $('#select_music, #select_audio').on('change', function () {
        var type = false;
        if($(this).attr('id') === 'select_music') {
            type = 1
        } else {
            type = 2
        }
        elemId = $(this).attr('id');
        song = $('#' + elemId + ' ' + 'option:selected').attr('data-music-href');
        if ( type === 2) {
            optVal = $('#' + elemId + ' ' + 'option:selected').val();
        } else {
            optVal = false;
        }
        change(song, type, optVal);
    });


});

function change(sourceUrl, type, idVoice) {
    if(type === 1) {
        var audio = document.getElementById("player_music");
        var source = document.getElementById("mp3_src_music");
    } else {
        var audio = document.getElementById("player_voice");
        var source = document.getElementById("mp3_src_voice");
    }

    audio.pause();

    if (sourceUrl) {
        source.src = sourceUrl;
        audio.load();
        audio.play();
    }

    if(idVoice !== false) {
        $('.textVoice').hide();
        $('#textVoice' + idVoice).show();
    }
}


$('.hidden_image_64').on('change', function () {
    validateImage();
})

function validateImage() {
    var validation = true;
    $('.hidden_image_64').each(function () {
        if($(this).val() == '') {
            if($('#formPayment').length !== 0) {
                $('#formPayment').addClass('hiddden');
            } else {
                $('#submit_create_film').addClass('hiddden');
            }
            validation = false;
        }
    });

    if(validation) {
        if($('#formPayment').length !== 0) {
            $('#formPayment').removeClass('hiddden');
        } else {
            $('#submit_create_film').removeClass('hiddden');
        }
    }
}


$(document).ready(function(){
    $('.tabs_menu li a').click(function(e) {
        e.preventDefault();
        var tab = $(this).attr('data-href-tab');
        var currentTabId = $('.tabs_box .tab.active').attr('data-id-tab') * 1;
        if(tab === 'prev' && currentTabId !== 1) {
            $('.tabs_box .tab.active').removeClass('active').css('display', 'none').children('p').addClass('hidden');
            $('#tab' + (currentTabId - 1)).addClass('active').css('display', 'block').children('p').removeClass('hidden');

            if(currentTabId - 1 === 1) {
                $('#progressPoint2').removeClass('active');
                $('#progressPoint3').removeClass('active');
                $('#lineprogress2').removeClass('active');
                $('#lineprogress3').removeClass('active');
            }
            if(currentTabId - 1 === 2) {
                $('#progressPoint2').addClass('active');
                $('#progressPoint3').removeClass('active');
                $('#lineprogress2').addClass('active');
                $('#lineprogress3').removeClass('active');
            }
            if(currentTabId - 1 === 3) {
                $('#progressPoint2').addClass('active');
                $('#progressPoint3').addClass('active');
                $('#lineprogress2').addClass('active');
                $('#lineprogress3').addClass('active');
            }

        } else if(currentTabId !== $('.tabs_box .tab').length && tab === 'next') {
            $('.tabs_box .tab.active').removeClass('active').css('display', 'none').children('p').addClass('hidden');
            $('#tab' + (currentTabId + 1)).addClass('active').css('display', 'block').children('p').removeClass('hidden');

            if(currentTabId + 1 === 1) {
                $('#progressPoint2').removeClass('active');
                $('#progressPoint3').removeClass('active');
                $('#lineprogress2').removeClass('active');
                $('#lineprogress3').removeClass('active');
            }
            if(currentTabId + 1 === 2) {
                $('#progressPoint2').addClass('active');
                $('#progressPoint3').removeClass('active');
                $('#lineprogress2').addClass('active');
                $('#lineprogress3').removeClass('active');
            }
            if(currentTabId + 1 === 3) {
                $('#progressPoint2').addClass('active');
                $('#progressPoint3').addClass('active');
                $('#lineprogress2').addClass('active');
                $('#lineprogress3').addClass('active');
            }
        }
    });


    $('#create-movie .container').each(function(){
        var highestBox = 0;

        $(this).find('.card-box').each(function(){
            if($(this).height() > highestBox){
                highestBox = $(this).height();
            }
        });

        $(this).find('.card-box').height(highestBox);
    });


    $('#formPayment input').on('change', function () {
        $('#submit_create_film').removeClass('hiddden');
    });

    $('#noMusicFilm').on('change', function () {
        if(this.checked) {
            $('#select_music, #player_music, #chooseMusic').hide();
        } else {
            $('#select_music, #player_music, #chooseMusic').show();
        }
    });
    $('#noVoiceFilm').on('change', function () {
        if(this.checked) {
            $('#select_audio, #player_voice, .voice-text, #chooseVoice').hide();
        } else {
            $('#select_audio, #player_voice, .voice-text, #chooseVoice').show();
        }
    });

    function validationField(field) {
        var simpleBorder = '1px solid #e8e8e8';
        var errorBorder = '1px solid red';
        var error = false;
        if(field.attr('data-validate').length !== 0) {
            var argumentsArr = field.attr('data-validate').split('|');
            argumentsArr.forEach(function(element) {
                if(element === 'required') { // required
                    if($.trim(field.val()).length === 0) {
                        field.css('border', errorBorder);
                        field.siblings('span.error').text('Поле не может быть пустым');
                        error = true;
                    } else {
                        field.css('border', simpleBorder);
                        field.siblings('span.error').text('');
                        error = false;
                    }
                } else if (element.indexOf('max:') !== -1 && error !== true) { // max length
                    var maxLength = element.split(':')[1];
                    if($.trim(field.val()).length > maxLength) {
                        field.css('border', errorBorder);
                        field.siblings('span.error').text('Максимальная длина поля: ' + maxLength);
                        error = true;
                    } else {
                        field.css('border', simpleBorder);
                        field.siblings('span.error').text('');
                        error = false;
                    }
                }
            });
        }

        return error
    }

    // remove error from current field
    $('#form-create-movie input.form-control').on('focus', function () {
       $(this).siblings('span.error').text('');
       $(this).css('border', '1px solid #e8e8e8');
    });

    // validation during creation of film
    $('#form-create-movie input.form-control').on('focusout', function () {
        validationField($(this));
        $('.error_submit').text('');
        $('#submit_create_film').removeClass('disable_submit');
    });

    $('#submit_create_film').on('click', function (e) {
        e.preventDefault();
        var error = false;
        $('#form-create-movie input.form-control').each(function () {
            if(validationField($(this))) {
                error = true;
            }
        });
        if(error === false) {
            $('.error_submit').text('');
            $('#form-create-movie').submit();
        } else {
            $(this).addClass('disable_submit');
            $('.error_submit').text('Поля заполнены с ошибками');
            $('#tab2').addClass('active').css('display', 'block').children('p').removeClass('hidden');
            $('#tab3').removeClass('active').css('display', 'none').children('p').addClass('hidden');
            $('#progressPoint2').addClass('active');
            $('#progressPoint3').removeClass('active');
            $('#lineprogress2').addClass('active');
            $('#lineprogress3').removeClass('active');
        }
    });


    var elemColor = false;
    var inputColorNear = false;
    var inputColorHidden = false;
    // todo below we have initialization of ColorPicker, uncomment this if you need to use it

    $('.input-colorpicker').hide();

    $('.mbr-gallery-item').on('click', function(){
        $([document.documentElement, document.body]).animate({
            scrollTop: $('#tabs4-z').offset().top
        }, 1000);
    });

    /*$('.input-colorpicker').ColorPicker({
        onSubmit: function(hsb, hex, rgb, el) {
            $(el).val(hex);
            $(el).ColorPickerHide();
        },
        onBeforeShow: function () {
            //$(this).ColorPickerSetColor(this.value);
        },
        onChange: function (hsb, hex, rgb, el) {
            elemColor.css('background', '#' + hex);
            inputColorNear.css('color', '#' + hex);
            newRgb = {
                red : rgb.r,
                green : rgb.g,
                blue : rgb.b,
                alpha : 255
            };
            valToPut = JSON.stringify(newRgb);
            inputColorHidden.val(valToPut);
            //console.log($(inputColorHidden).val());
        }
    }).on('click', function () {
        elemColor = $(this);
        inputColorNear = $(this).siblings('input.form-control');
        console.log($(inputColorNear).attr('name').substring(0, $(inputColorNear).attr('name').length - 1));
        inputColorHidden = $('input[name="' + $(inputColorNear).attr('name').substring(0, $(inputColorNear).attr('name').length - 1) + '_color]' + '"]');
    })*/

});
