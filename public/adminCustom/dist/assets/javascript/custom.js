$(document).ready(function () {
    // mass values to fill input
    $('input.single[type="checkbox"]').on('change', function () {
        $('.input_to_delete#send').val('');
        var array_ids = [];
        $('input.single[type="checkbox"]:checked').each(function () {
            array_ids.push($(this).attr('data-id'));
        });
        $('.input_to_delete#send').val(array_ids);
    });

    activeLink();
});


function activeLink() {
    var adminRequest = location.href.split('/admin');
    if(adminRequest[1] == '/' || adminRequest[1] == '') {
        $('#adminDashboard').addClass('active');
    } else {
        $('a[data_nav_link="' + adminRequest[1] + '"]').addClass('active').parents('.menu, .menu-item').addClass('has-open');
    }
}

