<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsermoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('usermovies', function (Blueprint $table) {
           $table->integer('movie_category_id')->references('id')->on('moviecategories');
           $table->integer('template_movie_id')->references('id')->on('templatemovies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('usermovies', function (Blueprint $table) {
           $table->dropColumn('movie_category_id');
           $table->dropColumn('template_movie_id');
        });
    }
}
