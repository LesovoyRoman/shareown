<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTemplatemoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('templatemovies', function (Blueprint $table) {
           $table->integer('movie_category_id')->references('id')->on('moviecategories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('templatemovies', function (Blueprint $table) {
            $table->dropColumn('movie_category_id');
        });
    }
}
