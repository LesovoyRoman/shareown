<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovieCategoriesTemplateMovies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movie_categories_template_movies', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('template_movies_id')->references('id')->on('templatemovies');
        $table->integer('movie_categories_id')->references('id')->on('moviecategories');
        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movie_categories_template_movies');
    }
}
