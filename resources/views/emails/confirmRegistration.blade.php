<!DOCTYPE html>
<html>
<head>
	<title>Добро пожаловать</title>
</head>

<body>
<h2>Добро пожаловать, {{$user['name']}}!</h2>
<br/>
Вы зарегистрировались на сайте {{config('app.url')}}. Ваш логин - {{$user['email']}}.
</body>

</html>