@extends('letmeshare.layouts.master')

@section('content')


	<section class="header8 cid-qY9Krrhtcx mbr-fullscreen mbr-parallax-background" id="header8-1">

		<div class="mbr-overlay" style="opacity: 0.8; background-color: rgb(255, 51, 102);">
		</div>

		<div class="container align-center">
			<div class="row justify-content-md-center">
				<div class="mbr-white col-md-10">
					<h1 class="mbr-section-title align-center py-2 mbr-bold mbr-fonts-style display-2">Генератор Рекламных Видео</h1>
					<p class="mbr-text align-center py-2 mbr-fonts-style display-5">
						Вашему бизнесу нужно видео, но вы не готовы платить $500 за его создание? С помощью нашего сервиса вы сможете создать рекламный видео-ролик для вашего сайта не за $1000, и даже не за $100, а всего за $19</p>
					<div class="mbr-media show-modal align-center py-2" data-modal=".modalWindow">
						<span class="mbri-play mbr-iconfont"></span>
					</div>

					@foreach ($template_movies as $template_movie)
						<div class="mbr-media show-modal align-center py-2" id="show-modal-{{ $template_movie['movie_id'] }}" data-modal=".modalWindow-{{ $template_movie['movie_id'] }}" style="display: none;">
							<span class="mbri-play mbr-iconfont"></span>
						</div>
					@endforeach

					<div class="icon-description align-center font-italic pb-3 mbr-fonts-style display-7">
						Смотреть как это работает</div>
					<div class="mbr-section-btn text-center"><a class="btn btn-md btn-primary display-4" href="#create-movie">СОЗДАТЬ СЕЙЧАС</a></div>
				</div>
			</div>
		</div>



		<div class="modalWindow" style="display: none;">
			<div class="modalWindow-container">
				<div class="modalWindow-video-container">
					<div class="modalWindow-video">
						<iframe width="100%" height="100%" frameborder="0" allowfullscreen="1" {{-- src="http://www.youtube.com/watch?v=uNCr7NdOJgw" data-src="http://www.youtube.com/watch?v=uNCr7NdOJgw"--}} data-src="https://app.vidgeos.com/embed/17312" src="https://app.vidgeos.com/embed/17312"></iframe>
					</div>
					<a class="close" role="button" data-dismiss="modal">
						<span aria-hidden="true" class="mbri-close mbr-iconfont closeModal"></span>
						<span class="sr-only">Close</span>
					</a>
				</div>
			</div>
		</div>

		@foreach ($template_movies as $template_movie)
			<div>
				<div class="modalWindow-{{ $template_movie['movie_id'] }}" style="display: none; margin-top: 30px;">
					<div class="modalWindow-container">
						<div class="modalWindow-video-container">
							<div class="modalWindow-video">
								<iframe width="100%" height="100%" frameborder="0" allowfullscreen="1" data-src="{{ $template_movie['link_demo'] }}"></iframe>
							</div>
							<a class="close" role="button" data-dismiss="modal" style="margin-top: 16px;">
								<span aria-hidden="true" class="mbri-close mbr-iconfont closeModal"></span>
								<span class="sr-only">Close</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		@endforeach
	</section>


	<section class="mbr-section article content1 cid-qY9MlGDgBq" id="content1-3">
		<div class="container">
			<div class="media-container-row">
				<div class="mbr-text col-12 col-md-12">
					<h2 class="mbr-fonts-style block-title align-center display-2">Видео - Залог Успеха Вашего Сайта</h2>
				</div>
			</div>
		</div>
	</section>


	<section class="mbr-section article content9 cid-qY9MZqMrWS" id="content9-5">
		<div class="container">
			<div class="inner-container" style="width: 100%;">
				<div class="section-text align-center mbr-fonts-style display-5">Не важно каким бизнесом вы занимаетесь, наличие видео на сайте повышает уровень профессионализма, доверия со стороны потребителей, конверсию и в итоге, позволяет получать больше заказов и больше продавать</div>
			</div>
		</div>
	</section>


	<section class="features1 cid-qY9Mg9f2B8" id="features1-2">
		<div class="container">
			<div class="media-container-row">

				<div class="card p-3 col-12 col-md-6 col-lg-4">
					<div class="card-img pb-3">
						<span class="mbr-iconfont mbrib-touch" style="color: rgb(71, 199, 32);"></span>
					</div>
					<div class="card-box">
						<h4 class="card-title py-3 mbr-fonts-style display-5">
							Выберите шаблон видео</h4>
						<p class="mbr-text mbr-fonts-style display-7">Мы подготовили для вас целую библиотеку готовых, профессиональных шаблонов видео-роликов для разных видов бизнеса</p>
					</div>
				</div>

				<div class="card p-3 col-12 col-md-6 col-lg-4">
					<div class="card-img pb-3">
						<span class="mbr-iconfont mbri-magic-stick" style="color: rgb(71, 199, 32);"></span>
					</div>
					<div class="card-box">
						<h4 class="card-title py-3 mbr-fonts-style display-5">
							Добавьте ваш логотип</h4>
						<p class="mbr-text mbr-fonts-style display-7">
							Вам остается добавить логотип, адрес сайта и некоторые другие данные, чтобы создать ваше видео.</p>
					</div>
				</div>

				<div class="card p-3 col-12 col-md-6 col-lg-4">
					<div class="card-img pb-3">
						<span class="mbr-iconfont fa-film fa" style="color: rgb(71, 199, 32); font-size: 64px;"></span>
					</div>
					<div class="card-box">
						<h4 class="card-title py-3 mbr-fonts-style display-5">Получите готовый фильм</h4>
						<p class="mbr-text mbr-fonts-style display-7">
							Мы всего за 5 минут создадим для вас готовый рекламный видео-ролик, который вы сможете скачать и загрузить на youtube</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="tabs2 cid-qY9QyUTNgp" id="tabs2-8">
		<div class="container">
			<h2 class="mbr-section-title align-center pb-3 mbr-fonts-style display-2">Выберите Шаблон Видео</h2>
			<div class="media-container-row">
				<div class="col-12 col-md-12">
					<ul class="nav nav-tabs tablist_movies" role="tablist">
						<li class="nav-item"><a class="nav-link mbr-fonts-style show display-7" role="tab" data-toggle="tab" href="#tabs2-8_tab5" aria-selected="false" data-ids-show=".card-movie">
								Все</a></li>
						@foreach($movie_categories as $key => $movie_category)
						<li class="nav-item"><a class="nav-link mbr-fonts-style show display-7" role="tab" data-toggle="tab" href="#tabs2-8_tab5" aria-selected="false" data-ids-show=".card-movie{{ $key }}">
								{{ $movie_category }}</a></li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</section>



    @if ($template_movies)
        <section class="features3 cid-qL2Q4ZrQGI" id="create-movie">
            <div class="container">
                <h2 class="mbr-white align-center pb-5 mbr-fonts-style mbr-bold display-2" style="color: #232323; padding-bottom: 20px !important;">Выбери фильм для своего бизнеса</h2>
                @if (Session::has('message'))
                    <div class="alert alert-{{ Session::get('code') }}">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                @endif
                @php $count_curr_movie = 0 @endphp

                    @foreach ($template_movies as $key => $template_movie)
						@if($key == 0 || $key % 4 == 0)
                        @php $count_curr_movie++ @endphp
                        {{--<div class="media-container-row">--}}
                        @endif
                            <div class="notAnimate p-3 col-12 col-md-6 col-lg-3 @php foreach($template_movie['categories'] as $movie_cat) { echo 'card-movie' . $movie_cat . ' '; } @endphp card-movie">
                                <div class="card-wrapper">
                                    <div class="card-img">
                                        <img src="{{ $template_movie['path_image'] }}" alt="LetMeShare" title="">
                                    </div>
                                    <div class="card-box">
                                        <h4 class="card-title mbr-fonts-style display-7 title-template-movie">
                                            <br>
                                            {{ $template_movie['title'] }}
                                        </h4>
                                        <p class="mbr-text mbr-fonts-style display-7 desc-template-movie">
                                            {{ $template_movie['description'] }}
                                            <br>
                                        </p>
                                    </div>

                                    <div class="mbr-section-btn text-center">
                                        <a href="{{--@if($template_movie['price'] > 0 && !session()->get('template_movies.'.$template_movie['movie_id'].'.payment_confirmed')) {{ route('paypal.express-checkout', ['template_movie_id' => $template_movie['movie_id']]) }} @else--}} {{ url('movies/create?template=' . $template_movie['movie_id']) }} {{--@endif--}}" class="btn btn-primary display-4">Создать {{ $template_movie['price'] }}$</a>
                                        <a class="btn btn-primary-outline display-4 show-modal" data-iframe="{{ $template_movie['link_demo'] }}" data-name-modal="modalDemo" data-modal=".modalWindowDemo" onclick="$('.modalDemo').find('iframe').attr('data-src', $(this).attr('data-iframe')); updateModalVideos($(this));">Демо ролик</a>

                                    </div>
                                </div>
                            </div>
                            @if($key == $count_curr_movie * 4 - 1)
                            {{--</div>--}}
                            @endif
                    @endforeach
            </div>

            <div class="cid-qY9Krrhtcx">
                <div class="modalWindow modalDemo modalWindowDemo" style="display: none;" id="modal-movie">
                    <div class="modalWindow-container">
                        <div class="modalWindow-video-container">
                            <div class="modalWindow-video">
                                <iframe width="100%" height="100%" frameborder="0" allowfullscreen="1" data-src="https://www.youtube.com/watch?v=aSFNUIar"></iframe>
                            </div>
                            <a class="close" role="button" data-dismiss="modal">
                                <span aria-hidden="true" class="mbri-close mbr-iconfont closeModal"></span>
                                <span class="sr-only">Close</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif


    {{--
	<section class="features3 cid-qY9PpJJPAg" id="features3-6">
		<div class="container">
			<div class="media-container-row">
				<div class="notAnimate p-3 col-12 col-md-6 col-lg-3 card-movie1 card-movie">
					<div class="card-wrapper">
						<div class="card-img">
							<img src="{{ asset('letmeshare/assets/images/robot-492x276.png') }}" alt="Mobirise" title="">
						</div>
						<div class="card-box">
							<h4 class="card-title mbr-fonts-style display-7">Робот</h4>
							<p class="mbr-text mbr-fonts-style display-7">
								Идеально для услуг грузоперевозок, грузчиков, вывоза мусора, доставки мебели</p>
						</div>
						<div class="mbr-section-btn text-center"><a href="#" class="btn btn-primary display-4">&nbsp; &nbsp;Создать $19</a> <a class="btn btn-primary-outline display-4 show-modal" data-iframe="https://www.youtube.com/watch?v=a8-yk9NP758" data-name-modal="modalDemo" data-modal=".modalWindowDemo" onclick="$('.modalDemo').find('iframe').attr('data-src', $(this).attr('data-iframe')); updateModalVideos($(this));">Демо ролик</a></div>
					</div>
				</div>

				<div class="notAnimate p-3 col-12 col-md-6 col-lg-3 card-movie2 card-movie">
					<div class="card-wrapper">
						<div class="card-img">
							<img src="{{ asset('letmeshare/assets/images/silach-492x276.png') }}" alt="Mobirise" title="">
						</div>
						<div class="card-box">
							<h4 class="card-title mbr-fonts-style display-7">
								Силач</h4>
							<p class="mbr-text mbr-fonts-style display-7">
								Идеально для услуг грузоперевозок, грузчиков, вывоза мусора, доставки мебели</p>
						</div>
						<div class="mbr-section-btn text-center"><a href="#" class="btn btn-primary display-4">&nbsp; &nbsp;Создать $19&nbsp;</a> <a class="btn btn-primary-outline display-4 show-modal" data-iframe="https://www.youtube.com/watch?v=D4hAVemuQXY" data-name-modal="modalDemo" data-modal=".modalWindowDemo" onclick=" updateModalVideos($(this));">Демо ролик</a></div>
					</div>
				</div>

				<div class="notAnimate p-3 col-12 col-md-6 col-lg-3 card-movie3 card-movie">
					<div class="card-wrapper">
						<div class="card-img">
							<img src="{{ asset('letmeshare/assets/images/byk-492x275.png') }}" alt="Mobirise" title="">
						</div>
						<div class="card-box">
							<h4 class="card-title mbr-fonts-style display-7">
								Бык</h4>
							<p class="mbr-text mbr-fonts-style display-7">
								Идеально для услуг грузоперевозок, грузчиков, <em>вывоза</em> мусора, доставки мебели</p>
						</div>
						<div class="mbr-section-btn text-center"><a href="#" class="btn btn-primary display-4">Создать $19</a> <a href="#" class="btn btn-primary-outline display-4">Демо ролик</a></div>
					</div>
				</div>

				<div class="notAnimate p-3 col-12 col-md-6 col-lg-3 card-movie4 card-movie">
					<div class="card-wrapper">
						<div class="card-img">
							<img src="{{ asset('letmeshare/assets/images/car1-492x276.png') }}" alt="Mobirise" title="">
						</div>
						<div class="card-box">
							<h4 class="card-title mbr-fonts-style display-7">
								Погрузчик</h4>
							<p class="mbr-text mbr-fonts-style display-7">
								Идеально для услуг грузоперевозок, грузчиков, вывоза мусора, доставки мебели
							</p>
						</div>
						<div class="mbr-section-btn text-center"><a href="#" class="btn btn-primary display-4">Создать $19 &nbsp;</a> <a href="#" class="btn btn-primary-outline display-4">Демо ролик</a></div>
					</div>
				</div>
			</div>
		</div>



	<div class="cid-qY9Krrhtcx">
		<div class="modalWindow modalDemo modalWindowDemo" style="display: none;" id="modal-movie">
			<div class="modalWindow-container">
				<div class="modalWindow-video-container">
					<div class="modalWindow-video">
						<iframe width="100%" height="100%" frameborder="0" allowfullscreen="1" data-src="https://www.youtube.com/watch?v=yIZZ5IBV2AY"></iframe>
					</div>
					<a class="close" role="button" data-dismiss="modal">
						<span aria-hidden="true" class="mbri-close mbr-iconfont closeModal"></span>
						<span class="sr-only">Close</span>
					</a>
				</div>
			</div>
		</div>

	</div>

	</section>

	<section class="features3 cid-qYa0CSl3c1" id="features3-9">
		<div class="container">
			<div class="media-container-row">
				<div class="notAnimate p-3 col-12 col-md-6 col-lg-3 card-movie5 card-movie">
					<div class="card-wrapper">
						<div class="card-img">
							<img src="{{ asset('letmeshare/assets/images/car2-492x277.png') }}" alt="Mobirise" title="">
						</div>
						<div class="card-box">
							<h4 class="card-title mbr-fonts-style display-7">Машина</h4>
							<p class="mbr-text mbr-fonts-style display-7">
								Идеально для услуг грузоперевозок, грузчиков, вывоза мусора, доставки мебели</p>
						</div>
						<div class="mbr-section-btn text-center"><a href="#" class="btn btn-primary display-4">Создать $19 &nbsp;&nbsp;</a> <a href="#" class="btn btn-primary-outline display-4">Демо ролик</a></div>
					</div>
				</div>

				<div class="notAnimate p-3 col-12 col-md-6 col-lg-3 card-movie6 card-movie">
					<div class="card-wrapper">
						<div class="card-img">
							<img src="{{ asset('letmeshare/assets/images/cran-492x276.png') }}" alt="Mobirise" title="">
						</div>
						<div class="card-box">
							<h4 class="card-title mbr-fonts-style display-7">
								Кран</h4>
							<p class="mbr-text mbr-fonts-style display-7">
								Идеально для услуг грузоперевозок, грузчиков, вывоза мусора, доставки мебели</p>
						</div>
						<div class="mbr-section-btn text-center"><a href="#" class="btn btn-primary display-4">&nbsp; &nbsp;Создать &nbsp;$19</a> <a href="#" class="btn btn-primary-outline display-4">Демо ролик</a></div>
					</div>
				</div>

				<div class="notAnimate p-3 col-12 col-md-6 col-lg-3 card-movie7 card-movie">
					<div class="card-wrapper">
						<div class="card-img">
							<img src="{{ asset('letmeshare/assets/images/dom-492x276.png') }}" alt="Mobirise" title="">
						</div>
						<div class="card-box">
							<h4 class="card-title mbr-fonts-style display-7">
								Подъем на этаж</h4>
							<p class="mbr-text mbr-fonts-style display-7">
								Идеально для услуг грузоперевозок, грузчиков, <em>вывоза</em> мусора, доставки мебели</p>
						</div>
						<div class="mbr-section-btn text-center"><a href="#" class="btn btn-primary display-4">&nbsp; &nbsp;Создать $19&nbsp;</a> <a href="#" class="btn btn-primary-outline display-4">Демо ролик</a></div>
					</div>
				</div>

				<div class="notAnimate p-3 col-12 col-md-6 col-lg-3 card-movie8 card-movie">
					<div class="card-wrapper">
						<div class="card-img">
							<img src="{{ asset('letmeshare/assets/images/lampa-492x276.png') }}" alt="Mobirise" title="">
						</div>
						<div class="card-box">
							<h4 class="card-title mbr-fonts-style display-7">
								Сделай сам</h4>
							<p class="mbr-text mbr-fonts-style display-7">
								Идеально для услуг ремонта, сборки мебели, производства мебели на заказ, ремонта мебели</p>
						</div>
						<div class="mbr-section-btn text-center"><a href="#" class="btn btn-primary display-4">Создать $19 &nbsp;</a> <a href="#" class="btn btn-primary-outline display-4">Демо ролик</a></div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="features3 cid-qYa19NvOqU" id="features3-a">
		<div class="container">
			<div class="media-container-row">
				<div class="notAnimate p-3 col-12 col-md-6 col-lg-3 card-movie9 card-movie">
					<div class="card-wrapper">
						<div class="card-img">
							<img src="{{ asset('letmeshare/assets/images/ventilator-492x277.png') }}" alt="Mobirise" title="">
						</div>
						<div class="card-box">
							<h4 class="card-title mbr-fonts-style display-7">Вентиляция</h4>
							<p class="mbr-text mbr-fonts-style display-7">
								Идеально для услуг электриков, сборки мебели, услуг вентиляции</p>
						</div>
						<div class="mbr-section-btn text-center"><a href="#" class="btn btn-primary display-4">Создать $19</a> <a href="#" class="btn btn-primary-outline display-4">Демо ролик</a></div>
					</div>
				</div>

				<div class="notAnimate p-3 col-12 col-md-6 col-lg-3 card-movie10 card-movie">
					<div class="card-wrapper">
						<div class="card-img">
							<img src="{{ asset('letmeshare/assets/images/lamp-492x276.png') }}" alt="Mobirise" title="">
						</div>
						<div class="card-box">
							<h4 class="card-title mbr-fonts-style display-7">
								Электрика</h4>
							<p class="mbr-text mbr-fonts-style display-7">
								Идеально для услуг электриков, квартирного ремонта, ремонта техники</p>
						</div>
						<div class="mbr-section-btn text-center"><a href="#" class="btn btn-primary display-4">Создать $19&nbsp;</a> <a href="#" class="btn btn-primary-outline display-4">Демо ролик</a></div>
					</div>
				</div>

				<div class="notAnimate p-3 col-12 col-md-6 col-lg-3 card-movie11 card-movie">
					<div class="card-wrapper">
						<div class="card-img">
							<img src="{{ asset('letmeshare/assets/images/stroika-492x276.png') }}" alt="Mobirise" title="">
						</div>
						<div class="card-box">
							<h4 class="card-title mbr-fonts-style display-7">Стройка</h4>
							<p class="mbr-text mbr-fonts-style display-7">
								Идеально для услуг строительства домов, коттеджного строительства</p>
						</div>
						<div class="mbr-section-btn text-center"><a href="#" class="btn btn-primary display-4">Создать $19</a> <a href="#" class="btn btn-primary-outline display-4">Демо ролик</a></div>
					</div>
				</div>

				<div class="notAnimate p-3 col-12 col-md-6 col-lg-3 card-movie12 card-movie">
					<div class="card-wrapper">
						<div class="card-img">
							<img src="{{ asset('letmeshare/assets/images/sceme-492x275.png') }}" alt="Mobirise" title="">
						</div>
						<div class="card-box">
							<h4 class="card-title mbr-fonts-style display-7">Дизайн</h4>
							<p class="mbr-text mbr-fonts-style display-7">
								Идеально для услуг квартирного ремонта, проектирования дизайна</p>
						</div>
						<div class="mbr-section-btn text-center"><a href="#" class="btn btn-primary display-4">Создать $19</a> <a href="#" class="btn btn-primary-outline display-4">Демо ролик</a></div>
					</div>
				</div>
			</div>
		</div>
	</section> --}}

	<section class="mbr-section info5 cid-qYa2YaC8V4" id="info5-b">
		<div class="container">
			<div class="row justify-content-center content-row">
				<div class="media-container-column title col-12 col-lg-7 col-md-6">
					<h3 class="mbr-section-subtitle align-left mbr-light pb-3 mbr-fonts-style display-5">
						Поторопись! Предложение ограничено по времени&nbsp;</h3>
					<h2 class="align-left mbr-bold mbr-fonts-style mbr-white mbr-section-title display-2">Сегодня каждое видео</h2>
				</div>
				<div class="media-container-column col-12 col-lg-3 col-md-4">
					<div class="mbr-section-btn align-right py-4"><a class="btn btn-white-outline display-2" href=" ">$19</a></div>
				</div>
			</div>
		</div>
	</section>

	<section class="toggle1 cid-qYa3vd38Y8" id="toggle1-c">
		<div class="container">
			<div class="media-container-row">
				<div class="col-12 col-md-8">
					<div class="section-head text-center space30">
						<h2 class="mbr-section-title pb-5 mbr-fonts-style display-2">Частые Вопросы</h2>
					</div>
					<div class="clearfix"></div>
					<div id="bootstrap-toggle" class="toggle-panel accordionStyles tab-content">
						<div class="card">
							<div class="card-header" role="tab" id="headingOne">
								<a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-parent="#accordion" data-core="" href="#collapse1_10" aria-expanded="false" aria-controls="collapse1">
									<h4 class="mbr-fonts-style display-5">
										<span class="sign mbr-iconfont mbri-arrow-down inactive"></span>В каком формате я получу видео?</h4>
								</a>
							</div>
							<div id="collapse1_10" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body p-4">
									<p class="mbr-fonts-style panel-text display-7">
										Вы получите видео в формате mp4 в разрешении 720р</p>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" role="tab" id="headingTwo">
								<a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-parent="#accordion" data-core="" href="#collapse2_10" aria-expanded="false" aria-controls="collapse2">
									<h4 class="mbr-fonts-style display-5">
										<span class="sign mbr-iconfont mbri-arrow-down inactive"></span>Как мне дальше использовать видео?</h4>
								</a>

							</div>
							<div id="collapse2_10" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body p-4">
									<p class="mbr-fonts-style panel-text display-7">
										Вы можете загрузить видео на Youtube, Vimeo или любой другой видео-сервис, и получить там ссылку для вставки видео на вашем сайте</p>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" role="tab" id="headingThree">
								<a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-parent="#accordion" data-core="" href="#collapse3_10" aria-expanded="false" aria-controls="collapse3">
									<h4 class="mbr-fonts-style display-5">
										<span class="sign mbr-iconfont mbri-arrow-down inactive"></span>Видео принадлежит мне?</h4>
								</a>
							</div>
							<div id="collapse3_10" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingThree">
								<div class="panel-body p-4">
									<p class="mbr-fonts-style panel-text display-7">Да, права на видео полностью принадлежат вам по лицензии Common Creatives, вы можете беспрепятственно использовать видео на сайте</p>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" role="tab" id="headingThree">
								<a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-parent="#accordion" data-core="" href="#collapse4_10" aria-expanded="false" aria-controls="collapse4">
									<h4 class="mbr-fonts-style display-5">
										<span class="sign mbr-iconfont mbri-arrow-down inactive"></span>Где я могу использовать видео?</h4>
								</a>
							</div>
							<div id="collapse4_10" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingThree">
								<div class="panel-body p-4">
									<p class="mbr-fonts-style panel-text display-7">
										Где угодно, например, на вашем сайте, в видео-рекламе на Facebook, в социальных сетях, в сообщениях ваших мессенджеров.</p>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" role="tab" id="headingThree">
								<a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-parent="#accordion" data-core="" href="#collapse5_10" aria-expanded="false" aria-controls="collapse5">
									<h4 class="mbr-fonts-style display-5">
										<span class="sign mbr-iconfont mbri-arrow-down inactive"></span>Сколько времени занимает создание видео?</h4>
								</a>
							</div>
							<div id="collapse5_10" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingThree">
								<div class="panel-body p-4">
									<p class="mbr-fonts-style panel-text display-7">
										В зависимости от загруженности сервера от 1 до 5 минут. В любом случае мы вышлем вам готовое видео на почту, оно также будет доступно в вашем личном кабинете на сайте.</p>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" role="tab" id="headingThree">
								<a role="button" class="collapsed panel-title text-black" data-toggle="collapse" data-parent="#accordion" data-core="" href="#collapse6_10" aria-expanded="false" aria-controls="collapse6">
									<h4 class="mbr-fonts-style display-5">
										<span class="sign mbr-iconfont mbri-arrow-down inactive"></span>Сколько стоит создать видео?</h4>
								</a>
							</div>
							<div id="collapse6_10" class="panel-collapse noScroll collapse" role="tabpanel" aria-labelledby="headingThree">
								<div class="panel-body p-4">
									<p class="mbr-fonts-style panel-text display-7">
										Вы можете создать на нашем сервисе готовый рекламный ролик всего за 19 долларов! Создание такого видео в видео-студии будет стоить более 500 долларов, но с помощью нашего сервиса сегодня вы получите его всего за 19 долларов. Поторопитесь, цена может измениться без предупреждения.<br>Обратите внимание, деньги за услугу не возвращаются</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>



	</section>





	{{-- OLD --}}




	{{--<section class="header8 cid-qL2OfBq22Q mbr-fullscreen mbr-parallax-background" id="header8-2">--}}
	{{--<div class="mbr-overlay" style="opacity: 0.7; background-color: rgb(7, 59, 76);"></div>--}}

	{{--<div class="container align-center">
        <div class="row justify-content-md-center">
            <div class="mbr-white col-md-10">
                <h1 class="mbr-section-title align-center py-2 mbr-bold mbr-fonts-style display-1">{{ __('Поделись своей верой') }}</h1>
                <p class="mbr-text align-center py-2 mbr-fonts-style display-5">
                    {{ __('Отправь другу специально созданный для него фильм,') }}
                    <br>
                    {{ __('чтобы поделиться своей верой и рассказать об Иисусе Христе') }}
                </p>

                <div class="mbr-media show-modal align-center py-2" data-modal=".modalWindow">
                    <span class="mbri-play mbr-iconfont"></span>
                </div>

                @foreach ($template_movies as $template_movie)
                    <div class="mbr-media show-modal align-center py-2" id="show-modal-{{ $template_movie['movie_id'] }}" data-modal=".modalWindow-{{ $template_movie['movie_id'] }}" style="display: none;">
                        <span class="mbri-play mbr-iconfont"></span>
                    </div>
                @endforeach

                <div class="icon-description align-center font-italic pb-3 mbr-fonts-style display-7">
                    Нажми кнопку, чтобы узнать как это работает!
                </div>
            </div>
        </div>
    </div> --}}


	{{--<div>
			<div class="modalWindow" style="display: none; margin-top: 30px;">
				<div class="modalWindow-container">
					<div class="modalWindow-video-container">
						<div class="modalWindow-video">
							<iframe width="100%" height="100%" frameborder="0" allowfullscreen="1" data-src="http://www.youtube.com/watch?v=uNCr7NdOJgw"></iframe>
						</div>
						<a class="close" role="button" data-dismiss="modal" style="margin-top: 16px;">
							<span aria-hidden="true" class="mbri-close mbr-iconfont closeModal"></span>
							<span class="sr-only">Close</span>
						</a>
					</div>
				</div>
			</div>
		</div>--}}

{{--

	<section class="features1 cid-qLwuifOO95" id="features1-l">
		<div class="container">
			<div class="media-container-row">

				<div class="card p-3 col-12 col-md-6 col-lg-4">
					<div class="card-img pb-3">
						<span class="mbr-iconfont mbrib-touch"></span>
					</div>
					<div class="card-box">
						<h4 class="card-title py-3 mbr-fonts-style display-5">Выбери фильм</h4>
						<p class="mbr-text mbr-fonts-style display-7">
							Мы предлагаем несколько разных тематик фильмов, выбери тот, который больше подойдет твоему другу</p>
					</div>
				</div>

				<div class="card p-3 col-12 col-md-6 col-lg-4">
					<div class="card-img pb-3">
						<span class="mbr-iconfont mbri-camera"></span>
					</div>
					<div class="card-box">
						<h4 class="card-title py-3 mbr-fonts-style display-5">
							Загрузи фото друга</h4>
						<p class="mbr-text mbr-fonts-style display-7">
							Загрузи фотографию твоего друга, укажи другую необходимую информацию, чтобы подготовить фильм</p>
					</div>
				</div>

				<div class="card p-3 col-12 col-md-6 col-lg-4">
					<div class="card-img pb-3">
						<span class="mbr-iconfont mbri-share"></span>
					</div>
					<div class="card-box">
						<h4 class="card-title py-3 mbr-fonts-style display-5">Отправь фильм</h4>
						<p class="mbr-text mbr-fonts-style display-7">
							Отправь ссылку на фильм твоему другу, перейдя на наш сайт он увидит фильм, а ты поделишься с ним своей верой в Бога</p>
					</div>
				</div>

			</div>
		</div>
	</section>
--}}

	{{--@if ($template_movies)
	<section class="features3 cid-qL2Q4ZrQGI" id="create-movie">
		<div class="container">
			<h2 class="mbr-white align-center pb-5 mbr-fonts-style mbr-bold display-2" style="color: #232323; padding-bottom: 20px !important;">Выбери фильм для друга</h2>
			@if (Session::has('message'))
				<div class="alert alert-{{ Session::get('code') }}">
					<p>{{ Session::get('message') }}</p>
				</div>
			@endif
			<div class="media-container-row">
				@foreach ($template_movies as $template_movie)
					<div class="card p-3 col-12 col-md-6 col-lg-4">
						<div class="card-wrapper">
							<div class="card-img">
								<img src="{{ $template_movie['path_image'] }}" alt="LetMeShare" title="">
							</div>
							<div class="card-box">
								<h4 class="card-title mbr-fonts-style display-7">
									<strong>{{ $template_movie['title'] }}</strong>
								</h4>
								<p class="mbr-text mbr-fonts-style display-7">
									{{ $template_movie['description'] }}
									<br>
									<strong>Цена: @currency_format($template_movie['price'])</strong>
								</p>
							</div>
							<div class="mbr-section-btn text-center">
								<a href="@if($template_movie['price'] > 0 && !session()->get('template_movies.'.$template_movie['movie_id'].'.payment_confirmed')) {{ route('paypal.express-checkout', ['template_movie_id' => $template_movie['movie_id']]) }} @else {{ url('movies/create?template=' . $template_movie['movie_id']) }} @endif" class="btn btn-primary display-4">Создать фильм</a>
								<a href="" class="btn btn-primary-outline display-4 youtube" data-button="#show-modal-{{ $template_movie['movie_id'] }}">Смотреть демо</a>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</section>
	@endif--}}



	<section class="testimonials1 cid-qYa7OhPmFR" id="testimonials1-e">
		<div class="container">
			<div class="media-container-row">
				<div class="title col-12 align-center">
					<h2 class="pb-3 mbr-fonts-style display-2">
						Отзывы Предпринимателей</h2>
					<h3 class="mbr-section-subtitle mbr-light pb-3 mbr-fonts-style display-5">Ваш бизнес выиграет, если у вас будет хорошее видео на сайте</h3>
				</div>
			</div>
		</div>

		<div class="container pt-3 mt-2">
			<div class="media-container-row">
				<div class="mbr-testimonial p-3 align-center col-12 col-md-6 col-lg-4">
					<div class="panel-item p-3">
						<div class="card-block">
							<div class="testimonial-photo">
								<img src="{{ asset('letmeshare/assets/images/face1.jpg') }}">
							</div>
							<p class="mbr-text mbr-fonts-style display-7">За 5 минут сделала потрясающее видео на для сайта, спасибо, я очень довольна!</p>
						</div>
						<div class="card-footer">
							<div class="mbr-author-name mbr-bold mbr-fonts-style display-7">
								Яна</div>
							<small class="mbr-author-desc mbr-italic mbr-light mbr-fonts-style display-7">
								Студия дизайна</small>
						</div>
					</div>
				</div>

				<div class="mbr-testimonial p-3 align-center col-12 col-md-6 col-lg-4">
					<div class="panel-item p-3">
						<div class="card-block">
							<div class="testimonial-photo">
								<img src="{{ asset('letmeshare/assets/images/anton-240x227.jpg') }}" alt="" title="">
							</div>
							<p class="mbr-text mbr-fonts-style display-7">
								Рад, что мне подсказали этот сервис. Сделал видео для своего сайта грузоперевозок по Эстонии</p>
						</div>
						<div class="card-footer">
							<div class="mbr-author-name mbr-bold mbr-fonts-style display-7">
								Антон</div>
							<small class="mbr-author-desc mbr-italic mbr-light mbr-fonts-style display-7">
								Грузоперевозки</small>
						</div>
					</div>
				</div>

				<div class="mbr-testimonial p-3 align-center col-12 col-md-6 col-lg-4">
					<div class="panel-item p-3">
						<div class="card-block">
							<div class="testimonial-photo">
								<img src="{{ asset('letmeshare/assets/images/face3.jpg') }}">
							</div>
							<p class="mbr-text mbr-fonts-style display-7">
								Сделали сайт, но чего-то не хватало, увидели рекламу сервиса, после чего создали видео, спасибо создателям!</p>
						</div>
						<div class="card-footer">
							<div class="mbr-author-name mbr-bold mbr-fonts-style display-7">
								Татьяна</div>
							<small class="mbr-author-desc mbr-italic mbr-light mbr-fonts-style display-7">
								Ремонт квартир</small>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>





	{{--<section class="mbr-section info2 cid-qLP49FpoqZ" id="info2-2t">
		<div class="mbr-overlay" style="opacity: 0.8; background-color: rgb(7, 59, 76);"></div>

		<div class="container">
			<div class="row main justify-content-center">
				<div class="media-container-column col-12 col-lg-3 col-md-4">
					<div class="mbr-section-btn align-left py-4"><a class="btn btn-primary display-4" href="https://letmegive.org">
							ПОДДЕРЖАТЬ</a></div>
				</div>
				<div class="media-container-column title col-12 col-lg-7 col-md-6">
					<h2 class="align-right mbr-bold mbr-white pb-3 mbr-fonts-style display-5"><span style="font-weight: normal;">Понравился проект? &nbsp;Поддержите нас!</span></h2>
					<h3 class="mbr-section-subtitle align-right mbr-light mbr-white mbr-fonts-style display-7">Рендеринг каждого видео стоит денег, &nbsp;вы можете бесплатно отправлять видео только потому, что есть те, кто жертвуют финансы на эту задачу. Поддержите проект финансово при помощи христианской краудфандинговой платформы Let Me Give, чтобы еще многие христиане смогли поделиться своей верой с друзьями</h3>
				</div>
			</div>
		</div>
	</section>--}}






@endsection

@section('javascript')
	<script>
		$(function () {
			if (window.location.hash.length > 0) {
				console.log(window.location.hash)
				var elem = $('#' + window.location.hash.replace('#', ''));
				if (elem) {
					$('body,html').animate({
						scrollTop: $(elem).offset().top - 65
					}, 500);
				}
			}

			$(document).on('click', 'a.youtube', function (e) {
			    e.preventDefault();
			    var modal_window = $(this).attr('data-button');

			    $(modal_window).trigger('click');
			});
		});
	</script>
@endsection

@section('stylesheet')
	<style>
		@foreach($template_movies as $template_movie)
			.cid-qL2OfBq22Q .modalWindow-{{ $template_movie['movie_id'] }} {
				position: fixed;
				z-index: 5000;
				left: 0;
				top: 0;
				background-color: rgba(61, 61, 61, 0.65);
				width: 100%;
				height: 100%;
			}
			.cid-qL2OfBq22Q .modalWindow-{{ $template_movie['movie_id'] }} .modalWindow-container {
				display: table-cell;
				vertical-align: middle;
			}
			.cid-qL2OfBq22Q .modalWindow-{{ $template_movie['movie_id'] }} .modalWindow-video {
				height: calc(44.9943757vw);
				width: 80vw;
				margin: 0 auto;
			}
		@endforeach
	</style>
@endsection
