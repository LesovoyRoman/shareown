@extends('letmeshare.layouts.master', [
	'title' => 'Художественный фильм',
	'scripts' => [
		['letmeshare/assets/bootstrapcarouselswipe/bootstrap-carousel-swipe.js', 'async defer'],
		['letmeshare/assets/mbr-testimonials-slider/mbr-testimonials-slider.js', 'async defer']
	],
	'styles' => [
		'letmeshare/assets/bootstrap-material-design-font/css/material.css'
	]
])

@section('content')
	<section class="mbr-section content5 cid-qLwYSR1cEW" id="content5-1d">
		<div class="container">
			<div class="media-container-row">
				<div class="title col-12 col-md-8">
					<h2 class="align-center mbr-bold mbr-white pb-3 mbr-fonts-style display-1">
						Твой фильм готов</h2>
					<h3 class="mbr-section-subtitle align-center mbr-light mbr-white pb-3 mbr-fonts-style display-5">
						Отправь ссылку на него другу по электронной почте</h3>
				</div>
			</div>
		</div>
	</section>

	<section class="mbr-section content4 cid-qLx6T46TID" id="content4-1g">
		<div class="container">
			<div class="media-container-row">
				<div class="title col-12 col-md-8">
					<h3 class="mbr-section-subtitle align-center mbr-light mbr-fonts-style display-5">
						<a href="{{ url('movies/' . $movie_data->id . '/show') }}">{{ url('movies/' . $movie_data->id . '/show') }}</a>
					</h3>
				</div>
			</div>
		</div>
	</section>

	<section class="header7 cid-qL2T8N7yap" id="header7-d">
		<div class="container">
			<div class="media-container-row">
				<div class="media-content align-right">
					<div class="mbr-section-text mbr-white pb-3">
						<p class="mbr-text mbr-fonts-style display-5">
							<br>Мы подготовили фильм о твоем друге. Теперь дело за тобой - отправь фильм другу и поделись своей верой в Иисуса&nbsp;<br></p>
					</div>
						<a class="btn btn-md btn-primary display-4" id="share-via-email"><span class="mdi-communication-email mbr-iconfont mbr-iconfont-btn" style="font-size: 16px;"></span>Отправить</a>
					</div>

					<div class="mbr-figure" style="width: 125%;">
						<iframe class="mbr-embedded-video" src="{{ $movie_data->link_youtube }}" width="1280" height="720" frameborder="0" allowfullscreen></iframe>
					</div>
			</div>
		</div>
	</section>

	{{-- @todo: QuotesController --}}
	<section class="carousel slide testimonials-slider cid-qLwZKyR7Xg" id="testimonials-slider1-1e">
		<div class="container text-center">
			<h2 class="pb-5 mbr-fonts-style display-2">Бери пример Благовестия с мужей Божьих</h2>

			<div class="carousel slide" data-ride="carousel" role="listbox">
				<div class="carousel-inner">
					<div class="carousel-item">
						<div class="user col-md-8">
							<div class="user_image">
								<img src="{{ asset('letmeshare/assets/images/john-stott-300x300.jpg') }}" alt="" title="">
							</div>
							<div class="user_text pb-3">
								<p class="mbr-fonts-style display-7">
									Джон Стотт сказал: «„Благовествовать“ … означает не „приобретать души“ … а просто провозглашать Благую весть, независимо от результатов»
								</p>
							</div>
							<div class="user_name mbr-bold pb-2 mbr-fonts-style display-7">
								Джон Стотт</div>
							<div class="user_desk mbr-light mbr-fonts-style display-7">Британский теолог и священник англиканской церкви, пионер евангельского движения</div>
						</div>
					</div><div class="carousel-item">
						<div class="user col-md-8">
							<div class="user_image">
								<img src="{{ asset('letmeshare/assets/images/3691-oswald-j-smith-197x240.jpg') }}" alt="" title="">
							</div>
							<div class="user_text pb-3">
								<p class="mbr-fonts-style display-7">
									Ты должен идти или послать себе заменy
								</p>
							</div>
							<div class="user_name mbr-bold pb-2 mbr-fonts-style display-7">Освальд Дж. Смитт</div>
							<div class="user_desk mbr-light mbr-fonts-style display-7">
								Канадский писатель и проповедник</div>
						</div>
					</div><div class="carousel-item">
						<div class="user col-md-8">
							<div class="user_image">
								<img src="{{ asset('letmeshare/assets/images/-4-225x225.jpg') }}" alt="" title="">
							</div>
							<div class="user_text pb-3">
								<p class="mbr-fonts-style display-7">Бог не столько использует христиан с даром благовестия, сколько верность тысяч и миллионов христиан, которые никогда не скажут что благовестие их дар
								</p>
							</div>
							<div class="user_name mbr-bold pb-2 mbr-fonts-style display-7">Марк Девер</div>
							<div class="user_desk mbr-light mbr-fonts-style display-7">Христианский пастор, автор книг</div>
						</div>
					</div><div class="carousel-item">
						<div class="user col-md-8">
							<div class="user_image">
								<img src="{{ asset('letmeshare/assets/images/-5-180x235.jpg') }}" alt="" title="">
							</div>
							<div class="user_text pb-3">
								<p class="mbr-fonts-style display-7">Благовествование не состоит главным образом в том, чтобы идти и стучаться в двери дома, пытаясь за пять или даже меньше минут убедить в истине людей, совершенно не испытывающих своей нужды во Христе, а затем мчаться к следующему дому. Чтобы благовествование действительно сохраняло свой смысл и значение, оно должно выражаться во всем нашем образе жизни
								</p>
							</div>
							<div class="user_name mbr-bold pb-2 mbr-fonts-style display-7">Моррис Венден</div>
							<div class="user_desk mbr-light mbr-fonts-style display-7">
								Американский проповедник</div>
						</div>
					</div><div class="carousel-item">
						<div class="user col-md-8">
							<div class="user_image">
								<img src="{{ asset('letmeshare/assets/images/-6-225x225.jpg') }}" alt="" title="">
							</div>
							<div class="user_text pb-3">
								<p class="mbr-fonts-style display-7">Для меня все благовестие заключается в четырех словах, а именно: "Бог умер за меня
								</p>
							</div>
							<div class="user_name mbr-bold pb-2 mbr-fonts-style display-7">Чарльз Сперджен</div>
							<div class="user_desk mbr-light mbr-fonts-style display-7">Английский проповедник и богослов, пастор крупнейшей баптистской церкви Англии</div>
						</div>
					</div><div class="carousel-item">
						<div class="user col-md-8">
							<div class="user_image">
								<img src="{{ asset('letmeshare/assets/images/grem-160x166.jpg') }}" alt="" title="">
							</div>
							<div class="user_text pb-3">
								<p class="mbr-fonts-style display-7">Я представляю величайший продукт в мире. Почему его нельзя продвигать так же, как мыло?
								</p>
							</div>
							<div class="user_name mbr-bold pb-2 mbr-fonts-style display-7">Билли Грэм</div>
							<div class="user_desk mbr-light mbr-fonts-style display-7">
								Величайший проповедник</div>
						</div>
					</div><div class="carousel-item">
						<div class="user col-md-8">
							<div class="user_image">
								<img src="{{ asset('letmeshare/assets/images/tereza-331x327.jpg') }}" alt="" title="">
							</div>
							<div class="user_text pb-3">
								<p class="mbr-fonts-style display-7">Наша миссия заключается в том, чтобы передавать любовь Божью, - не мертвого Бога, но Бога живого, Бога любви
								</p>
							</div>
							<div class="user_name mbr-bold pb-2 mbr-fonts-style display-7">Мать Тереза</div>
							<div class="user_desk mbr-light mbr-fonts-style display-7">
								Католическая монахиня, основательница женской монашеской конгрегации «Сёстры миссионерки любви», Лауреат Нобелевской премии мира</div>
						</div>
					</div></div>

				<div class="carousel-controls">
					<a class="carousel-control-prev" role="button" data-slide="prev">
						<span aria-hidden="true" class="mbri-arrow-prev mbr-iconfont"></span>
						<span class="sr-only">Previous</span>
					</a>

					<a class="carousel-control-next" role="button" data-slide="next">
						<span aria-hidden="true" class="mbri-arrow-next mbr-iconfont"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</section>

	<div class="modal" tabindex="-1" role="dialog" id="share-movie-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<div class="alert" style="display: none;"></div>

					<div class="row">
						<div class="col-md-12">
							{!! Form::open(['route' => 'movies.shareViaEmail', 'id' => 'share-movie-form', 'class' => 'form-horizontal']) !!}
								{!! Form::hidden('movie_id', $movie_data->id) !!}

								{!! Form::label('age', 'Введите E-Mail адрес друга', ['class'=>'control-label']) !!}
								<div>
									<span class="string_error_field">@if ($errors->has('email')) {{ $errors->first('email') }} @endif</span>
									{!! Form::email('email', '', ['class'=>('form-control' . ($errors->has('email') ? ' error_field' : ''))]) !!}
								</div>

								<div class="text-center">
									{!! Form::button( __('Отправить') , ['class' => 'btn btn-primary']) !!}
								</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('javascript')
	<script>
        fbq('track', 'Purchase', {
            value: 1,
            currency: 'USD',
        });

		$(function () {
			$('#share-via-email').on('click', function (e) {
				e.preventDefault();

				$.map($('#share-movie-form').find('label, input[type="email"], button'), function (item) {
					$(item).removeClass('hidden');
				});

				$('#share-movie-modal').modal();
			});

			$('#share-movie-form button').on('click', function () {
				var $this = $(this);
				var $email_field = $this.closest('form').find('input[type="email"]');

				function emailValidation(elem) {
					return /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(elem);
				}

				if (!emailValidation($.trim($email_field.val()))) {
					$email_field.addClass('error_field');
					$email_field.siblings('.string_error_field').text('Введите корректный email');

					return false;
				}

				$.ajax({
					type: "POST",
					dataType: "json",
					url: "{{ url('/movies/shareViaEmail') }}",
					data: $('#share-movie-form').serialize(),
					beforeSend: function () {
						$this.attr('disabled', true);
						$this.closest('form').find('.string_error_field').text('');
						$this.closest('form').find('input').removeClass('error_field');
						$('#share-movie-modal').find('.alert').removeClass('alert-success alert-danger').hide();
					},
					error: function (error) {
						$this.attr('disabled', false);
						$('#share-movie-modal').find('.alert').addClass('alert-danger').text('Возникла ошибка при отправке письма').show();
						console.error(error);
					},
					success: function (response) {
						$this.attr('disabled', false);

						if (response.status == 200) {
							$('#share-movie-modal').find('.alert').addClass('alert-success').text(response.msg).show();

							setTimeout(function () {
								$('#share-movie-modal').modal('hide');
							}, 2000);
						} else {
							$('#share-movie-modal').find('.alert').addClass('alert-danger').text(response.msg).show();
						}
					}
				})
			})
		});
	</script>
@endsection
