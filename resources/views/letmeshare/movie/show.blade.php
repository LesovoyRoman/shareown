@extends('letmeshare.layouts.movieshow', [
	'title' => 'Художественный фильм',
	'scripts' => [
		['letmeshare/assets/formoid/formoid.min.js', 'async defer']
	],
	'styles' => []
])

@section('content')
	<section class="mbr-section content5 cid-qLwTYYnQhU" id="content5-1a" style="padding-top: 40px;">
		<div class="container">
			<div class="media-container-row">
				<div class="title col-12 col-md-8">
					<h2 class="align-center mbr-bold mbr-white pb-3 mbr-fonts-style display-1">{{ $movie_data->title }}</h2>
					<h3 class="mbr-section-subtitle align-center mbr-light mbr-white pb-3 mbr-fonts-style display-5">Похоже, что твой друг приготовил для тебя фильм</h3>
				</div>
			</div>
		</div>
	</section>

	<section class="cid-qLwOdzZrW3" id="video1-15">
		<figure class="mbr-figure align-center">
			<div class="video-block" style="width: 100%;">
				<div>
					<iframe class="mbr-embedded-video" src="{{ $movie_data->link_youtube }}" width="1280" height="720" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</figure>
	</section>

	<section class="testimonials3 cid-qLwU4ibVtI" id="testimonials3-1b">
		<div class="container">
			<div class="media-container-row">
				<div class="media-content px-3 align-self-center mbr-white py-2">
					<p class="mbr-text testimonial-text mbr-fonts-style display-7">
						Я часто злился, и у меня была огромная дыра в сердце. Ничто меня не радовало. Затем я женился на богобоязненной женщине, и дома она каждое утро читала Библию. Спустя какое-то время она сказала: "Ты хотите, чтобы я почитала тебе вслух?". Тогда я сел, и она стала читать мне Библию вслух каждое утро. В конце концов я сказал: "Хорошо, дай теперь мне прочитать", и теперь уже я начал читать ее вслух. Тогда это было похоже на то, как будто  Господь сказал мне: "Чак, пора возвращаться домой. Это длилось достаточно долго". Теперь мое сердце снова наполняется
					</p>
					<p class="mbr-author-name pt-4 mb-2 mbr-fonts-style display-7">
						Чак Норрис, из интервью&nbsp;на сцене на съезде NRB</p>
					<p class="mbr-author-desc mbr-fonts-style display-7">
						Голливудский актер</p>
				</div>

				<div class="mbr-figure pl-lg-5" style="width: 135%;">
					<img src="{{ asset('letmeshare/assets/images/746-chack-794540-1003x746.jpg') }}" alt="" title="">
				</div>
			</div>
		</div>
	</section>

	<section class="testimonials5 cid-qLwVDB2cER" id="testimonials5-1c">
		<div class="container">
			<div class="media-container-row">
				<div class="title col-12 align-center">
					<h2 class="pb-3 mbr-fonts-style display-2">
						Бог ждет тебя!</h2>
					<h3 class="mbr-section-subtitle mbr-light pb-3 mbr-fonts-style display-5">
						Все больше и больше людей в мире отдают свое сердце Иисусу Христу. Многие политики, ученые, философы, кинозвезды и миллиарды людей каждый день приходят к Богу</h3>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="media-container-column">
				<div class="mbr-testimonial align-center col-12 col-md-10">
					<div class="panel-item">
						<div class="card-block">
							<div class="testimonial-photo">
								<img src="{{ asset('letmeshare/assets/images/1a35077d5fe0a4faad3e103dda3f0443-cropped-460x460-460x460.jpg') }}" alt="" title="">
							</div>
							<p class="mbr-text mbr-fonts-style mbr-white display-7">
								Около 13 лет назад я пришел к очень сложной точке, и именно размышления о Страданиях Христа вывели меня из кризиса.&nbsp;Жизнь такова, что на каждом из нас она оставляет свои шрамы. И я пришел к ранам Христа в поисках исцеления своих ран. И я начал видеть то, через что Он прошел. И начал понимать это так, как никогда не понимал раньше, несмотря на то, что я слышал эту историю множество раз</p>
						</div>
						<div class="card-footer">
							<div class="mbr-author-name mbr-bold mbr-fonts-style mbr-white display-7">
								Мел Гибсон</div>
							<small class="mbr-author-desc mbr-italic mbr-light mbr-fonts-style mbr-white display-7">
								Голивудский актер, режисер</small>
						</div>
					</div>
				</div>
				<div class="mbr-testimonial align-center col-12 col-md-10">
					<div class="panel-item">
						<div class="card-block">
							<div class="testimonial-photo">
								<img src="{{ asset('letmeshare/assets/images/images-4-229x220.jpg') }}" alt="" title="">
							</div>
							<p class="mbr-text mbr-fonts-style mbr-white display-7">В наше время такие заявления о переосмысливании жизни и покаянии не актуальны и СМИ не выгодны, так же и с политической стороны это достаточно опасно.<br>Поэтому я решил опубликовать это у себя на страничке в Facebook, я решил поделиться этими прекрасными переживаниями и принятым решением в жизни со своими друзьями и поклонниками, ведь одному спасенному человеку радуются Небеса.<br>Я хотел бы поделиться этой новостью с каждым, с любовью к вам, ваш брат Сильвестр</p>
						</div>
						<div class="card-footer">
							<div class="mbr-author-name mbr-bold mbr-fonts-style mbr-white display-7">
								Сильвестр Сталлоне</div>
							<small class="mbr-author-desc mbr-italic mbr-light mbr-fonts-style mbr-white display-7">
								Голливудский актер</small>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="mbr-section form1 cid-qLwOCG8W2E" id="form1-18">
		<div class="container">
			<div class="row justify-content-center">
				<div class="title col-12 col-lg-8">
					<h2 class="mbr-section-title align-center pb-3 mbr-fonts-style display-2">
						Хочешь узнать больше о Боге?</h2>
					<h3 class="mbr-section-subtitle align-center mbr-light pb-3 mbr-fonts-style display-5">Оставь заявку и мы вышлем тебе совершенно <strong>бесплатно</strong> Новый завет, Библию или другую полезную литературу о Боге&nbsp;</h3>
				</div>
			</div>
		</div>

		{{-- @todo: add possibility to send emails --}}
		{{--<div class="container">
			<div class="row justify-content-center">
				<div class="media-container-column col-lg-8" data-form-type="formoid">
					<div data-form-alert="" hidden="">Спасибо, твоя заявка получена!</div>

					<form class="mbr-form" action="#" method="post" data-form-title="Mobirise Form"><input type="hidden" name="email" data-form-email="true" value="MKzNigarhwbpQcrrUL4p8J/XJ7pVG5zSuQmcR+1DCKWK6Fg8c0r1BAkyHob7EuIvS9eFtqtyu4aCUrH7mfpHLsHzFkDwx8AeO2mgT+bhmfl+lLK94NOAE2xKVOnboCq4" data-form-field="Email">
						<div class="row row-sm-offset">
							<div class="col-md-4 multi-horizontal" data-for="name">
								<div class="form-group">
									<label class="form-control-label mbr-fonts-style display-7" for="name-form1-18">Имя</label>
									<input type="text" class="form-control" name="name" data-form-field="Name" required="" id="name-form1-18">
								</div>
							</div>
							<div class="col-md-4 multi-horizontal" data-for="email">
								<div class="form-group">
									<label class="form-control-label mbr-fonts-style display-7" for="email-form1-18">Емейл</label>
									<input type="email" class="form-control" name="email" data-form-field="Email" required="" id="email-form1-18">
								</div>
							</div>
							<div class="col-md-4 multi-horizontal" data-for="phone">
								<div class="form-group">
									<label class="form-control-label mbr-fonts-style display-7" for="phone-form1-18">Телефон</label>
									<input type="tel" class="form-control" name="phone" data-form-field="Phone" id="phone-form1-18">
								</div>
							</div>
						</div>

						<span class="input-group-btn"><button href="" type="submit" class="btn btn-primary btn-form display-4">ПОЛУЧИТЬ ЕВАНГЕЛИЕ</button></span>
					</form>
				</div>
			</div>
		</div>--}}
	</section>
@endsection

@section('javascript')
	<script>
        fbq('track', 'ViewContent');
	</script>
@endsection

@section('stylesheet')
	<style>
		#app {
			padding-top: 0 !important;
		}
	</style>
@endsection
