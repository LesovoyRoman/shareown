@extends('letmeshare.layouts.master', [
	'title' => 'Подготовка видео',
	'scripts' => [
		['letmeshare/assets/countdown/jquery.countdown.min.js', 'async defer']
	],
	'styles' => [
		'letmeshare/assets/socicon/css/styles.css'
	],
])

@section('content')


	<section class="countdown2 cid-qYaizvEtTK" id="countdown2-y">
		<div class="container">
			<h2 class="mbr-section-title pb-3 align-center mbr-fonts-style display-2">
				Идет создание видео...</h2>
			<h3 class="mbr-section-subtitle align-center mbr-fonts-style display-5">Ваше видео в очереди на создание. Мы пришлем вам уведомление на почту, когда процесс завершится. Пока вы можете создать еще одно видео или посмотреть уже созданные видео</h3>
		</div>
		<div class="container pt-5 mt-2">
			<div class=" countdown-cont align-center p-4">
				<div class="event-name align-left mbr-white ">
					<h4 class="mbr-fonts-style display-5">Создание ролика</h4> {{-- TODO add data of tracks --}}
				</div>
				<div class="countdown align-center py-2" data-due-date="2018/12/01">
				</div>
				<div class="daysCountdown" title="Дней"></div>
				<div class="hoursCountdown" title="Часов"></div>
				<div class="minutesCountdown" title="Минут"></div>
				<div class="secondsCountdown" title="Секунд"></div>
				<div class="event-date align-left mbr-white">
					<h5 class="mbr-fonts-style display-7">{{ date('Y-m-d') }} - создаем ваш рекламный шедевр</h5>
				</div>
			</div>

			<div class="media-container-row pt-5">
				<div class="mbr-section-btn align-center"><a target="_blank" class="btn btn-primary display-4" href="{{ url('/#create-movie') }}">СОЗДАТЬ ВИДЕО</a>
					<a class="btn btn-black-outline display-4" target="_blank" href="{{ url('movies') }}">МОИ ВИДЕО</a></div>
			</div>
		</div>
	</section>

	{!! Form::open(['route' => 'movies.store', 'id' => 'movie-data', 'class' => 'hidden']) !!}

		{{ Form::hidden('user_id', $movie_data['user_id']) }}
		{{ Form::hidden('template_movie_id', $movie_data['template_movie_id']) }}
		{{ Form::hidden('movie_category_id', $movie_data['movie_category_id']) }}
		{{ Form::hidden('link_youtube') }}

        {{-- New fields --}}

        @isset($movie_data['details']['question'])
            {{ Form::hidden('details[question]', $movie_data['details']['question']) }}
        @endisset

        @isset($movie_data['details']['answer'])
            {{ Form::hidden('details[answer]', $movie_data['details']['answer']) }}
        @endisset

        @isset($movie_data['details']['logo'])
            {{ Form::hidden('details[logo]', $movie_data['details']['logo']) }}
        @endisset

        @isset($movie_data['details']['company']['name'])
            {{ Form::hidden('details[company][name]', $movie_data['details']['company']['name']) }}
        @endisset

        @isset($movie_data['details']['company']['site'])
            {{ Form::hidden('details[company][site]', $movie_data['details']['company']['site']) }}
        @endisset

        @isset($movie_data['details']['company']['phone'])
            {{ Form::hidden('details[company][phone]', $movie_data['details']['company']['phone']) }}
        @endisset

        @isset($movie_data['details']['action_area'])
            {{ Form::hidden('details[action_area]', $movie_data['details']['action_area']) }}
        @endisset

        @isset($movie_data['details']['cta'])
            {{ Form::hidden('details[cta]', $movie_data['details']['cta']) }}
        @endisset

	{!! Form::close() !!}
@endsection

@section('javascript')

	<script>
        $(function () {
            var selected_movie_id = 1;
            showForm(selected_movie_id);

            $('#movie_id').on('change', function () {
                selected_movie_id = parseInt($(this).val());
                showForm(selected_movie_id);
            });

            function showForm(selected_movie_id) {
                $.map($('form'), function (form) {
                    $(form).hide();
                })

                switch (selected_movie_id) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        $('#movie_1_7').show();
                        break;

                    case 8:
                    case 9:
                    case 10:
                        $('#movie_8_10').show();
                        break;

                    case 11:
                        $('#movie_11').show();
                        break;

                    case 12:
                        $('#movie_12').show();
                        break;
                }
            }

            setFormData();
            function setFormData(){
                //var $button = $(this);
               // var $form = $('#'+$button.data('form-id'));

                /**
                 * @todo!
                 *
                 * - consider creating predefined sets for `question` and `answer` fields.
                 * - `company_site` and `call_to_action (cta)` fields might be transformed to uppercase.
                 * - `phone` field must be validated as phone number.
                 * - create limitations on input length for each field
                 */


                // CHANGED !
                var form_data = {
                    movie_id: {{$movie_data['details']['template_movie_id']}},

                    company: {
                        name:  '{{$movie_data['details']['company']['name']}}',
                        phone: '{{$movie_data['details']['company']['phone']}}'
                    }
                };

                @isset($movie_data['details']['company']['name_color'])
                    form_data['company']['name_color'] = JSON.parse('{{$movie_data['details']['company']['name_color']}}'.replace(/&quot;/g, '\"'));
                @endisset

                @isset($movie_data['details']['company']['phone_color'])
                    form_data['company']['phone_color'] = JSON.parse('{{$movie_data['details']['company']['phone_color']}}'.replace(/&quot;/g, '\"'));
                @endisset

                @isset($movie_data['details']['bg_music_id'])
                    @if(!empty($movie_data['details']['noMusic']) && $movie_data['details']['noMusic'] == 1)
                        form_data['bg_music_id'] = 0;
                    @else
                        form_data['bg_music_id'] = '{{$movie_data['details']['bg_music_id']}}';
                    @endif
                @endisset

                @isset($movie_data['details']['bg_audio_id'])
                    @if(!empty($movie_data['details']['noVoice']) && $movie_data['details']['noVoice'] == 1)
                        form_data['bg_audio_id'] = 0;
                    @else
                        form_data['bg_audio_id'] = '{{$movie_data['details']['bg_audio_id']}}';
                    @endif
                @endisset

                @isset($movie_data['details']['logo'])
                    form_data['company']['logo'] = '{{$movie_data['details']['logo']}}';
                @endisset

                @isset($movie_data['details']['company']['site'])
                    form_data['company']['site'] = '{{$movie_data['details']['company']['site']}}'.replace(/&quot;/g, '\"');
                @endisset

                @isset($movie_data['details']['company']['site_color'])
                    form_data['company']['site_color'] = JSON.parse('{{$movie_data['details']['company']['site_color']}}'.replace(/&quot;/g, '\"'));
                @endisset

                @isset($movie_data['details']['slogan_1'])
                    form_data['slogan_1'] = '{{$movie_data['details']['slogan_1']}}'.replace(/&quot;/g, '\"');
                @endisset

                @isset($movie_data['details']['slogan_1_color'])
                    form_data['slogan_1_color'] = JSON.parse('{{$movie_data['details']['slogan_1_color']}}'.replace(/&quot;/g, '\"'));
                @endisset

                @isset($movie_data['details']['slogan_2'])
                    form_data['slogan_2'] = '{{$movie_data['details']['slogan_2']}}'.replace(/&quot;/g, '\"');
                @endisset

                @isset($movie_data['details']['slogan_2_color'])
                    form_data['slogan_2_color'] = JSON.parse('{{$movie_data['details']['slogan_2_color']}}'.replace(/&quot;/g, '\"'));
                @endisset

                @isset($movie_data['details']['slogan_3'])
                    form_data['slogan_3'] = '{{$movie_data['details']['slogan_3']}}'.replace(/&quot;/g, '\"');
                @endisset

                @isset($movie_data['details']['slogan_3_color'])
                    form_data['slogan_3_color'] = JSON.parse('{{$movie_data['details']['slogan_3_color']}}'.replace(/&quot;/g, '\"'));
                @endisset

                @isset($movie_data['details']['slogan_4'])
                    form_data['slogan_4'] = '{{$movie_data['details']['slogan_4']}}'.replace(/&quot;/g, '\"');
                @endisset

                @isset($movie_data['details']['slogan_4_color'])
                    form_data['slogan_4_color'] = JSON.parse('{{$movie_data['details']['slogan_4_color']}}'.replace(/&quot;/g, '\"'));
                @endisset

                @isset($movie_data['details']['slogan_5'])
                    form_data['slogan_5'] = '{{$movie_data['details']['slogan_5']}}'.replace(/&quot;/g, '\"');
                @endisset

                @isset($movie_data['details']['slogan_5_color'])
                    form_data['slogan_5_color'] = JSON.parse('{{$movie_data['details']['slogan_5_color']}}'.replace(/&quot;/g, '\"'));
                @endisset

                @isset($movie_data['details']['slogan_6'])
                    form_data['slogan_6'] = '{{$movie_data['details']['slogan_6']}}'.replace(/&quot;/g, '\"');
                @endisset

                @isset($movie_data['details']['slogan_6_color'])
                    form_data['slogan_6_color'] = JSON.parse('{{$movie_data['details']['slogan_6_color']}}'.replace(/&quot;/g, '\"'));
                @endisset

                @isset($movie_data['details']['slogan_7'])
                    form_data['slogan_7'] = '{{$movie_data['details']['slogan_7']}}'.replace(/&quot;/g, '\"');
                @endisset

                @isset($movie_data['details']['slogan_7_color'])
                    form_data['slogan_7_color'] = JSON.parse('{{$movie_data['details']['slogan_7_color']}}'.replace(/&quot;/g, '\"'));
                @endisset

                @isset($movie_data['details']['advantage_1'])
                    form_data['advantage_1'] = '{{$movie_data['details']['advantage_1']}}'.replace(/&quot;/g, '\"');
                @endisset

                @isset($movie_data['details']['advantage_1_color'])
                    form_data['advantage_1_color'] = JSON.parse('{{$movie_data['details']['advantage_1_color']}}'.replace(/&quot;/g, '\"'));
                @endisset

                @isset($movie_data['details']['advantage_2'])
                    form_data['advantage_2'] = '{{$movie_data['details']['advantage_2']}}'.replace(/&quot;/g, '\"');
                @endisset

                @isset($movie_data['details']['advantage_2_color'])
                    form_data['advantage_2_color'] = JSON.parse('{{$movie_data['details']['advantage_2_color']}}'.replace(/&quot;/g, '\"'));
                @endisset

                @isset($movie_data['details']['advantage_3'])
                    form_data['advantage_3'] = '{{$movie_data['details']['advantage_3']}}'.replace(/&quot;/g, '\"');
                @endisset

                @isset($movie_data['details']['advantage_3_color'])
                    form_data['advantage_3_color'] = JSON.parse('{{$movie_data['details']['advantage_3_color']}}'.replace(/&quot;/g, '\"'));
                @endisset

                @isset($movie_data['details']['advantage_4'])
                    form_data['advantage_4'] = '{{$movie_data['details']['advantage_4']}}'.replace(/&quot;/g, '\"');
                @endisset

                @isset($movie_data['details']['advantage_4_color'])
                    form_data['advantage_4_color'] = JSON.parse('{{$movie_data['details']['advantage_4_color']}}'.replace(/&quot;/g, '\"'));
                @endisset

                @isset($movie_data['details']['advantage_5'])
                    form_data['advantage_5'] = '{{$movie_data['details']['advantage_5']}}'.replace(/&quot;/g, '\"');
                @endisset

                @isset($movie_data['details']['advantage_5_color'])
                    form_data['advantage_5_color'] = JSON.parse('{{$movie_data['details']['advantage_5_color']}}'.replace(/&quot;/g, '\"'));
                @endisset

                @isset($movie_data['details']['cta'])
                    form_data['cta'] = '{{$movie_data['details']['cta']}}'.replace(/&quot;/g, '\"');
                @endisset

                @isset($movie_data['details']['cta_color'])
                    form_data['cta_color'] = JSON.parse('{{$movie_data['details']['cta_color']}}'.replace(/&quot;/g, '\"'));
                @endisset

                @isset($movie_data['details']['question'])
                    form_data['question'] = '{{$movie_data['details']['question']}}'.replace(/&quot;/g, '\"');
                @endisset

                @isset($movie_data['details']['question_color'])
                    form_data['question_color'] = JSON.parse('{{$movie_data['details']['question_color']}}'.replace(/&quot;/g, '\"'));
                @endisset

                @isset($movie_data['details']['answer'])
                    form_data['answer'] = '{{$movie_data['details']['answer']}}'.replace(/&quot;/g, '\"');
                @endisset

                @isset($movie_data['details']['answer_color'])
                    form_data['answer_color'] = JSON.parse('{{$movie_data['details']['answer_color']}}'.replace(/&quot;/g, '\"'));
                @endisset

                @isset($movie_data['details']['first'])
                    form_data['first'] = '{{$movie_data['details']['first']}}'.replace(/&quot;/g, '\"');
                @endisset

                @isset($movie_data['details']['first_color'])
                    form_data['first_color'] = JSON.parse('{{$movie_data['details']['first_color']}}'.replace(/&quot;/g, '\"'));
                @endisset

                // For movie 11
                @isset($movie_data['details']['company']['sphere'])
                    form_data['company']['sphere'] = '{{$movie_data['details']['company']['sphere']}}'.replace(/&quot;/g, '\"');
                @endisset

                @isset($movie_data['details']['company']['sphere_color'])
                    form_data['company']['sphere_color'] = JSON.parse('{{$movie_data['details']['company']['sphere_color']}}'.replace(/&quot;/g, '\"'));
                @endisset

                @isset($movie_data['details']['photo_1'])
                    form_data['photo_1'] = '{{$movie_data['details']['photo_1']}}';
                @endisset

                @isset($movie_data['details']['photo_2'])
                    form_data['photo_2'] = '{{$movie_data['details']['photo_2']}}';
                @endisset

                @isset($movie_data['details']['photo_3'])
                    form_data['photo_3'] = '{{$movie_data['details']['photo_3']}}';
                @endisset

                createMovie(form_data);
            };

            function loadFile(filePath, callback) {
                var xhr = new XMLHttpRequest();
                xhr.open("GET", filePath, true);
                xhr.onload = function () { return callback(this.responseText) }
                xhr.setRequestHeader("Accept", 'application/json');
                xhr.send(null);

            }

            function createMovie(form_data) {
                var movieConfig = getMovieConfig(form_data);

                if (!movieConfig) {
                    alert('Unable to retrieve movie config!');
                    return;
                }

                var trackers = getTrackers(form_data['movie_id']);

                if (!trackers) {
                    alert('Unable to retrieve movie trackers!');
                    return;
                }

                var promises = [];
                trackers.forEach(function (file, index) {
                    promises.push(new Promise(function (resolve, reject) {
                        loadFile(file, function (responseText) {
                            setTimeout(resolve, 100, JSON.parse(responseText).frames);
                        });
                    }));
                });

                Promise.all(promises).then(function (result) {
                    result.forEach(function (keyframes, index) {
                        movieConfig.transformations[index].keyframes = keyframes;
                    });

                    prepareMovieData(movieConfig, form_data);
                });

                function prepareMovieData(movieConfig, form_data) {
                    /**
                     * @todo!
                     *
                     * Consider storing these credentials on the backend.
                     * E.g., if we are using Laravel, we can store them in .env file.
                     */


                    // Default credentials
                    var api_key = '{{ env('YOUTUBE_API') }}';
                    var api_secret = '{{ env('YOUTUBE_API_SECRET') }}';
                    var region = '{{ env('YOUTUBE_REGION') }}';
                    var project_id = '{{ env('YOUTUBE_PROJECT_ID') }}';

                    FX.config.update({
                        apikey: api_key,
                        apisecret: api_secret,
                        region: region
                    });

                    var sdl = FX.SDL();
                    var myProject = new FX.Project({ params: {ProjectId: project_id} });

                    // Video
                    var video_url = '{{ url('letmeshare/videos/' . $movie_data['template_movie_id'] . '.mp4') }}';

                    var tracks = [];

                    // VideoBackground
                    tracks.push(new sdl.VisualTrack({
                        content: {
                            type: "video",
                            source: {
                                path: video_url
                            }
                        }
                    }));


                    // Transformations
                    movieConfig.transformations.forEach(function (transformation) {
                        ['images', 'texts'].forEach(function (type) {
                            if (transformation[type]) {
                                transformation[type].forEach(function (item) {
                                    tracks.push(new sdl.VisualTrack(getVisualTrackParams(transformation, type, item)));
                                });
                            }
                        });
                    });

                    // Audio
                    var audiotracks = [];

                    if(form_data['bg_audio_id'] !== 0) {
                        audiotracks.push(new sdl.AudioTrack({
                            type: "mix",
                            source: {
                                type: "simple",
                                path: getVoice(form_data['bg_audio_id'])
                            }
                        }));
                    }

                    if(form_data['bg_music_id'] !== 0) {
                        audiotracks.push(new sdl.AudioTrack({
                            type: "mix",
                            volume: 0.4,
                            source: {
                                type: "simple",
                                path: getMusicBg(form_data['bg_music_id'])
                            }
                        }));
                    }

                    var scene = new sdl.Scene({
                        numframes: movieConfig.numframes,
                        tracks: tracks,
                        audio: {
                            audiotracks: audiotracks
                        }
                    });

                    var movie = new sdl.Movie({
                        params: {
                            vparams: {
                                 width: 1920,
                                 height: 1080,
                                videoframerate: {
                                    num: movieConfig.framerate,
                                    den: 1
                                }
                            },
                            aparams: {
                                audiocodec: 'AUDIO_AAC',
                                audioabr: 128000
                            }
                        },
                        scenes: [scene]
                    });


                    function makeid() {
                        var text = "";
                        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                        for (var i = 0; i < 10; i++)
                            text += possible.charAt(Math.floor(Math.random() * possible.length));

                        return text;
                    }

                    var new_id = makeid();

                    myProject.createMovie({Name: 'movie_' + new_id + '_' + form_data['movie_id'], Movie: movie}, function (error, data) {
                        if (error) {
                            console.error(error); // an error occurred
                        } else {
                            uploadMovieToYoutube(data, new_id);
                        }
                    });
                }
            }



            function getVisualTrackParams(transformation, type, item) {
                var visualTrackParams = {
                    numframes: transformation.numframes,
                    offset: transformation.offset,
                    content: {},
                    transformations: [{
                        type: 'quadtracking',
                        trackdata: {
                            type: 'embedded',
                            embeddeddata: {
                                frames: transformation.keyframes
                            }
                        }
                    }]
                };

                // Content according to the type (image or text)
                if (type === 'images') {
                    visualTrackParams.content = {
                        type: "http",
                        source: {
                            path: item.value
                        }
                    };
                } else {
                    visualTrackParams.content = {
                        type: "textsimple",
                        fontsize: item.fontsize,
                        color: item.color,
                        xalignment: item.hasOwnProperty('alignment') && item.alignment.hasOwnProperty('x') ? item.alignment.x : "centered",
                        yalignment: item.hasOwnProperty('alignment') && item.alignment.hasOwnProperty('y') ? item.alignment.y : "middle",
                        text: {
                            "type": "constant",
                            "value": item.value
                        }
                    };

                    if (item.font) {
                        visualTrackParams.content.source = {
                            path: item.font
                        };
                    }
                }

                // Position adjustment
                if (item.posadjust) {
                    visualTrackParams.transformations[0].posadjust = item.posadjust;
                }

                // Blendmode
                if (item.blendmode) {
                    visualTrackParams.blendmode = item.blendmode;
                }

                // Opacity function (fade-in and fade-out)
                if (item.opacityfunction) {
                    visualTrackParams.opacityfunction = item.opacityfunction;
                }

                // Text related properties
                if (item.width) {
                    visualTrackParams.content.width = item.width;
                }

                if (item.height) {
                    visualTrackParams.content.height = item.height;
                }

                return visualTrackParams;
            }


            function uploadMovieToYoutube(data, name) {

                var render = new FX.Render({apiVersion: '2016-06-02'});

                var parameters = {
                    ProjectId: data['Project-UID'],
                    Movie: data['Resource-Name'],
                    Params: {},
                    Format: 'mp4',
                    Title: 'Рекламное видео id' + name,
                    Description: 'Описание рекламного видео id' + name,
                    Category: '27',
                    Async: false,
                    AuthSecret: '{{ env('AUTH_SECRET') }}',
                    Status: 'unlisted',
                    NotifySubscribers: false
                }

                render.renderToYoutube(parameters, function(err, data) {
                    if(err) {
                        console.log(err); // an error occurred
                    } else {

                        if (data.hasOwnProperty('upload') && data.upload.hasOwnProperty('status') && data.upload.hasOwnProperty('result')) {
                            if (parseInt(data.upload.status) === 200) {
                                document.getElementsByName('link_youtube')[0].value = 'https://youtube.com/watch?v=' + data.upload.result;

                                // Give 15 secs for youtube to render video
                                setTimeout(function () {
                                    document.getElementById("movie-data").submit();
                                }, 20000);
                            } else {
                                var error_html = '<div class="container countdown-cont align-center">';
                                error_html += '<h3>Возникла ошибка при загрузке видео на YouTube! Пожалуйста, перезагрузите страницу!</h3>';
                                error_html += '</div>';
                                $('#countdown1-1l').html(error_html);
                                console.error(data.upload.result); // @TODO here the rendering mistake
                            }
                        }
                    }
                });
            }


            function getVoice(id) {
                /**
                 * @todo!
                 *
                 * Map `background_music_ids` to `urls`(!) of actual music files (locate `music/` folder in project's root).
                 * We can't use straight path to a file because it is just not working for some reason.
                 */

                var music = {
                    1: 'https://sag.devbrother.com/storage/audiotags/1/1.wav',
                    2: 'https://sag.devbrother.com/storage/audiotags/1/2.wav',
                    3: 'https://sag.devbrother.com/storage/audiotags/1/3.wav',
                    4: 'https://sag.devbrother.com/storage/audiotags/1/4.wav',
                    5: 'https://sag.devbrother.com/storage/audiotags/1/5.wav',
                    6: 'https://sag.devbrother.com/storage/audiotags/1/6.wav',
                    7: 'https://sag.devbrother.com/storage/audiotags/1/7.wav',
                    8: 'https://sag.devbrother.com/storage/audiotags/1/8.wav',
                    9: 'https://sag.devbrother.com/storage/audiotags/1/9.wav',
                    10: 'https://sag.devbrother.com/storage/audiotags/1/10.wav',
                    11: 'https://sag.devbrother.com/storage/audiotags/1/11.wav',
                    12: 'https://sag.devbrother.com/storage/audiotags/1/12.wav',
                    13: 'https://sag.devbrother.com/storage/audiotags/1/13.wav',
                    14: 'https://sag.devbrother.com/storage/audiotags/1/14.wav',
                    15: 'https://sag.devbrother.com/storage/audiotags/1/15.wav',
                    16: 'https://sag.devbrother.com/storage/audiotags/1/16.wav',

                    66: 'https://sag.devbrother.com/storage/audiotags/4/nadoelamebel/1.wav',
                    77: 'https://sag.devbrother.com/storage/audiotags/4/nadoelamebel/2.wav',
                    88: 'https://sag.devbrother.com/storage/audiotags/4/nadoelamebel/3.wav',
                    99: 'https://sag.devbrother.com/storage/audiotags/4/nadoelamebel/4.wav',


                    // № 8
                    // sdelai sam
                    17: 'https://sag.devbrother.com/storage/audiotags/2/sobratmebel/1.wav',
                    18: 'https://sag.devbrother.com/storage/audiotags/2/sobratmebel/2.wav',
                    19: 'https://sag.devbrother.com/storage/audiotags/2/sobratmebel/3.wav',

                    20: 'https://sag.devbrother.com/storage/audiotags/2/stalyarroboty/1.wav',
                    21: 'https://sag.devbrother.com/storage/audiotags/2/stalyarroboty/2.wav',
                    22: 'https://sag.devbrother.com/storage/audiotags/2/stalyarroboty/3.wav',
                    23: 'https://sag.devbrother.com/storage/audiotags/2/stalyarroboty/4.wav',

                    143: 'https://sag.devbrother.com/storage/audiotags/4/zadumaliremont/1.wav',
                    144: 'https://sag.devbrother.com/storage/audiotags/4/zadumaliremont/2.wav',
                    145: 'https://sag.devbrother.com/storage/audiotags/4/zadumaliremont/3.wav',
                    146: 'https://sag.devbrother.com/storage/audiotags/4/zadumaliremont/4.wav',

                    // № 9
                    // ventilyaziya
                    24: 'https://sag.devbrother.com/storage/audiotags/2/condizioner/1.wav',
                    25: 'https://sag.devbrother.com/storage/audiotags/2/condizioner/2.wav',
                    26: 'https://sag.devbrother.com/storage/audiotags/2/condizioner/3.wav',
                    27: 'https://sag.devbrother.com/storage/audiotags/2/ventilyaziya/1.wav',
                    28: 'https://sag.devbrother.com/storage/audiotags/2/ventilyaziya/2.wav',
                    29: 'https://sag.devbrother.com/storage/audiotags/2/ventilyaziya/3.wav',
                    30: 'https://sag.devbrother.com/storage/audiotags/2/ventilyaziya/4.wav',

                    // № 10
                    // elektrika
                    31: 'https://sag.devbrother.com/storage/audiotags/2/elektrika/1.wav',
                    32: 'https://sag.devbrother.com/storage/audiotags/2/elektrika/2.wav',
                    33: 'https://sag.devbrother.com/storage/audiotags/2/elektrika/3.wav',
                    34: 'https://sag.devbrother.com/storage/audiotags/2/elektrika/4.wav',
                    35: 'https://sag.devbrother.com/storage/audiotags/2/provodka/1.wav',
                    36: 'https://sag.devbrother.com/storage/audiotags/2/provodka/2.wav',
                    37: 'https://sag.devbrother.com/storage/audiotags/2/provodka/3.wav',
                    38: 'https://sag.devbrother.com/storage/audiotags/2/provodka/4.wav',

                    // № 12 & 11
                    // dizain
                    39: 'https://sag.devbrother.com/storage/audiotags/4/nadoelamebel/1.wav',
                    40: 'https://sag.devbrother.com/storage/audiotags/4/nadoelamebel/2.wav',
                    41: 'https://sag.devbrother.com/storage/audiotags/4/nadoelamebel/3.wav',
                    42: 'https://sag.devbrother.com/storage/audiotags/4/nadoelamebel/4.wav',
                    43: 'https://sag.devbrother.com/storage/audiotags/4/zadumaliremont/1.wav',
                    44: 'https://sag.devbrother.com/storage/audiotags/4/zadumaliremont/2.wav',
                    45: 'https://sag.devbrother.com/storage/audiotags/4/zadumaliremont/3.wav',
                    46: 'https://sag.devbrother.com/storage/audiotags/4/zadumaliremont/4.wav',
                };

                return music[id];
            }

            function getMusicBg(id) {
                /**
                 * @todo!
                 *
                 * Map `background_music_ids` to `urls`(!) of actual music files (locate `music/` folder in project's root).
                 * We can't use straight path to a file because it is just not working for some reason.
                 */

                var music = {
                    1: 'https://sag.devbrother.com/storage/ElectroFuturisticLogoIntroNoMelody.wav',
                    2: 'https://sag.devbrother.com/storage/Fun-30sec_2.wav',
                    3: 'https://sag.devbrother.com/storage/Comedy.wav',
                    4: 'https://sag.devbrother.com/storage/Quirky.wav',
                    5: 'https://sag.devbrother.com/storage/Quirky_second_version.wav',
                    6: 'https://sag.devbrother.com/storage/Quirky_second_version_30.wav',
                    7: 'https://sag.devbrother.com/storage/CartoonsAdventureLoopedVersion2.wav',
                    8: 'https://sag.devbrother.com/storage/NoSopranoSax.wav',
                    9: 'https://sag.devbrother.com/storage/FunnyOrchestral.wav',
                    10: 'https://sag.devbrother.com/storage/CartoonsAdventureUnderscore.wav',
                    11: 'https://sag.devbrother.com/storage/Comedy_60.wav'
                };

                return music[id];
            }

            function getTrackers(movie_id) {
                /**
                 * @todo!
                 *
                 *  Map `movie_ids` to `paths`(!) to actual trackers (locate `movies/` folder in project's root).
                 *  Here we can use straight path to a file because we are loading it with our own `loadFile()` function.
                 *
                 *  DO NOT CHANGE ORDER OF TRACKERS!
                 */

                var trackers = [];

                switch (movie_id) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        trackers = [
                           '{{ asset('letmeshare/videos/new/simple-advertisement-generator/movies') }}' + '/' + movie_id + '/trackers/Question.json',
                           '{{ asset('letmeshare/videos/new/simple-advertisement-generator/movies') }}' + '/' + movie_id + '/trackers/Info.json'
                        ];

                        break;

                    case 8:
                    case 10:
                        trackers = [
                            '{{ asset('letmeshare/videos/new/simple-advertisement-generator/movies') }}' + '/' + movie_id + '/trackers/First_1.json',
                            '{{ asset('letmeshare/videos/new/simple-advertisement-generator/movies') }}' + '/' + movie_id + '/trackers/First_2.json',,
                            '{{ asset( 'letmeshare/videos/new/simple-advertisement-generator/movies' ) }}' + '/' + movie_id + '/trackers/Question.json',
                            '{{ asset( 'letmeshare/videos/new/simple-advertisement-generator/movies' ) }}' + '/' + movie_id + '/trackers/Info.json'
                        ];
                        break;

                    case 9:
                        trackers = [
                            '{{ asset('letmeshare/videos/new/simple-advertisement-generator/movies') }}' + '/' + movie_id + '/trackers/First.json',
                            '{{ asset( 'letmeshare/videos/new/simple-advertisement-generator/movies' ) }}' + '/' + movie_id + '/trackers/Question.json',
                            '{{ asset( 'letmeshare/videos/new/simple-advertisement-generator/movies' ) }}' + '/' + movie_id + '/trackers/Info.json'
                        ];
                        break;

                    case 11:
                        trackers = [
                            '{{ asset('letmeshare/videos/new/simple-advertisement-generator/movies') }}' + '/' + movie_id + '/trackers/First.json',
                            '{{ asset('letmeshare/videos/new/simple-advertisement-generator/movies') }}' + '/' + movie_id + '/trackers/Main.json'
                         ];
                         break;

                    case 12:
                         trackers = [
                             '{{ asset('letmeshare/videos/new/simple-advertisement-generator/movies' ) }}' + '/' + movie_id + '/trackers/Name.json',
                             '{{ asset('letmeshare/videos/new/simple-advertisement-generator/movies' ) }}' + '/' + movie_id + '/trackers/Logo_and_sphere.json',
                             '{{ asset('letmeshare/videos/new/simple-advertisement-generator/movies' ) }}' + '/' + movie_id + '/trackers/Slogan_1.json',
                             '{{ asset('letmeshare/videos/new/simple-advertisement-generator/movies' ) }}' + '/' + movie_id + '/trackers/Slogan_2.json',
                             '{{ asset('letmeshare/videos/new/simple-advertisement-generator/movies' ) }}' + '/' + movie_id + '/trackers/Slogan_3.json',
                             '{{ asset('letmeshare/videos/new/simple-advertisement-generator/movies' ) }}' + '/' + movie_id + '/trackers/Slogan_4.json',
                             '{{ asset('letmeshare/videos/new/simple-advertisement-generator/movies' ) }}' + '/' + movie_id + '/trackers/Photos.json',
                             '{{ asset('letmeshare/videos/new/simple-advertisement-generator/movies' ) }}' + '/' + movie_id + '/trackers/Advantages.json',
                             '{{ asset('letmeshare/videos/new/simple-advertisement-generator/movies' ) }}' + '/' + movie_id + '/trackers/Slogan_5.json',
                             '{{ asset('letmeshare/videos/new/simple-advertisement-generator/movies' ) }}' + '/' + movie_id + '/trackers/Slogan_6.json',
                             '{{ asset('letmeshare/videos/new/simple-advertisement-generator/movies' ) }}' + '/' + movie_id + '/trackers/Slogan_7.json',
                             '{{ asset('letmeshare/videos/new/simple-advertisement-generator/movies' ) }}' + '/' + movie_id + '/trackers/Info.json'
                         ];
                         break;
                 }

                 return trackers;
             }

            function getMovieConfig(form_data) {
                /**
                * @todo!
                *
                * `videoName` property value represents the path to the main background video.
                * There are two options:
                * 1) If we are retrieving video resource from Impossible Software (IS) server - `videoName` value must
                * 	  represent the name of any video uploaded to the IS server (see `Videos` section in Impossible Software
                * 	  Console (ISC));
                * 2) If we are retrieving video resource from our server - `videoName` value must represent the url to
                * 	  the file located on our server.
                */

                var movie_id = form_data['movie_id'];

                var configs = {
                    1: {
                         numframes: 703,
                         framerate: 25,
                         transformations: [
                             {
                                 numframes: 35,
                                 offset: 530,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['question'] ? form_data['question'] : 'Доставить мебель?',
                                         color: form_data['question_color'] ? form_data['question_color'] : {red: 255, green: 255, blue: 0, alpha: 255},
                                         fontsize: 120.0,
                                         width: 1910,
                                         height: 482,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.45,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     },
                                     {
                                         value: form_data['answer'] ? form_data['answer'] : 'Не усложняйте себе жизнь, обратитесь к профессионалам',
                                         color: form_data['answer_color'] ? form_data['answer_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 100.0,
                                         width: 1910,
                                         height: 535,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.45,
                                             offsetx: 0.0,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     },
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 76,
                                 offset: 566,
                                 images: [
                                     {
                                         value: form_data['company']['logo'] ? form_data['company']['logo'] : 'https://www.microcreatives.com/wp-content/uploads/2017/02/Apr-3-2017_Examples-of-Good-and-Bad-Typography-Logos.jpg',
                                         posadjust: {
                                             scalex: 0.15,
                                             scaley: 0.25,
                                             offsetx: 0.25,
                                             offsety: 0.17
                                         },
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 texts: [
                                     {
                                         value: form_data['company']['name'] ? form_data['company']['name'] : 'Ремонтник',
                                         color: form_data['company']['name_color'] ? form_data['company']['name_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 200.0,
                                         width: 764,
                                         height: 430,
                                         alignment: {
                                             x: 'flushleft',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.4,
                                             scaley: 0.4,
                                             offsetx: 0.45,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['site'] ? form_data['company']['site'] : 'REMONT.RU',
                                         color: form_data['company']['site_color'] ? form_data['company']['site_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 161,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.15,
                                             offsetx: 0.15,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['phone'] ? form_data['company']['phone'] : '+79876543210',
                                         color: form_data['company']['phone_color'] ? form_data['company']['phone_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'middle'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.65
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['cta'] ? form_data['cta'] : 'ЗВОНИТЕ СЕЙЧАС!',
                                         color: form_data['cta_color'] ? form_data['cta_color'] : {red: 255, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.75
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                 ],
                                 keyframes: {}
                             }
                         ]
                    },
                    2: {
                         numframes: 712,
                         framerate: 25,
                         transformations: [
                             {
                                 numframes: 41,
                                 offset: 541,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['question'] ? form_data['question'] : 'Доставить мебель?',
                                         color: form_data['question_color'] ? form_data['question_color'] : {red: 255, green: 255, blue: 0, alpha: 255},
                                         fontsize: 120.0,
                                         width: 1910,
                                         height: 482,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.45,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     },
                                     {
                                         value: form_data['answer'] ? form_data['answer'] : 'Не усложняйте себе жизнь, обратитесь к профессионалам',
                                         color: form_data['answer_color'] ? form_data['answer_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 100.0,
                                         width: 1910,
                                         height: 535,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.45,
                                             offsetx: 0.0,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     },
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 82,
                                 offset: 583,
                                 images: [
                                     {
                                         value: form_data['company']['logo'] ? form_data['company']['logo'] : 'https://www.microcreatives.com/wp-content/uploads/2017/02/Apr-3-2017_Examples-of-Good-and-Bad-Typography-Logos.jpg',
                                         posadjust: {
                                             scalex: 0.15,
                                             scaley: 0.25,
                                             offsetx: 0.25,
                                             offsety: 0.17
                                         },
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 texts: [
                                     {
                                         value: form_data['company']['name'] ? form_data['company']['name'] : 'Ремонтник',
                                         color: form_data['company']['name_color'] ? form_data['company']['name_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 200.0,
                                         width: 764,
                                         height: 430,
                                         alignment: {
                                             x: 'flushleft',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.4,
                                             scaley: 0.4,
                                             offsetx: 0.45,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['site'] ? form_data['company']['site'] : 'REMONT.RU',
                                         color: form_data['company']['site_color'] ? form_data['company']['site_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 161,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.15,
                                             offsetx: 0.15,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['phone'] ? form_data['company']['phone'] : '+79876543210',
                                         color: form_data['company']['phone_color'] ? form_data['company']['phone_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'middle'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.65
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['cta'] ? form_data['cta'] : 'ЗВОНИТЕ СЕЙЧАС!',
                                         color: form_data['cta_color'] ? form_data['cta_color'] : {red: 255, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.75
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                 ],
                                 keyframes: {}
                             }
                         ]
                    },
                    3: {
                         numframes: 628,
                         framerate: 25,
                         transformations: [
                             {
                                 numframes: 43,
                                 offset: 457,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['question'] ? form_data['question'] : 'Доставить мебель?',
                                         color: form_data['question_color'] ? form_data['question_color'] : {red: 255, green: 255, blue: 0, alpha: 255},
                                         fontsize: 120.0,
                                         width: 1910,
                                         height: 482,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.45,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     },
                                     {
                                         value: form_data['answer'] ? form_data['answer'] : 'Не усложняйте себе жизнь, обратитесь к профессионалам',
                                         color: form_data['answer_color'] ? form_data['answer_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 100.0,
                                         width: 1910,
                                         height: 535,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.45,
                                             offsetx: 0.0,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     },
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 86,
                                 offset: 501,
                                 images: [
                                     {
                                         value: form_data['company']['logo'] ? form_data['company']['logo'] : 'https://www.microcreatives.com/wp-content/uploads/2017/02/Apr-3-2017_Examples-of-Good-and-Bad-Typography-Logos.jpg',
                                         posadjust: {
                                             scalex: 0.15,
                                             scaley: 0.25,
                                             offsetx: 0.25,
                                             offsety: 0.17
                                         },
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 texts: [
                                     {
                                         value: form_data['company']['name'] ? form_data['company']['name'] : 'Ремонтник',
                                         color: form_data['company']['name_color'] ? form_data['company']['name_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 200.0,
                                         width: 764,
                                         height: 430,
                                         alignment: {
                                             x: 'flushleft',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.4,
                                             scaley: 0.4,
                                             offsetx: 0.45,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['site'] ? form_data['company']['site'] : 'REMONT.RU',
                                         color: form_data['company']['site_color'] ? form_data['company']['site_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 161,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.15,
                                             offsetx: 0.15,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['phone'] ? form_data['company']['phone'] : '+79876543210',
                                         color: form_data['company']['phone_color'] ? form_data['company']['phone_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'middle'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.65
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['cta'] ? form_data['cta'] : 'ЗВОНИТЕ СЕЙЧАС!',
                                         color: form_data['cta_color'] ? form_data['cta_color'] : {red: 255, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.75
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                 ],
                                 keyframes: {}
                             }
                         ]
                    },
                    4: {
                         numframes: 835,
                         framerate: 25,
                         transformations: [
                             {
                                 numframes: 32,
                                 offset: 668,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['question'] ? form_data['question'] : 'Доставить мебель?',
                                         color: form_data['question_color'] ? form_data['question_color'] : {red: 255, green: 255, blue: 0, alpha: 255},
                                         fontsize: 120.0,
                                         width: 1910,
                                         height: 482,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.45,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     },
                                     {
                                         value: form_data['answer'] ? form_data['answer'] : 'Не усложняйте себе жизнь, обратитесь к профессионалам',
                                         color: form_data['answer_color'] ? form_data['answer_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 100.0,
                                         width: 1910,
                                         height: 535,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.45,
                                             offsetx: 0.0,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     },
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 50,
                                 offset: 701,
                                 images: [
                                     {
                                         value: form_data['company']['logo'] ? form_data['company']['logo'] : 'https://www.microcreatives.com/wp-content/uploads/2017/02/Apr-3-2017_Examples-of-Good-and-Bad-Typography-Logos.jpg',
                                         posadjust: {
                                             scalex: 0.15,
                                             scaley: 0.25,
                                             offsetx: 0.25,
                                             offsety: 0.17
                                         },
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 texts: [
                                     {
                                         value: form_data['company']['name'] ? form_data['company']['name'] : 'Ремонтник',
                                         color: form_data['company']['name_color'] ? form_data['company']['name_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 200.0,
                                         width: 764,
                                         height: 430,
                                         alignment: {
                                             x: 'flushleft',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.4,
                                             scaley: 0.4,
                                             offsetx: 0.45,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['site'] ? form_data['company']['site'] : 'REMONT.RU',
                                         color: form_data['company']['site_color'] ? form_data['company']['site_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 161,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.15,
                                             offsetx: 0.15,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['phone'] ? form_data['company']['phone'] : '+79876543210',
                                         color: form_data['company']['phone_color'] ? form_data['company']['phone_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'middle'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.65
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['cta'] ? form_data['cta'] : 'ЗВОНИТЕ СЕЙЧАС!',
                                         color: form_data['cta_color'] ? form_data['cta_color'] : {red: 255, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.75
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                 ],
                                 keyframes: {}
                             }
                         ]
                    },
                    5: {
                         numframes: 711,
                         framerate: 25,
                         transformations: [
                             {
                                 numframes: 50,
                                 offset: 539,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['question'] ? form_data['question'] : 'Доставить мебель?',
                                         color: form_data['question_color'] ? form_data['question_color'] : {red: 255, green: 255, blue: 0, alpha: 255},
                                         fontsize: 120.0,
                                         width: 1910,
                                         height: 482,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.45,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     },
                                     {
                                         value: form_data['answer'] ? form_data['answer'] : 'Не усложняйте себе жизнь, обратитесь к профессионалам',
                                         color: form_data['answer_color'] ? form_data['answer_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 100.0,
                                         width: 1910,
                                         height: 535,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.45,
                                             offsetx: 0.0,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     },
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 100,
                                 offset: 590,
                                 images: [
                                     {
                                         value: form_data['company']['logo'] ? form_data['company']['logo'] : 'https://www.microcreatives.com/wp-content/uploads/2017/02/Apr-3-2017_Examples-of-Good-and-Bad-Typography-Logos.jpg',
                                         posadjust: {
                                             scalex: 0.15,
                                             scaley: 0.25,
                                             offsetx: 0.25,
                                             offsety: 0.17
                                         },
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 texts: [
                                     {
                                         value: form_data['company']['name'] ? form_data['company']['name'] : 'Ремонтник',
                                         color: form_data['company']['name_color'] ? form_data['company']['name_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 200.0,
                                         width: 764,
                                         height: 430,
                                         alignment: {
                                             x: 'flushleft',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.4,
                                             scaley: 0.4,
                                             offsetx: 0.45,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['site'] ? form_data['company']['site'] : 'REMONT.RU',
                                         color: form_data['company']['site_color'] ? form_data['company']['site_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 161,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.15,
                                             offsetx: 0.15,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['phone'] ? form_data['company']['phone'] : '+79876543210',
                                         color: form_data['company']['phone_color'] ? form_data['company']['phone_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'middle'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.65
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['cta'] ? form_data['cta'] : 'ЗВОНИТЕ СЕЙЧАС!',
                                         color: form_data['cta_color'] ? form_data['cta_color'] : {red: 255, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.75
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                 ],
                                 keyframes: {}
                             }
                         ]
                    },
                    6: {
                         numframes: 698,
                         framerate: 25,
                         transformations: [
                             {
                                 numframes: 34,
                                 offset: 531,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['question'] ? form_data['question'] : 'Доставить мебель?',
                                         color: form_data['question_color'] ? form_data['question_color'] : {red: 255, green: 255, blue: 0, alpha: 255},
                                         fontsize: 120.0,
                                         width: 1910,
                                         height: 482,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.45,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     },
                                     {
                                         value: form_data['answer'] ? form_data['answer'] : 'Не усложняйте себе жизнь, обратитесь к профессионалам',
                                         color: form_data['answer_color'] ? form_data['answer_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 100.0,
                                         width: 1910,
                                         height: 535,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.45,
                                             offsetx: 0.0,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     },
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 82,
                                 offset: 566,
                                 images: [
                                     {
                                         value: form_data['company']['logo'] ? form_data['company']['logo'] : 'https://www.microcreatives.com/wp-content/uploads/2017/02/Apr-3-2017_Examples-of-Good-and-Bad-Typography-Logos.jpg',
                                         posadjust: {
                                             scalex: 0.15,
                                             scaley: 0.25,
                                             offsetx: 0.25,
                                             offsety: 0.17
                                         },
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 texts: [
                                     {
                                         value: form_data['company']['name'] ? form_data['company']['name'] : 'Ремонтник',
                                         color: form_data['company']['name_color'] ? form_data['company']['name_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 200.0,
                                         width: 764,
                                         height: 430,
                                         alignment: {
                                             x: 'flushleft',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.4,
                                             scaley: 0.4,
                                             offsetx: 0.45,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['site'] ? form_data['company']['site'] : 'REMONT.RU',
                                         color: form_data['company']['site_color'] ? form_data['company']['site_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 161,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.15,
                                             offsetx: 0.15,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['phone'] ? form_data['company']['phone'] : '+79876543210',
                                         color: form_data['company']['phone_color'] ? form_data['company']['phone_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'middle'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.65
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['cta'] ? form_data['cta'] : 'ЗВОНИТЕ СЕЙЧАС!',
                                         color: form_data['cta_color'] ? form_data['cta_color'] : {red: 255, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.75
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                 ],
                                 keyframes: {}
                             }
                         ]
                    },
                    7: {
                         numframes: 815,
                         framerate: 25,
                         transformations: [
                             {
                                 numframes: 43,
                                 offset: 641,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['question'] ? form_data['question'] : 'Доставить мебель?',
                                         color: form_data['question_color'] ? form_data['question_color'] : {red: 255, green: 255, blue: 0, alpha: 255},
                                         fontsize: 120.0,
                                         width: 1910,
                                         height: 482,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.45,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     },
                                     {
                                         value: form_data['answer'] ? form_data['answer'] : 'Не усложняйте себе жизнь, обратитесь к профессионалам',
                                         color: form_data['answer_color'] ? form_data['answer_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 100.0,
                                         width: 1910,
                                         height: 535,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.45,
                                             offsetx: 0.0,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     },
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 86,
                                 offset: 685,
                                 images: [
                                     {
                                         value: form_data['company']['logo'] ? form_data['company']['logo'] : 'https://www.microcreatives.com/wp-content/uploads/2017/02/Apr-3-2017_Examples-of-Good-and-Bad-Typography-Logos.jpg',
                                         posadjust: {
                                             scalex: 0.15,
                                             scaley: 0.25,
                                             offsetx: 0.25,
                                             offsety: 0.17
                                         },
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 texts: [
                                     {
                                         value: form_data['company']['name'] ? form_data['company']['name'] : 'Ремонтник',
                                         color: form_data['company']['name_color'] ? form_data['company']['name_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 200.0,
                                         width: 764,
                                         height: 430,
                                         alignment: {
                                             x: 'flushleft',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.4,
                                             scaley: 0.4,
                                             offsetx: 0.45,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['site'] ? form_data['company']['site'] : 'REMONT.RU',
                                         color: form_data['company']['site_color'] ? form_data['company']['site_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 161,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.15,
                                             offsetx: 0.15,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['phone'] ? form_data['company']['phone'] : '+79876543210',
                                         color: form_data['company']['phone_color'] ? form_data['company']['phone_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'middle'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.65
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['cta'] ? form_data['cta'] : 'ЗВОНИТЕ СЕЙЧАС!',
                                         color: form_data['cta_color'] ? form_data['cta_color'] : {red: 255, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.75
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                 ],
                                 keyframes: {}
                             }
                         ]
                    },
                    8: {
                         numframes: 630,
                         framerate: 24,
                         transformations: [
                             {
                                 numframes: 41,
                                 offset: 1,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['first'] ? form_data['first'] : 'Планируете ремонт?',
                                         color: form_data['first_color'] ? form_data['first_color'] : {red: 28, green: 54, blue: 138, alpha: 255},
                                         fontsize: 200.0,
                                         width: 1297,
                                         height: 729,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 1.0,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     }
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 114,
                                 offset: 42,
                                 blendmode: 4,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['first'] ? form_data['first'] : 'Планируете ремонт?',
                                         color: form_data['first_color'] ? form_data['first_color'] : {red: 28, green: 54, blue: 138, alpha: 255},
                                         fontsize: 200.0,
                                         width: 1297,
                                         height: 729,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 1.0,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 151,
                                                     value: 0.5
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 156,
                                                     value: 0
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 67,
                                 offset: 379,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['question'] ? form_data['question'] : 'Доставить мебель?',
                                         color: form_data['question_color'] ? form_data['question_color'] : {red: 28, green: 54, blue: 138, alpha: 255},
                                         fontsize: 120.0,
                                         width: 1910,
                                         height: 482,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.45,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     },
                                     {
                                         value: form_data['answer'] ? form_data['answer'] : 'Не усложняйте себе жизнь, обратитесь к профессионалам',
                                         color: form_data['answer_color'] ? form_data['answer_color'] : {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 100.0,
                                         width: 1910,
                                         height: 535,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.45,
                                             offsetx: 0.0,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     },
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 133,
                                 offset: 447,
                                 images: [
                                     {
                                         value: form_data['company']['logo'] ? form_data['company']['logo'] : 'https://www.microcreatives.com/wp-content/uploads/2017/02/Apr-3-2017_Examples-of-Good-and-Bad-Typography-Logos.jpg',
                                         posadjust: {
                                             scalex: 0.15,
                                             scaley: 0.25,
                                             offsetx: 0.25,
                                             offsety: 0.17
                                         },
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 texts: [
                                     {
                                         value: form_data['company']['name'] ? form_data['company']['name'] : 'Ремонтник',
                                         color: form_data['company']['name_color'] ? form_data['company']['name_color'] : {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 200.0,
                                         width: 764,
                                         height: 430,
                                         alignment: {
                                             x: 'flushleft',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.4,
                                             scaley: 0.4,
                                             offsetx: 0.45,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['site'] ? form_data['company']['site'] : 'REMONT.RU',
                                         color: form_data['company']['site_color'] ? form_data['company']['site_color'] : {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 161,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.15,
                                             offsetx: 0.15,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['phone'] ? form_data['company']['phone'] : '+79876543210',
                                         color: form_data['company']['phone_color'] ? form_data['company']['phone_color'] : {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'middle'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.65
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['cta'] ? form_data['cta'] : 'ЗВОНИТЕ СЕЙЧАС!',
                                         color: form_data['cta_color'] ? form_data['cta_color'] : {red: 255, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.75
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                 ],
                                 keyframes: {}
                             }
                         ]
                    },
                    9: {
                         numframes: 789,
                         framerate: 24,
                         transformations: [
                             {
                                 numframes: 78,
                                 offset: 1,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['first'] ? form_data['first'] : 'Планируете ремонт?',
                                         color: form_data['first_color'] ? form_data['first_color'] : {red: 28, green: 54, blue: 138, alpha: 255},
                                         fontsize: 200.0,
                                         width: 1297,
                                         height: 729,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 1.0,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 73,
                                                     value: 0.5
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 78,
                                                     value: 0
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 67,
                                 offset: 542,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['question'] ? form_data['question'] : 'Доставить мебель?',
                                         color: form_data['question_color'] ? form_data['question_color'] : {red: 28, green: 54, blue: 138, alpha: 255},
                                         fontsize: 120.0,
                                         width: 1910,
                                         height: 482,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.45,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     },
                                     {
                                         value: form_data['answer'] ? form_data['answer'] : 'Не усложняйте себе жизнь, обратитесь к профессионалам',
                                         color: form_data['answer_color'] ? form_data['answer_color'] : {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 100.0,
                                         width: 1910,
                                         height: 535,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.45,
                                             offsetx: 0.0,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     },
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 133,
                                 offset: 610,
                                 images: [
                                     {
                                         value: form_data['company']['logo'] ? form_data['company']['logo'] : 'https://www.microcreatives.com/wp-content/uploads/2017/02/Apr-3-2017_Examples-of-Good-and-Bad-Typography-Logos.jpg',
                                         posadjust: {
                                             scalex: 0.15,
                                             scaley: 0.25,
                                             offsetx: 0.25,
                                             offsety: 0.17
                                         },
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 texts: [
                                     {
                                         value: form_data['company']['name'] ? form_data['company']['name'] : 'Ремонтник',
                                         color: form_data['company']['name_color'] ? form_data['company']['name_color'] : {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 200.0,
                                         width: 764,
                                         height: 430,
                                         alignment: {
                                             x: 'flushleft',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.4,
                                             scaley: 0.4,
                                             offsetx: 0.45,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['site'] ? form_data['company']['site'] : 'REMONT.RU',
                                         color: form_data['company']['site_color'] ? form_data['company']['site_color'] : {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 161,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.15,
                                             offsetx: 0.15,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['phone'] ? form_data['company']['phone'] : '+79876543210',
                                         color: form_data['company']['phone_color'] ? form_data['company']['phone_color'] : {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'middle'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.65
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['cta'] ? form_data['cta'] : 'ЗВОНИТЕ СЕЙЧАС!',
                                         color: form_data['cta_color'] ? form_data['cta_color'] : {red: 255, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.75
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                 ],
                                 keyframes: {}
                             }
                         ]
                    },
                    10: {
                         numframes: 765,
                         framerate: 24,
                         transformations: [
                             {
                                 numframes: 41,
                                 offset: 1,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['first'] ? form_data['first'] : 'Планируете ремонт?',
                                         color: form_data['first_color'] ? form_data['first_color'] : {red: 28, green: 54, blue: 138, alpha: 255},
                                         fontsize: 200.0,
                                         width: 1297,
                                         height: 729,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 1.0,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     }
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 266,
                                 offset: 42,
                                 blendmode: 4,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['first'] ? form_data['first'] : 'Планируете ремонт?',
                                         color: form_data['first_color'] ? form_data['first_color'] : {red: 28, green: 54, blue: 138, alpha: 255},
                                         fontsize: 200.0,
                                         width: 1297,
                                         height: 729,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 1.0,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     }
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 67,
                                 offset: 513,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['question'] ? form_data['question'] : 'Доставить мебель?',
                                         color: form_data['question_color'] ? form_data['question_color'] : {red: 28, green: 54, blue: 138, alpha: 255},
                                         fontsize: 120.0,
                                         width: 1910,
                                         height: 482,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.45,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     },
                                     {
                                         value: form_data['answer'] ? form_data['answer'] : 'Не усложняйте себе жизнь, обратитесь к профессионалам',
                                         color: form_data['answer_color'] ? form_data['answer_color'] : {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 100.0,
                                         width: 1910,
                                         height: 535,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.45,
                                             offsetx: 0.0,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     },
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 133,
                                 offset: 581,
                                 images: [
                                     {
                                         value: form_data['company']['logo'] ? form_data['company']['logo'] : 'https://www.microcreatives.com/wp-content/uploads/2017/02/Apr-3-2017_Examples-of-Good-and-Bad-Typography-Logos.jpg',
                                         posadjust: {
                                             scalex: 0.15,
                                             scaley: 0.25,
                                             offsetx: 0.25,
                                             offsety: 0.17
                                         },
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 texts: [
                                     {
                                         value: form_data['company']['name'] ? form_data['company']['name'] : 'Ремонтник',
                                         color: form_data['company']['name_color'] ? form_data['company']['name_color'] : {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 200.0,
                                         width: 764,
                                         height: 430,
                                         alignment: {
                                             x: 'flushleft',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.4,
                                             scaley: 0.4,
                                             offsetx: 0.45,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['site'] ? form_data['company']['site'] : 'REMONT.RU',
                                         color: form_data['company']['site_color'] ? form_data['company']['site_color'] : {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 161,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.15,
                                             offsetx: 0.15,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['phone'] ? form_data['company']['phone'] : '+79876543210',
                                         color: form_data['company']['phone_color'] ? form_data['company']['phone_color'] : {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'middle'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.65
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['cta'] ? form_data['cta'] : 'ЗВОНИТЕ СЕЙЧАС!',
                                         color: form_data['cta_color'] ? form_data['cta_color'] : {red: 255, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.75
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                 ],
                                 keyframes: {}
                             }
                         ]
                    },
                    11: {
                         numframes: 501,
                         framerate: 25,
                         transformations: [
                             {
                                 numframes: 73,
                                 offset: 52,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['company']['name'] ? form_data['company']['name'] : 'Ремонтник',
                                         color: {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 893,
                                         height: 128,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.3,
                                             offsetx: 0.0,
                                             offsety: -0.3
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 15,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['site'] ? form_data['company']['site'] : 'REMONT.RU',
                                         color: {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1083,
                                         height: 126,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.2,
                                             offsetx: 0.0,
                                             offsety: 1.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 15,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 89,
                                 offset: 412,
                                 images: [
                                     {
                                         value: form_data['company']['logo'] ? form_data['company']['logo'] : 'https://www.microcreatives.com/wp-content/uploads/2017/02/Apr-3-2017_Examples-of-Good-and-Bad-Typography-Logos.jpg',
                                         posadjust: {
                                             scalex: 0.15,
                                             scaley: 0.25,
                                             offsetx: 0.15,
                                             offsety: 0.15
                                         },
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 texts: [
                                     {
                                         value: form_data['company']['name'] ? form_data['company']['name'] : 'Ремонтник',
                                         color: form_data['company']['name_color'] ? form_data['company']['name_color'] : {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 200.0,
                                         width: 1051,
                                         height: 269,
                                         alignment: {
                                             x: 'flushleft',
                                             y: 'middle'
                                         },
                                         posadjust: {
                                             scalex: 0.55,
                                             scaley: 0.25,
                                             offsetx: 0.4,
                                             offsety: 0.15

                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 30,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['sphere'] ? form_data['company']['sphere'] : 'Ремонт квартир',
                                         color: form_data['company']['sphere_color'] ? form_data['company']['sphere_color'] : {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1339,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.45
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 30,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['site'] ? form_data['company']['site'] : 'REMONT.RU',
                                         color: form_data['company']['site_color'] ? form_data['company']['site_color'] : {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 161,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.55
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 30,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['phone'] ? form_data['company']['phone'] : '+79876543210',
                                         color: form_data['company']['phone_color'] ? form_data['company']['phone_color'] : {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.65
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 30,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['cta'] ? form_data['cta'] : 'ЗВОНИТЕ СЕЙЧАС!',
                                         color: form_data['cta_color'] ? form_data['cta_color'] : {red: 255, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.75
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 30,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                 ],
                                 keyframes: {}
                             }
                         ]
                    },
                    12: {
                         numframes: 1752,
                         framerate: 30,
                         transformations: [
                             {
                                 numframes: 61,
                                 offset: 107,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['company']['name'] ? form_data['company']['name'] : 'Ремонтник',
                                         color: form_data['company']['name_color'] ? form_data['company']['name_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 200.0,
                                         width: 1391,
                                         height: 528,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 1.0,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 46,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 61,
                                                     value: 0
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 84,
                                 offset: 210,
                                 images: [
                                     {
                                         value: form_data['company']['logo'] ? form_data['company']['logo'] : 'https://www.microcreatives.com/wp-content/uploads/2017/02/Apr-3-2017_Examples-of-Good-and-Bad-Typography-Logos.jpg',
                                         posadjust: {
                                             scalex: 0.4,
                                             scaley: 0.4,
                                             offsetx: 0.3,
                                             offsety: 0.1
                                         },
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 15,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 80,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 84,
                                                     value: 0
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 texts: [
                                     {
                                         value: form_data['company']['sphere'] ? form_data['company']['sphere'] : 'Сфера деятельности',
                                         color: form_data['company']['sphere_color'] ? form_data['company']['sphere_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 100.0,
                                         width: 667,
                                         height: 300,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.5,
                                             offsetx: 0.0,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 15,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 80,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 84,
                                                     value: 0
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 69,
                                 offset: 340,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['slogan_1'] ? form_data['slogan_1'] : 'Слоган 1',
                                         color: form_data['slogan_1_color'] ? form_data['slogan_1_color'] : {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 100.0,
                                         width: 1442,
                                         height: 155,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 1.0,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 15,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 62,
                                 offset: 518,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['slogan_2'] ? form_data['slogan_2'] : 'Слоган 2',
                                         color: form_data['slogan_2_color'] ? form_data['slogan_2_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 100.0,
                                         width: 1442,
                                         height: 155,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 1.0,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 15,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 56,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 62,
                                                     value: 0
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 47,
                                 offset: 640,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['slogan_3'] ? form_data['slogan_3'] : 'Слоган 3',
                                         color: form_data['slogan_3_color'] ? form_data['slogan_3_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 100.0,
                                         width: 635,
                                         height: 314,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 1.0,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 6,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 41,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 47,
                                                     value: 0
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 154,
                                 offset: 770,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['slogan_4'] ? form_data['slogan_4'] : 'Слоган 4',
                                         color: form_data['slogan_4_color'] ? form_data['slogan_4_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 100.0,
                                         width: 1508,
                                         height: 199,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 1.0,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 6,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 227,
                                 offset: 936,
                                 images: [
                                     {
                                         value: form_data['photo_1'] ? form_data['photo_1'] : 'http://bipbap.ru/wp-content/uploads/2017/04/leto_derevo_nebo_peyzazh_dom_derevya_domik_priroda_3000x2000.jpg',
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 1.0,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 6,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 88,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 91,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 227,
                                                     value: 0
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['photo_2'] ? form_data['photo_2'] : 'https://www.sunhome.ru/i/wallpapers/200/planeta-zemlya-kartinka.960x540.jpg',
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 1.0,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 91,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 97,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 156,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 159,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 227,
                                                     value: 0
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['photo_3'] ? form_data['photo_3'] : 'https://cs8.pikabu.ru/post_img/big/2017/06/06/2/1496710261194222281.jpg',
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 1.0,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 159,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 165,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 224,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 227,
                                                     value: 0
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 texts: false,
                                 keyframes: {}
                             },
                             {
                                 numframes: 57,
                                 offset: 1280,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['advantage_1'] ? form_data['advantage_1'] : 'Преимущество 1',
                                         color: form_data['advantage_1_color'] ? form_data['advantage_1_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 80.0,
                                         width: 678,
                                         height: 129,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.2,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 2,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 54,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 57,
                                                     value: 0
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['advantage_2'] ? form_data['advantage_2'] : 'Преимущество 2',
                                         color: form_data['advantage_2_color'] ? form_data['advantage_2_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 80.0,
                                         width: 678,
                                         height: 129,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.2,
                                             offsetx: 0.0,
                                             offsety: 0.2
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 4,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 54,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 57,
                                                     value: 0
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['advantage_3'] ? form_data['advantage_3'] : 'Преимущество 3',
                                         color: form_data['advantage_3_color'] ? form_data['advantage_3_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 80.0,
                                         width: 678,
                                         height: 129,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.2,
                                             offsetx: 0.0,
                                             offsety: 0.4
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 6,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 54,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 57,
                                                     value: 0
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['advantage_4'] ? form_data['advantage_4'] : 'Преимущество 4',
                                         color: form_data['advantage_4_color'] ? form_data['advantage_4_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 80.0,
                                         width: 678,
                                         height: 129,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.2,
                                             offsetx: 0.0,
                                             offsety: 0.6
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 8,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 54,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 57,
                                                     value: 0
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['advantage_5'] ? form_data['advantage_5'] : 'Преимущество 5',
                                         color: form_data['advantage_5_color'] ? form_data['advantage_5_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 80.0,
                                         width: 678,
                                         height: 129,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 0.2,
                                             offsetx: 0.0,
                                             offsety: 0.8
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 54,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 57,
                                                     value: 0
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 50,
                                 offset: 1400,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['slogan_5'] ? form_data['slogan_5'] : 'Слоган 5',
                                         color: form_data['slogan_5_color'] ? form_data['slogan_5_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 100.0,
                                         width: 1582,
                                         height: 255,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 1.0,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 76,
                                 offset: 1475,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['slogan_6'] ? form_data['slogan_6'] : 'Слоган 6',
                                         color: form_data['slogan_6_color'] ? form_data['slogan_6_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 100.0,
                                         width: 760,
                                         height: 310,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 1.0,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf'
                                     }
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 57,
                                 offset: 1575,
                                 images: false,
                                 texts: [
                                     {
                                         value: form_data['slogan_7'] ? form_data['slogan_7'] : 'Слоган 7',
                                         color: form_data['slogan_7_color'] ? form_data['slogan_7_color'] : {red: 255, green: 255, blue: 255, alpha: 255},
                                         fontsize: 100.0,
                                         width: 1478,
                                         height: 190,
                                         posadjust: {
                                             scalex: 1.0,
                                             scaley: 1.0,
                                             offsetx: 0.0,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/ofont.ru_League Gothic.ttf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 51,
                                                     value: 1
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 57,
                                                     value: 0
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 keyframes: {}
                             },
                             {
                                 numframes: 97,
                                 offset: 1655,
                                 images: [
                                     {
                                         value: form_data['company']['logo'] ? form_data['company']['logo'] : 'https://www.microcreatives.com/wp-content/uploads/2017/02/Apr-3-2017_Examples-of-Good-and-Bad-Typography-Logos.jpg',
                                         posadjust: {
                                             scalex: 0.15,
                                             scaley: 0.25,
                                             offsetx: 0.25,
                                             offsety: 0.17
                                         },
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 10,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     }
                                 ],
                                 texts: [
                                     {
                                         value: form_data['company']['name'] ? form_data['company']['name'] : 'Ремонтник',
                                         color: form_data['company']['name_color'] ? form_data['company']['name_color'] : {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 200.0,
                                         width: 764,
                                         height: 430,
                                         alignment: {
                                             x: 'flushleft',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.4,
                                             scaley: 0.4,
                                             offsetx: 0.45,
                                             offsety: 0.0
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 6,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['site'] ? form_data['company']['site'] : 'REMONT.RU',
                                         color: form_data['company']['site_color'] ? form_data['company']['site_color'] : {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 161,
                                         alignment: {
                                             x: 'centered',
                                             y: 'bottom'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.15,
                                             offsetx: 0.15,
                                             offsety: 0.5
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 6,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['company']['phone'] ? form_data['company']['phone'] : '+79876543210',
                                         color: form_data['company']['phone_color'] ? form_data['company']['phone_color'] : {red: 0, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'middle'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.65
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 6,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                     {
                                         value: form_data['cta'] ? form_data['cta'] : 'ЗВОНИТЕ СЕЙЧАС!',
                                         color: form_data['cta_color'] ? form_data['cta_color'] : {red: 255, green: 0, blue: 0, alpha: 255},
                                         fontsize: 80.0,
                                         width: 1337,
                                         height: 108,
                                         alignment: {
                                             x: 'centered',
                                             y: 'top'
                                         },
                                         posadjust: {
                                             scalex: 0.7,
                                             scaley: 0.1,
                                             offsetx: 0.15,
                                             offsety: 0.75
                                         },
                                         font: 'userdata/Vanishing Boy BTN.otf',
                                         opacityfunction: {
                                             type: 'keyframe',
                                             advancedkeyframes: [
                                                 {
                                                     type: "frame",
                                                     time: 0,
                                                     value: 0
                                                 },
                                                 {
                                                     type: "frame",
                                                     time: 6,
                                                     value: 1
                                                 }
                                             ]
                                         }
                                     },
                                 ],
                                 keyframes: {}
                             }
                         ]
                    }
                };

                return configs[movie_id];
            }
        });
    </script>
@endsection