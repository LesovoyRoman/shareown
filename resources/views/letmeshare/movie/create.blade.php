@extends('letmeshare.layouts.master', [
	'title' => 'Создать фильм для друга',
	'scripts' => [
			['https://maps.googleapis.com/maps/api/js?key=AIzaSyChC7vrJb_g4OcQvrvLCjkVaBRrWolGisE&libraries=places&callback=autocompletePlaces', 'async defer']
	]
])

@section('content')


	<section class="features11 cid-qYauaRapkU mbr-parallax-background" id="features11-14">
		<div class="mbr-overlay" style="opacity: 0.8;">
		</div>

        @php $data['movie_price'] > 0 ? $price = $data['movie_price'] : $price = '';  @endphp
		<div class="container">
			@if ($errors->any())
				<div class="alert alert-danger">
					{{ __('Внимание! При сохранении формы возникли ошибки: ') }}
					<ul>
						@php $advantage = false; @endphp
						@php $slogan = false; @endphp
						@foreach ($errors->all() as $error)
							@if (strpos($error, 'name field is required') !== false)
							   <li>{{ __('Заполните поле "Имя Компании"') }}</li>
							@elseif (strpos($error, 'name may not be greater') !== false)
								<li>{{ __('Слишком длинное поле "Имя Компании"') }}</li>
							@endif
							@if (strpos($error, 'phone field is required') !== false)
								<li>{{ __('Заполните поле "Телефон"') }}</li>
							@elseif (strpos($error, 'phone may not be greater') !== false)
									<li>{{ __('Слишком длинное поле "Телефон"') }}</li>
							@endif
							@if (strpos($error, 'site field is required') !== false)
								<li>{{ __('Заполните поле "Название сайта"') }}</li>
							@elseif (strpos($error, 'site may not be greater') !== false)
								<li>{{ __('Слишком длинное поле "Название сайта"') }}</li>
							@endif
							@if (strpos($error, 'sphere field is required') !== false)
								<li>{{ __('Заполните поле "Сфера деятельности"') }}</li>
							@elseif (strpos($error, 'sphere may not be greater') !== false)
								<li>{{ __('Слишком длинное поле "Сфера деятельности"') }}</li>
							@endif
							@if (strpos($error, 'cta field is required') !== false)
								<li>{{ __('Заполните поле "Призыв к действию"') }}</li>
							@elseif (strpos($error, 'cta may not be greater') !== false)
								<li>{{ __('Слишком длинное поле "Призыв к действию"') }}</li>
							@endif

							@if (strpos($error, 'slogan') !== false && $slogan == false)
								@php $slogan = true; @endphp
								<li>{{ __('Заполните все поля "Слоган" а также эти поля не могут быть длиннее 50 символов') }}</li>
							@endif
							@if (strpos($error, 'advantage') !== false && $advantage == false)
									@php $advantage = true; @endphp
								<li>{{ __('Заполните все поля "Преимущество" а также эти поля не могут быть длиннее 50 символов') }}</li>
							@endif

							@if (strpos($error, 'answer field is required') !== false)
								<li>{{ __('Заполните поле "Ответ"') }}</li>
							@elseif (strpos($error, 'answer may not be greater') !== false)
								<li>{{ __('Слишком длинное поле "Ответ"') }}</li>
							@endif
							@if (strpos($error, 'question field is required') !== false)
								<li>{{ __('Заполните поле "Интригующий вопрос"') }}</li>
							@elseif (strpos($error, 'question may not be greater') !== false)
								<li>{{ __('Слишком длинное поле "Интригующий вопрос"') }}</li>
							@endif
							@if (strpos($error, 'first field is required') !== false)
								<li>{{ __('Заполните поле "Промо"') }}</li>
							@elseif (strpos($error, 'first may not be greater') !== false)
								<li>{{ __('Слишком длинное поле "Промо"') }}</li>
							@endif
						@endforeach
					</ul>
				</div>
			@endif


			<div class="container-flex-custom">

				<div class="text-content">
					<div class="media-container-row ">
						<div class="col-md-12 mb-4">
							<h2 class="mb-4 mbr-fonts-style mbr-section-title display-2">Создание видео</h2>
							{{--<h3 class="mbr-section-subtitle mbr-fonts-style mb-4 display-5">
								Добавьте логотип</h3>--}}

							<div class="col-md-12">
								{!! Form::open(['route' => 'movies.processing', 'files' => true, 'id' => 'form-create-movie', 'class' => 'form-horizontal']) !!}

								{{ Form::hidden('template_movie_id', $data['template_movie_id']) }}
								{{ Form::hidden('movie_category_id', $data['movie_category_id']) }}

								@switch($data['template_movie_id'])
								@case(1)
								@case(2)
								@case(3)
								@case(4)
								@case(5)
								@case(6)
								@case(7)

								<div class="tabs_box">
									<ul class="tabs_menu">
										<li><a data-href-tab="prev" class="linkToTab"><img src="{{ asset('letmeshare/assets/images/arrowNewRight.png') }}" alt=""></a></li>
										<li class="progressLine">
											<div class="point-progress active" id="progressPoint1"></div>
											<div class="point-progress" id="progressPoint2"></div>
											<div class="point-progress" id="progressPoint3"></div>
											<div class="line-progress active" id="lineprogress1"></div>
											<div class="line-progress" id="lineprogress2"></div>
											<div class="line-progress" id="lineprogress3"></div>
										</li>
										<li><a data-href-tab="next" class="linkToTab"><img src="{{ asset('letmeshare/assets/images/arrowNewLeft.png') }}" alt=""></a>
										</li>
									</ul>
									<div class="tab active notAnimate" id="tab1" data-id-tab="1">

										<h3 class="mb-4 mbr-fonts-style mbr-section-title display-6">Выбор музыки и озвучки</h3>

										<div class="form-group ">
											<label for="noVoiceFilm">Без озвучки</label>
											<input type="checkbox" id="noVoiceFilm" name="noVoice" value="1">
											<br>
											<label for="noMusicFilm">Без музыки</label>
											<input type="checkbox" id="noMusicFilm" name="noMusic" value="1">
										</div>

										<div class="form-group ">

											<label  id="chooseMusic">Выберите фоновую музыку</label>
											<select id="select_music" name="movie_{{$data['template_movie_id']}}[bg_music_id]" class="bg_music_id form-control">
												<option value="1" data-music-href="http://sag.devbrother.com/storage/ElectroFuturisticLogoIntroNoMelody.wav">Electro Futuristic</option>
												<option value="2" data-music-href="http://sag.devbrother.com/storage/Fun-30sec_2.wav">Fun</option>
												<option value="3" data-music-href="http://sag.devbrother.com/storage/Comedy.wav">Comedy</option>
												<option value="4" data-music-href="http://sag.devbrother.com/storage/Quirky.wav">Quirky</option>
												<option value="6" data-music-href="http://sag.devbrother.com/storage/Quirky_second_version_30.wav">Quirky second version</option>
											</select>
                                            <audio id="player_music" controls="controls">
                                                <source id="mp3_src_music" src="http://sag.devbrother.com/storage/ElectroFuturisticLogoIntroNoMelody.wav" type="audio/mp3" />Your browser does not support the audio element.
                                            </audio>
											<br>
											<label id="chooseVoice">Выберите озвучку</label>
											<select id="select_audio" name="movie_{{$data['template_movie_id']}}[bg_audio_id]" class="bg_music_id form-control">
												<option value="1" data-music-href="http://sag.devbrother.com/storage/audiotags/1/1.wav">1 Доставка груза</option>
												<option value="2" data-music-href="http://sag.devbrother.com/storage/audiotags/1/2.wav">2 Доставка груза</option>
												<option value="3" data-music-href="http://sag.devbrother.com/storage/audiotags/1/3.wav">3 Доставка груза</option>
												<option value="4" data-music-href="http://sag.devbrother.com/storage/audiotags/1/4.wav">4 Доставка груза</option>
												<option value="5" data-music-href="http://sag.devbrother.com/storage/audiotags/1/5.wav">5 Доставка груза</option>
												<option value="6" data-music-href="http://sag.devbrother.com/storage/audiotags/1/6.wav">6 Доставка груза</option>
												<option value="7" data-music-href="http://sag.devbrother.com/storage/audiotags/1/7.wav">7 Доставка груза</option>
												<option value="8" data-music-href="http://sag.devbrother.com/storage/audiotags/1/8.wav">8 Доставка груза</option>
												<option value="9" data-music-href="http://sag.devbrother.com/storage/audiotags/1/9.wav">9 Доставка груза</option>
												<option value="10" data-music-href="http://sag.devbrother.com/storage/audiotags/1/10.wav">10 Доставка груза</option>
												<option value="11" data-music-href="http://sag.devbrother.com/storage/audiotags/1/11.wav">11 Доставка груза</option>
												<option value="12" data-music-href="http://sag.devbrother.com/storage/audiotags/1/12.wav">12 Доставка груза</option>
												<option value="13" data-music-href="http://sag.devbrother.com/storage/audiotags/1/13.wav">13 Доставка груза</option>
												<option value="14" data-music-href="http://sag.devbrother.com/storage/audiotags/1/14.wav">14 Доставка груза</option>
												<option value="15" data-music-href="http://sag.devbrother.com/storage/audiotags/1/15.wav">15 Доставка груза</option>
												<option value="16" data-music-href="http://sag.devbrother.com/storage/audiotags/1/16.wav">16 Доставка груза</option>
                                                <option value="66" data-music-href="http://sag.devbrother.com/storage/audiotags/4/nadoelamebel/1.wav">17 Надоела мебель?</option>
                                                <option value="77" data-music-href="http://sag.devbrother.com/storage/audiotags/4/nadoelamebel/2.wav">18 Надоела мебель?</option>
                                                <option value="88" data-music-href="http://sag.devbrother.com/storage/audiotags/4/nadoelamebel/3.wav">19 Надоела мебель?</option>
                                                <option value="99" data-music-href="http://sag.devbrother.com/storage/audiotags/4/nadoelamebel/4.wav">20 Надоела мебель?</option>
											</select>

											<audio id="player_voice" controls="controls">
												<source id="mp3_src_voice" src="http://sag.devbrother.com/storage/audiotags/1/1.wav" type="audio/mp3" />Your browser does not support the audio element.
											</audio>

											<div class="voice-text">
												<p id="textVoice1" class="textVoice">
													- Вам нужно перевести груз, думаете о том, как сделать все своими силами.<br>
													- Не мучьте себя, доверьте эту работу профессионалам.<br>
													- Звоните сегодня
												</p>
												<p id="textVoice2" class="textVoice">- Делаете ремонт и нуждаетесь в доставке мебели,<br>
													материалов или вывозе мусора?<br>
													- Думаем о том, как сделать все своими силами?<br>
													- Не мучьте себя, доверьте эту работу профессионалам.<br>
													- Звоните сегодня
												</p>
												<p id="textVoice3" class="textVoice">
													- Купили новую мебель, тяжелый шкаф или пианино, не знаете как занести в квартиру?<br>
													- Думаете о том, как сделать все своими силами.<br>
													- Не мучьте себя, доверьте эту работу профессионалам.<br>
													- Звоните сегодня
												</p>

												<p id="textVoice4" class="textVoice">
													- Нужны услуги грузоперевозок?<br>
													- Думаете о том, как сделать все своими силами?<br>
													- Не мучьте себя, доверьте эту работу профессионалам.<br>
													- Звоните сегодня
												</p>

												<p id="textVoice5" class="textVoice">
													- Нужны услуги грузоперевозок?<br>
													- Думаете о том, как сделать все своими силами?<br>
													- Не мучьте себя, облегчим вашу жизнь выполнив доставку аккуратно и точно в срок.<br>
													- Звоните сегодня
												</p>

												<p id="textVoice6" class="textVoice">
													- Нужны услуги грузоперевозок?<br>
													- Хотите сделать все своими силами?<br>
													- Поберегите свои нервы и имущество.<br>
													- Обращайтесь в нашу компанию, мы поможем!<br>
													- Звоните сейчас!
												</p>

												<p id="textVoice7" class="textVoice">
													- Купили новую мебель, тяжелый шкаф или пианино, не знаете как занести в квартиру?<br>
													- Хотите сделать все своими силами?<br>
													- Поберегите свои нервы и имущество.<br>
													- Обращайтесь в нашу компанию, мы поможем!<br>
													- Звоните сейчас!
												</p>

												<p id="textVoice8" class="textVoice">
													- Делаете ремонт и нуждаетесь в доставке мебели, материалов или вывозе мусора?<br>
													- Хотите сделать все своими силами?<br>
													- Поберегите свои нервы и имущество.<br>
													- Обращайтесь в нашу компанию, мы поможем!<br>
													- Звоните сейчас!
												</p>

												<p id="textVoice9" class="textVoice">
													- Вам нужно перевезти груз?<br>
													- Хотите сделать все своими силами?<br>
													- Поберегите свои нервы и имущество.<br>
													- Обращайтесь в нашу компанию, мы поможем!<br>
													- Звоните сейчас!
												</p>

												<p id="textVoice10" class="textVoice">
													- Вам нужно перевезти груз?<br>
													- Хотите все сделать сами?<br>
													- Не усложняйте себе жизнь, обратитесь в профессиональную компанию, звоните прямо сейчас!
												</p>

												<p id="textVoice11" class="textVoice">
													- Делаете ремонт и нуждаетесь в доставке мебели, материалов или вывозе мусора?<br>
													- Хотите все сделать сами?<br>
													- Не усложняйте себе жизнь, обратитесь в профессиональную компанию, звоните прямо сейчас!
												</p>

												<p id="textVoice12" class="textVoice">
													- Купили новую мебель, тяжелый шкаф или пианино, не знаете как занести в квартиру?<br>
													- Хотите все сделать сами?<br>
													- Не усложняйте себе жизнь, обратитесь в профессиональную компанию, звоните прямо сейчас!
												</p>

												<p id="textVoice13" class="textVoice">
													- Нужны услуги грузоперевозок?<br>
													- Хотите все сделать сами?<br>
													- Не усложняйте себе жизнь, обратитесь в профессиональную компанию, звоните прямо сейчас!
												</p>

												<p id="textVoice14" class="textVoice">
													- Нужны услуги грузоперевозок?<br>
													- Думаете как все сделать своими силами?<br>
													- Поберегите свои нервы и имущество.<br>
													- Обращайтесь в нашу компанию, мы поможем!<br>
													- Звоните сейчас!
												</p>

												<p id="textVoice15" class="textVoice">
													- Купили новую мебель, тяжелый шкаф или пианино, не знаете как занести в квартиру?<br>
													- Думаете как все сделать своими силами?<br>
													- Поберегите свои нервы и имущество.<br>
													- Обращайтесь в нашу компанию, мы поможем!<br>
													- Звоните сейчас!
												</p>

												<p id="textVoice16" class="textVoice">
													- Делаете ремонт и нуждаетесь в доставке мебели, материалов или вывозе мусора?<br>
													- Думаете как все сделать своими силами?<br>
													- Поберегите свои нервы и имущество.<br>
													- Обращайтесь в нашу компанию, мы поможем!<br>
													- Звоните прямо сейчас!
												</p>
                                                <p id="textVoice66" class="textVoice">
                                                    - Надоела старая мебель?<br>
                                                    - Думаете о том, как сделать все своими силами?<br>
                                                    - Не мучьте себя!<br>
                                                    - Доверьте эту работу профессионалам.<br>
                                                    - Звоните сегодня!
                                                </p>
                                                <p id="textVoice77" class="textVoice">
                                                    - Надоела старая мебель?<br>
                                                    - Хотите сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество, обращайтесь в нашу компанию, мы поможем! <br>
                                                    - Звоните сейчас!
                                                </p>
                                                <p id="textVoice88" class="textVoice">
                                                    - Надоела старая мебель?<br>
                                                    - Хотите все сделать сами?<br>
                                                    - Не усложняйте себе жизнь, обратитесь в профессиональную компанию!<br>
                                                    - Звоните прямо сейчас
                                                </p>
                                                <p id="textVoice99" class="textVoice">
                                                    - Надоела старая мебель?<br>
                                                    - Думаете о том, как сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество, обращайтесь в нашу компанию, мы поможем!<br>
                                                    - Звоните прямо сейчас!
                                                </p>

											</div>
										</div>
									</div>
									<div class="tab notAnimate" id="tab2" data-id-tab="2">

										<h3 class="mb-4 mbr-fonts-style mbr-section-title display-6">Заполнение основных полей с данными</h3>

										<div class="form-group">
											<label>Интригующий вопрос:</label>
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[question_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:40" name="movie_{{$data['template_movie_id']}}[question]" class="question form-control" />
										</div>

										<div class="form-group">
											<label>Ответ:</label>
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[answer_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:40" name="movie_{{$data['template_movie_id']}}[answer]" class="answer form-control" value="" />
										</div>

										<div class="form-group">
											<label>Название компании:</label>
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[company][name_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input data-validate="required|max:40" type="text" name="movie_{{$data['template_movie_id']}}[company][name]" class="company_name form-control" value="" />
										</div>

										<div class="form-group">
											<label>Сайт:</label>
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[company][site_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input data-validate="required|max:60" type="text" name="movie_{{$data['template_movie_id']}}[company][site]" class="company_site form-control" value="" />
										</div>

										<div class="form-group">
											<label>Телефон:</label>
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[company][phone_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text"  data-validate="required|max:25" name="movie_{{$data['template_movie_id']}}[company][phone]" class="company_phone form-control" value="" />
										</div>

										<div class="form-group">
											<label>Призыв к действию:</label>
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[cta_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text"  data-validate="required|max:60" name="movie_{{$data['template_movie_id']}}[cta]" class="cta form-control" value="" />
										</div>
									</div>
									<div class="tab notAnimate" id="tab3" data-id-tab="3">

										<h3 class="mb-4 mbr-fonts-style mbr-section-title display-6">Загрузка лого вашей компании</h3>

										<div class="form-group form_upload_photo">
											<div class="" id="holder_block_photo_film">
												<span class="string_error_field">@if ($errors->has('imgBase64')) {{ $errors->first('imgBase64') }} @endif</span>
												<span class="user_path_to_photo"></span>
												{!! Form::label('photo', 'Загрузите лого компании', ['class'=>'label_btn_upload_photo btn btn-primary']) !!}
												<input type="file" name="movie_{{$data['template_movie_id']}}[company][logo]" id="photo" class="hidden hidden_image_64" accept="image/*" />
												{!! Form::hidden('imgBase64', old('imgBase64'), ['class'=>'hidden', 'id'=>'imgBase64']) !!}
												<a id="change_result_cropped" class="btn btn-primary">Изменить позицию</a>
											</div>
										</div>

                                        @if($price > 0)
                                        <div class="form-group hiddden" id="formPayment">
                                            <h3 class="mb-4 mbr-fonts-style mbr-section-title display-6">Выберите способ оплаты</h3>
                                            <label for="checkedPaypal">PayPal</label>
                                            <input type="radio" id="checkedPaypal" name="paymentVariant" value="1">
                                            <br>
                                            <label for="checkedYandex">Yandex Money</label>
                                            <input type="radio" id="checkedYandex" name="paymentVariant" value="2">
                                        </div>
                                        @endif

                                        <div class="form-group">
                                            <div class="">
												<span class="error_submit"></span>
                                                {!! Form::submit( $price ? __('Создать фильм ') . $price . '$' : __('Создать фильм '), ['class' => 'btn btn-primary hiddden', 'id' => 'submit_create_film']) !!}
                                            </div>
                                        </div>

                                        <div class="media-container-row">
                                            <div class="col-md-12">
                                                <div class="col-md-12">
                                                    <div class="block_holder_img_preview_film">
                                                        <img src="" alt="" id="result_cropped">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

									</div>
								</div>

									<!-- Background music selector -->

								@break


								@case(8)
								@case(9)
								@case(10)
									<!-- Background music selector -->
								<div class="tabs_box">
									<ul class="tabs_menu">
										<li><a data-href-tab="prev" class="linkToTab"><img src="{{ asset('letmeshare/assets/images/arrowNewRight.png') }}" alt=""></a></li>
										<li class="progressLine">
											<div class="point-progress active" id="progressPoint1"></div>
											<div class="point-progress" id="progressPoint2"></div>
											<div class="point-progress" id="progressPoint3"></div>
											<div class="line-progress active" id="lineprogress1"></div>
											<div class="line-progress" id="lineprogress2"></div>
											<div class="line-progress" id="lineprogress3"></div>
										</li>
										<li><a data-href-tab="next" class="linkToTab"><img src="{{ asset('letmeshare/assets/images/arrowNewLeft.png') }}" alt=""></a></li>
									</ul>
									<div class="tab active notAnimate" id="tab1" data-id-tab="1">

										<h3 class="mb-4 mbr-fonts-style mbr-section-title display-6">Выбор музыки и озвучки</h3>

                                        <div class="form-group ">
                                            <label for="noVoiceFilm">Без озвучки</label>
                                            <input type="checkbox" id="noVoiceFilm" name="noVoice" value="1">
                                            <br>
                                            <label for="noMusicFilm">Без музыки</label>
                                            <input type="checkbox" id="noMusicFilm" name="noMusic" value="1">
                                        </div>

									<div class="form-group">

										<label  id="chooseMusic">Выберите фоновую музыку</label>
										<select id="select_music" name="movie_{{$data['template_movie_id']}}[bg_music_id]" class="bg_music_id form-control">
											<option value="1" data-music-href="http://sag.devbrother.com/storage/ElectroFuturisticLogoIntroNoMelody.wav">Electro Futuristic</option>
											<option value="2" data-music-href="http://sag.devbrother.com/storage/Fun-30sec_2.wav">Fun</option>
											<option value="3" data-music-href="http://sag.devbrother.com/storage/Comedy.wav">Comedy</option>
											<option value="4" data-music-href="http://sag.devbrother.com/storage/Quirky.wav">Quirky</option>
											<option value="6" data-music-href="http://sag.devbrother.com/storage/Quirky_second_version_30.wav">Quirky second version</option>
										</select>

                                        <audio id="player_music" controls="controls">
                                            <source id="mp3_src_music" src="http://sag.devbrother.com/storage/ElectroFuturisticLogoIntroNoMelody.wav" type="audio/mp3" />Your browser does not support the audio element.
                                        </audio>

										<br>

										<label id="chooseVoice">Выберите озвучку</label>
										@if($data['template_movie_id'] == 8)
											<select id="select_audio" name="movie_{{$data['template_movie_id']}}[bg_audio_id]" class="bg_music_id form-control">
												<option value="17" data-music-href="http://sag.devbrother.com/storage/audiotags/2/sobratmebel/1.wav">1 Сборка мебели</option>
												<option value="18" data-music-href="http://sag.devbrother.com/storage/audiotags/2/sobratmebel/2.wav">2 Сборка мебели</option>
												<option value="19" data-music-href="http://sag.devbrother.com/storage/audiotags/2/sobratmebel/3.wav">3 Сборка мебели</option>
												<option value="20" data-music-href="http://sag.devbrother.com/storage/audiotags/2/stalyarroboty/1.wav">1 Столяр. работы</option>
												<option value="21" data-music-href="http://sag.devbrother.com/storage/audiotags/2/stalyarroboty/2.wav">2 Столяр. работы</option>
												<option value="22" data-music-href="http://sag.devbrother.com/storage/audiotags/2/stalyarroboty/3.wav">3 Столяр. работы</option>
												<option value="23" data-music-href="http://sag.devbrother.com/storage/audiotags/2/stalyarroboty/4.wav">4 Столяр. работы</option>

                                                <option value="143" data-music-href="http://sag.devbrother.com/storage/audiotags/4/zadumaliremont/1.wav">1 Задумали ремонт?</option>
                                                <option value="144" data-music-href="http://sag.devbrother.com/storage/audiotags/4/zadumaliremont/2.wav">2 Задумали ремонт?</option>
                                                <option value="145" data-music-href="http://sag.devbrother.com/storage/audiotags/4/zadumaliremont/3.wav">3 Задумали ремонт?</option>
                                                <option value="146" data-music-href="http://sag.devbrother.com/storage/audiotags/4/zadumaliremont/4.wav">4 Задумали ремонт?</option>

											</select>

                                            <audio id="player_voice" controls="controls">
                                                <source id="mp3_src_voice" src="http://sag.devbrother.com/storage/audiotags/2/sobratmebel/1.wav" type="audio/mp3" />Your browser does not support the audio element.
                                            </audio>

                                            <div class="voice-text">
                                                <p id="textVoice17" class="textVoice">
                                                    - Вам нужно собрать мебель?<br>
                                                    - Хотите сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество!<br>
                                                    - Обращайтесь в нашу компанию, мы поможем.<br>
                                                    - Звоните сейчас!
                                                </p>
                                                <p id="textVoice18" class="textVoice">
                                                    - Вам нужно собрать мебель?<br>
                                                    - Хотите сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество!<br>
                                                    - Обращайтесь в нашу компанию, мы поможем.<br>
                                                    - Звоните сейчас!
                                                </p>
                                                <p id="textVoice19" class="textVoice">
                                                    - Вам нужно собрать мебель?<br>
                                                    - Хотите все сделать сами?<br>
                                                    - Не усложняйте себе жизнь.<br>
                                                    - Обратитесь в профессиональную компанию.<br>
                                                    - Звоните прямо сейчас.
                                                </p>
                                                <p id="textVoice20" class="textVoice">
                                                    - Задумали столярные работы?<br>
                                                    - Думаете о том, как сделать все своими силами?<br>
                                                    - Не мучьте себя, доверьте эту работу профессионалам, звоните сегодня
                                                </p>
                                                <p id="textVoice21" class="textVoice">
                                                    - Задумали столярные работы?<br>
                                                    - Хотите сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество!<br>
                                                    - Обращайтесь в нашу компанию, мы поможем.<br>
                                                    - Звоните сейчас
                                                </p>
                                                <p id="textVoice22" class="textVoice">
                                                    - Задумали столярные работы?<br>
                                                    - Хотите все сделать сами?<br>
                                                    - Не усложняйте себе жизнь.<br>
                                                    - Обратитесь в профессиональную компанию.<br>
                                                    - Звоните прямо сейчас.
                                                </p>
                                                <p id="textVoice23" class="textVoice">
                                                    - Задумали столярные работы?<br>
                                                    - Думаете о том, как сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество!<br>
                                                    - Обращайтесь в нашу компанию, мы поможем.<br>
                                                    - Звоните прямо сейчас
                                                </p>

                                                <p id="textVoice143" class="textVoice">
                                                    - Задумали ремонт?<br>
                                                    - Думаете о том, как сделать все своими силами?<br>
                                                    - Не мучьте себя! Доверьте эту работу профессионалам.<br>
                                                    - Звоните сегодня!
                                                </p>
                                                <p id="textVoice144" class="textVoice">
                                                    - Задумали ремонт?<br>
                                                    - Хотите сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество, обращайтесь в нашу компанию, мы поможем, звоните сейчас!
                                                </p>
                                                <p id="textVoice145" class="textVoice">
                                                    - Задумали ремонт?<br>
                                                    - Хотите все сделать сами?<br>
                                                    - Не усложняйте себе жизнь, обратитесь в профессиональную компанию, звоните прямо сейчас
                                                </p>
                                                <p id="textVoice146" class="textVoice">
                                                    - Задумали ремонт?<br>
                                                    - Думаете о том, как сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество, обращайтесь в нашу компанию, мы поможем, звоните прямо сейчас!
                                                </p>
                                            </div>


										@elseif($data['template_movie_id'] == 9)
											<select id="select_audio" name="movie_{{$data['template_movie_id']}}[bg_audio_id]" class="bg_music_id form-control">
												<option value="24" data-music-href="http://sag.devbrother.com/storage/audiotags/2/condizioner/1.wav">1 Кондиционер</option>
												<option value="25" data-music-href="http://sag.devbrother.com/storage/audiotags/2/condizioner/2.wav">2 Кондиционер</option>
												<option value="26" data-music-href="http://sag.devbrother.com/storage/audiotags/2/condizioner/3.wav">3 Кондиционер</option>
												<option value="27" data-music-href="http://sag.devbrother.com/storage/audiotags/2/ventilyaziya/1.wav">1 Вентиляция</option>
												<option value="28" data-music-href="http://sag.devbrother.com/storage/audiotags/2/ventilyaziya/2.wav">2 Вентиляция</option>
												<option value="29" data-music-href="http://sag.devbrother.com/storage/audiotags/2/ventilyaziya/3.wav">3 Вентиляция</option>
												<option value="30" data-music-href="http://sag.devbrother.com/storage/audiotags/2/ventilyaziya/4.wav">4 Вентиляция</option>

                                                <option value="143" data-music-href="http://sag.devbrother.com/storage/audiotags/4/zadumaliremont/1.wav">1 Задумали ремонт?</option>
                                                <option value="144" data-music-href="http://sag.devbrother.com/storage/audiotags/4/zadumaliremont/2.wav">2 Задумали ремонт?</option>
                                                <option value="145" data-music-href="http://sag.devbrother.com/storage/audiotags/4/zadumaliremont/3.wav">3 Задумали ремонт?</option>
                                                <option value="146" data-music-href="http://sag.devbrother.com/storage/audiotags/4/zadumaliremont/4.wav">4 Задумали ремонт?</option>
											</select>

                                            <audio id="player_voice" controls="controls">
                                                <source id="mp3_src_voice" src="http://sag.devbrother.com/storage/audiotags/2/condizioner/1.wav" type="audio/mp3" />Your browser does not support the audio element.
                                            </audio>

                                            <div class="voice-text">
                                                <p id="textVoice24" class="textVoice">
                                                    - Хотите установить кондиционер?<br>
                                                    - Хотите сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество!<br>
                                                    - Обращайтесь в нашу компанию, мы поможем, звоните сейчас
                                                </p>
                                                <p id="textVoice25" class="textVoice">
                                                    - Хотите установить кондиционер?<br>
                                                    - Хотите сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество!<br>
                                                    - Обращайтесь в нашу компанию, мы поможем, звоните сейчас
                                                </p>
                                                <p id="textVoice26" class="textVoice">
                                                    - Хотите установить кондиционер?<br>
                                                    - Хотите все сделать сами?<br>
                                                    - Не усложняйте себе жизнь.<br>
                                                    - Обратитесь в профессиональную компанию.<br>
                                                    - Звоните прямо сейчас!

                                                </p>
                                                <p id="textVoice27" class="textVoice">
                                                    - Нужны работы по вентиляции?<br>
                                                    - Думаете о том, как все сделать своими силами?<br>
                                                    - Не мучьте себя.<br>
                                                    - Доверьте эту работу профессионалам!<br>
                                                    - Звоните сегодня!
                                                </p>
                                                <p id="textVoice28" class="textVoice">
                                                    - Нужны работы по вентиляции?<br>
                                                    - Хотите сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество!<br>
                                                    - Обращайтесь в нашу компанию, мы поможем, звоните сейчас
                                                </p>
                                                <p id="textVoice29" class="textVoice">
                                                    - Нужны работы по вентиляции?<br>
                                                    - Хотите все сделать сами?<br>
                                                    - Не усложняйте себе жизнь.<br>
                                                    - Обратитесь в профессиональную компанию.<br>
                                                    - Звоните прямо сейчас!
                                                </p>
                                                <p id="textVoice30" class="textVoice">
                                                    - Нужны работы по вентиляции?<br>
                                                    - Думаете о том, как все сделать своими силами?<br>
                                                    - Поберегите свои нервы и имущество!<br>
                                                    - Обращайтесь в нашу компанию, мы поможем, звоните сейчас
                                                </p>

                                                <p id="textVoice143" class="textVoice">
                                                    - Задумали ремонт?<br>
                                                    - Думаете о том, как сделать все своими силами?<br>
                                                    - Не мучьте себя! Доверьте эту работу профессионалам.<br>
                                                    - Звоните сегодня!
                                                </p>
                                                <p id="textVoice144" class="textVoice">
                                                    - Задумали ремонт?<br>
                                                    - Хотите сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество, обращайтесь в нашу компанию, мы поможем, звоните сейчас!
                                                </p>
                                                <p id="textVoice145" class="textVoice">
                                                    - Задумали ремонт?<br>
                                                    - Хотите все сделать сами?<br>
                                                    - Не усложняйте себе жизнь, обратитесь в профессиональную компанию, звоните прямо сейчас
                                                </p>
                                                <p id="textVoice146" class="textVoice">
                                                    - Задумали ремонт?<br>
                                                    - Думаете о том, как сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество, обращайтесь в нашу компанию, мы поможем, звоните прямо сейчас!
                                                </p>

                                            </div>



										@elseif($data['template_movie_id'] == 10)
											<select id="select_audio" name="movie_{{$data['template_movie_id']}}[bg_audio_id]" class="bg_music_id form-control">
												<option value="31" data-music-href="http://sag.devbrother.com/storage/audiotags/2/elektrika/1.wav">1 Електрика</option>
												<option value="32" data-music-href="http://sag.devbrother.com/storage/audiotags/2/elektrika/2.wav">2 Електрика</option>
												<option value="33" data-music-href="http://sag.devbrother.com/storage/audiotags/2/elektrika/3.wav">3 Електрика</option>
												<option value="34" data-music-href="http://sag.devbrother.com/storage/audiotags/2/elektrika/4.wav">4 Електрика</option>
												<option value="35" data-music-href="http://sag.devbrother.com/storage/audiotags/2/provodka/1.wav">1 Проводка</option>
												<option value="36" data-music-href="http://sag.devbrother.com/storage/audiotags/2/provodka/2.wav">2 Проводка</option>
												<option value="37" data-music-href="http://sag.devbrother.com/storage/audiotags/2/provodka/3.wav">3 Проводка</option>
												<option value="38" data-music-href="http://sag.devbrother.com/storage/audiotags/2/provodka/4.wav">4 Проводка</option>

                                                <option value="143" data-music-href="http://sag.devbrother.com/storage/audiotags/4/zadumaliremont/1.wav">1 Задумали ремонт?</option>
                                                <option value="144" data-music-href="http://sag.devbrother.com/storage/audiotags/4/zadumaliremont/2.wav">2 Задумали ремонт?</option>
                                                <option value="145" data-music-href="http://sag.devbrother.com/storage/audiotags/4/zadumaliremont/3.wav">3 Задумали ремонт?</option>
                                                <option value="146" data-music-href="http://sag.devbrother.com/storage/audiotags/4/zadumaliremont/4.wav">4 Задумали ремонт?</option>
											</select>

                                            <audio id="player_voice" controls="controls">
                                                <source id="mp3_src_voice" src="http://sag.devbrother.com/storage/audiotags/2/elektrika/1.wav" type="audio/mp3" />Your browser does not support the audio element.
                                            </audio>

                                            <div class="voice-text">
                                                <p id="textVoice31" class="textVoice">
                                                    - Нужны работы по электрике?<br>
                                                    - Хотите все своими силами?<br>
                                                    - Поберегите свои нервы и имущество!<br>
                                                    - Обращайтесь в нашу компанию, мы поможем.<br>
                                                    - Звоните сейчас
                                                </p>
                                                <p id="textVoice32" class="textVoice">
                                                    - Нужны работы по электрике?<br>
                                                    - Хотите все своими силами?<br>
                                                    - Поберегите свои нервы и имущество!<br>
                                                    - Обращайтесь в нашу компанию, мы поможем.<br>
                                                    - Звоните сейчас
                                                </p>
                                                <p id="textVoice33" class="textVoice">
                                                    - Нужны работы по электрике?<br>
                                                    - Хотите все сделать сами?<br>
                                                    - Не усложняйте себе жизнь, обратитесь в профессиональную компанию, звоните прямо сейчас!
                                                </p>
                                                <p id="textVoice34" class="textVoice">
                                                    - Нужны работы по электрике?<br>
                                                    - Думаете о том, как сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество!<br>
                                                    - Обращайтесь в нашу компанию, мы поможем.<br>
                                                    - Звоните прямо сейчас
                                                </p>

                                                <p id="textVoice31" class="textVoice">
                                                    - Хотите поменять проводку?<br>
                                                    - Думаете о том, как сделать все своими силами?<br>
                                                    - Не мучьте себя.<br>
                                                    - Доверьте эту работу профессионалам.<br>
                                                    - Звоните сегодня
                                                </p>
                                                <p id="textVoice32" class="textVoice">
                                                    - Хотите поменять проводку?<br>
                                                    - Хотите сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество.<br>
                                                    - Обращайтесь в нашу компанию, мы поможем.<br>
                                                    - Звоните сейчас!
                                                </p>
                                                <p id="textVoice33" class="textVoice">
                                                    - Хотите поменять проводку?<br>
                                                    - Хотите все сделать сами?<br>
                                                    - Не усложняйте себе жизнь!<br>
                                                    - Обратитесь в профессиональную компанию, звоните прямо сейчас!
                                                </p>
                                                <p id="textVoice34" class="textVoice">
                                                    - Хотите поменять проводку?<br>
                                                    - Думаете о том, как сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество.<br>
                                                    - Обращайтесь в нашу компанию, мы поможем.<br>
                                                    - Звоните прямо сейчас!
                                                </p>

                                                <p id="textVoice143" class="textVoice">
                                                    - Задумали ремонт?<br>
                                                    - Думаете о том, как сделать все своими силами?<br>
                                                    - Не мучьте себя! Доверьте эту работу профессионалам.<br>
                                                    - Звоните сегодня!
                                                </p>
                                                <p id="textVoice144" class="textVoice">
                                                    - Задумали ремонт?<br>
                                                    - Хотите сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество, обращайтесь в нашу компанию, мы поможем, звоните сейчас!
                                                </p>
                                                <p id="textVoice145" class="textVoice">
                                                    - Задумали ремонт?<br>
                                                    - Хотите все сделать сами?<br>
                                                    - Не усложняйте себе жизнь, обратитесь в профессиональную компанию, звоните прямо сейчас
                                                </p>
                                                <p id="textVoice146" class="textVoice">
                                                    - Задумали ремонт?<br>
                                                    - Думаете о том, как сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество, обращайтесь в нашу компанию, мы поможем, звоните прямо сейчас!
                                                </p>
                                            </div>
										@endif

									</div>

								</div>

								<div class="tab notAnimate" id="tab2" data-id-tab="2">

										<h3 class="mb-4 mbr-fonts-style mbr-section-title display-6">Заполнение основных полей с данными</h3>

									<div class="form-group">
										<label>Промо:</label>
										<input type="hidden" name="movie_{{$data['template_movie_id']}}[first_color]">
										<div class="input-colorpicker" id="color"></div>
										<span class="error"></span>
										<input type="text" data-validate="required|max:40" name="movie_{{$data['template_movie_id']}}[first]" class="first form-control" value="" />
									</div>

									<div class="form-group">
										<label>Интригующий вопрос:</label>
										<input type="hidden" name="movie_{{$data['template_movie_id']}}[question_color]">
										<div class="input-colorpicker" id="color"></div>
										<span class="error"></span>
										<input type="text" data-validate="required|max:40" name="movie_{{$data['template_movie_id']}}[question]" class="question form-control" value="" />
									</div>

									<div class="form-group">
										<label>Ответ:</label>
										<input type="hidden" name="movie_{{$data['template_movie_id']}}[answer_color]">
										<div class="input-colorpicker" id="color"></div>
										<span class="error"></span>
										<input type="text" data-validate="required|max:40" name="movie_{{$data['template_movie_id']}}[answer]" class="answer form-control" value="" />
									</div>

									<div class="form-group">
										<label>Название компании:</label>
										<input type="hidden" name="movie_{{$data['template_movie_id']}}[company][name_color]">
										<div class="input-colorpicker" id="color"></div>
										<span class="error"></span>
										<input type="text" data-validate="required|max:40" name="movie_{{$data['template_movie_id']}}[company][name]" class="company_name form-control" value="" />
									</div>

									<div class="form-group">
										<label>Сайт:</label>
										<input type="hidden" name="movie_{{$data['template_movie_id']}}[company][site_color]">
										<div class="input-colorpicker" id="color"></div>
										<span class="error"></span>
										<input type="text" data-validate="required|max:60" name="movie_{{$data['template_movie_id']}}[company][site]" class="company_site form-control" value="" />
									</div>

									<div class="form-group">
										<label>Телефон:</label>
										<input type="hidden" name="movie_{{$data['template_movie_id']}}[company][phone_color]">
										<div class="input-colorpicker" id="color"></div>
										<span class="error"></span>
										<input type="text" data-validate="required|max:25" name="movie_{{$data['template_movie_id']}}[company][phone]" class="company_phone form-control" value="" />
									</div>

									<div class="form-group">
										<label>Призыв к действию:</label>
										<input type="hidden" name="movie_{{$data['template_movie_id']}}[cta_color]">
										<div class="input-colorpicker" id="color"></div>
										<span class="error"></span>
										<input type="text" data-validate="required|max:60" name="movie_{{$data['template_movie_id']}}[cta]" class="cta form-control" value="" />
									</div>

								</div>

								<div class="tab notAnimate" id="tab3" data-id-tab="3">

									<h3 class="mb-4 mbr-fonts-style mbr-section-title display-6">Загрузка лого вашей компании</h3>

                                    <div class="form-group form_upload_photo">
                                        <div class="" id="holder_block_photo_film">
                                            <span class="string_error_field">@if ($errors->has('imgBase64')) {{ $errors->first('imgBase64') }} @endif</span>
                                            <span class="user_path_to_photo"></span>
                                            {!! Form::label('photo', 'Загрузите лого компании', ['class'=>'label_btn_upload_photo btn btn-primary']) !!}
                                            <input type="file" name="movie_{{$data['template_movie_id']}}[company][logo]" id="photo" class="hidden hidden_image_64" accept="image/*" />
                                            {!! Form::hidden('imgBase64', old('imgBase64'), ['class'=>'hidden ', 'id'=>'imgBase64']) !!}
                                            <a id="change_result_cropped" class="btn btn-primary">Изменить позицию</a>
                                        </div>
                                    </div>

                                    @if($price > 0)
                                        <div class="form-group hiddden" id="formPayment">
                                            <h3 class="mb-4 mbr-fonts-style mbr-section-title display-6">Выберите способ оплаты</h3>
                                            <label for="checkedPaypal">PayPal</label>
                                            <input type="radio" id="checkedPaypal" name="paymentVariant" value="1">
                                            <br>
                                            <label for="checkedYandex">Yandex Money</label>
                                            <input type="radio" id="checkedYandex" name="paymentVariant" value="2">
                                        </div>
                                    @endif

                                    <div class="form-group">
                                        <div class="">
											<span class="error_submit"></span>
                                            {!! Form::submit( $price ? __('Создать фильм ') . $price . '$' : __('Создать фильм '), ['class' => 'btn btn-primary hiddden', 'id' => 'submit_create_film']) !!}
                                        </div>
                                    </div>

                                    <div class="media-container-row">
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <div class="block_holder_img_preview_film">
                                                    <img src="" alt="" id="result_cropped">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

								</div>
							</div>


								@break

								@case(11)
									<div class="tabs_box">
										<ul class="tabs_menu">
											<li><a data-href-tab="prev" class="linkToTab"><img src="{{ asset('letmeshare/assets/images/arrowNewRight.png') }}" alt=""></a></li>
											<li class="progressLine">
												<div class="point-progress active" id="progressPoint1"></div>
												<div class="point-progress" id="progressPoint2"></div>
												<div class="point-progress" id="progressPoint3"></div>
												<div class="line-progress active" id="lineprogress1"></div>
												<div class="line-progress" id="lineprogress2"></div>
												<div class="line-progress" id="lineprogress3"></div>
											</li>
											<li><a data-href-tab="next" class="linkToTab"><img src="{{ asset('letmeshare/assets/images/arrowNewLeft.png') }}" alt=""></a></li>
										</ul>
										<div class="tab active notAnimate" id="tab1" data-id-tab="1">

											<h3 class="mb-4 mbr-fonts-style mbr-section-title display-6">Выбор музыки и озвучки</h3>

                                            <div class="form-group ">
                                                <label for="noVoiceFilm">Без озвучки</label>
                                                <input type="checkbox" id="noVoiceFilm" name="noVoice" value="1">
                                                <br>
                                                <label for="noMusicFilm">Без музыки</label>
                                                <input type="checkbox" id="noMusicFilm" name="noMusic" value="1">
                                            </div>

										<!-- Background music selector -->
										<div class="form-group">
											<label  id="chooseMusic">Выберите фоновую музыку</label>
											<select id="select_music" name="movie_{{$data['template_movie_id']}}[bg_music_id]" class="bg_music_id form-control">
												<option value="1" data-music-href="http://sag.devbrother.com/storage/ElectroFuturisticLogoIntroNoMelody.wav">Electro Futuristic</option>
												<option value="2" data-music-href="http://sag.devbrother.com/storage/Fun-30sec_2.wav">Fun</option>
												<option value="3" data-music-href="http://sag.devbrother.com/storage/Comedy.wav">Comedy</option>
												<option value="4" data-music-href="http://sag.devbrother.com/storage/Quirky.wav">Quirky</option>
												<option value="6" data-music-href="http://sag.devbrother.com/storage/Quirky_second_version_30.wav">Quirky second version</option>
											</select>

											<audio id="player_music" controls="controls">
												<source id="mp3_src_music" src="http://sag.devbrother.com/storage/ElectroFuturisticLogoIntroNoMelody.wav" type="audio/mp3" />Your browser does not support the audio element.
											</audio>

                                            <select id="select_audio" name="movie_{{$data['template_movie_id']}}[bg_audio_id]" class="bg_music_id form-control">
                                                <option value="39" data-music-href="http://sag.devbrother.com/storage/audiotags/4/nadoelamebel/1.wav">1 Надоела мебель</option>
                                                <option value="40" data-music-href="http://sag.devbrother.com/storage/audiotags/4/nadoelamebel/2.wav">2 Надоела мебель</option>
                                                <option value="41" data-music-href="http://sag.devbrother.com/storage/audiotags/4/nadoelamebel/3.wav">3 Надоела мебель</option>
                                                <option value="42" data-music-href="http://sag.devbrother.com/storage/audiotags/4/nadoelamebel/4.wav">4 Надоела мебель</option>
                                            </select>

                                            <audio id="player_voice" controls="controls">
                                                <source id="mp3_src_voice" src="http://sag.devbrother.com/storage/audiotags/4/nadoelamebel/1.wav" type="audio/mp3" />Your browser does not support the audio element.
                                            </audio>

                                            <div class="voice-text">
                                                <p id="textVoice39" class="textVoice">
                                                    - Надоела старая мебель?<br>
                                                    - Думаете о том, как сделать все своими силами?<br>
                                                    - Не мучьте себя!<br>
                                                    - Доверьте эту работу профессионалам.<br>
                                                    - Звоните сегодня!
                                                </p>
                                                <p id="textVoice40" class="textVoice">
                                                    - Надоела старая мебель?<br>
                                                    - Хотите сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество, обращайтесь в нашу компанию, мы поможем, звоните сейчас!
                                                </p>
                                                <p id="textVoice41" class="textVoice">
                                                    - Надоела старая мебель?<br>
                                                    - Хотите все сделать сами?<br>
                                                    - Не усложняйте себе жизнь, обратитесь в профессиональную компанию, звоните прямо сейчас
                                                </p>
                                                <p id="textVoice42" class="textVoice">
                                                    - Надоела старая мебель?<br>
                                                    - Думаете о том, как сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество, обращайтесь в нашу компанию, мы поможем, звоните прямо сейчас!
                                                </p>
                                            </div>


										</div>
									</div>

									<div class="tab notAnimate" id="tab2" data-id-tab="2">

										<h3 class="mb-4 mbr-fonts-style mbr-section-title display-6">Заполнение основных полей с данными</h3>

										<div class="form-group">
											<label>Название компании:</label>
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[company][name_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:40" name="movie_{{$data['template_movie_id']}}[company][name]" class="company_name form-control" value="" />
										</div>

										<div class="form-group">
											<label>Сфера деятельности:</label>
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[company][sphere_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:60" name="movie_{{$data['template_movie_id']}}[company][sphere]" class="company_sphere form-control" value="" />
										</div>

										<div class="form-group">
											<label>Сайт:</label>
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[company][site_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:60" name="movie_{{$data['template_movie_id']}}[company][site]" class="company_site form-control" value="" />
										</div>

										<div class="form-group">
											<label>Телефон:</label>
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[company][phone_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:25" name="movie_{{$data['template_movie_id']}}[company][phone]" class="company_phone form-control" value="" />
										</div>

										<div class="form-group">
											<label>Призыв к действию:</label>
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[cta_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:60" name="movie_{{$data['template_movie_id']}}[cta]" class="cta form-control" value="" />
										</div>

									</div>

									<div class="tab notAnimate" id="tab3" data-id-tab="3">

										<h3 class="mb-4 mbr-fonts-style mbr-section-title display-6">Загрузка лого вашей компании</h3>
										<div class="form-group form_upload_photo">
											<div class="" id="holder_block_photo_film">
												<span class="string_error_field">@if ($errors->has('imgBase64')) {{ $errors->first('imgBase64') }} @endif</span>
												<span class="user_path_to_photo"></span>
												{!! Form::label('photo', 'Загрузите лого компании', ['class'=>'label_btn_upload_photo btn btn-primary']) !!}
												<input type="file" name="movie_{{$data['template_movie_id']}}[company][logo]" id="photo" class="hidden hidden_image_64" accept="image/*" />
												{!! Form::hidden('imgBase64', old('imgBase64'), ['class'=>'hidden ', 'id'=>'imgBase64']) !!}
												<a id="change_result_cropped" class="btn btn-primary">Изменить позицию</a>
											</div>
										</div>

                                        @if($price > 0)
                                            <div class="form-group hiddden" id="formPayment">
                                                <h3 class="mb-4 mbr-fonts-style mbr-section-title display-6">Выберите способ оплаты</h3>
                                                <label for="checkedPaypal">PayPal</label>
                                                <input type="radio" id="checkedPaypal" name="paymentVariant" value="1">
                                                <br>
                                                <label for="checkedYandex">Yandex Money</label>
                                                <input type="radio" id="checkedYandex" name="paymentVariant" value="2">
                                            </div>
                                        @endif

                                        <div class="form-group">
                                            <div class="">
												<span class="error_submit"></span>
                                                {!! Form::submit( $price ? __('Создать фильм ') . $price . '$' : __('Создать фильм '), ['class' => 'btn btn-primary hiddden', 'id' => 'submit_create_film']) !!}
                                            </div>
                                        </div>

                                        <div class="media-container-row">
                                            <div class="col-md-12">
                                                <div class="col-md-12">
                                                    <div class="block_holder_img_preview_film">
                                                        <img src="" alt="" id="result_cropped">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

									</div>
								</div>


								@break


								@case(12)
										<div class="tabs_box">
											<ul class="tabs_menu">
												<li><a data-href-tab="prev" class="linkToTab"><img src="{{ asset('letmeshare/assets/images/arrowNewRight.png') }}" alt=""></a></li>
												<li class="progressLine">
													<div class="point-progress active" id="progressPoint1"></div>
													<div class="point-progress" id="progressPoint2"></div>
													<div class="point-progress" id="progressPoint3"></div>
													<div class="line-progress active" id="lineprogress1"></div>
													<div class="line-progress" id="lineprogress2"></div>
													<div class="line-progress" id="lineprogress3"></div>
												</li>
												<li><a data-href-tab="next" class="linkToTab"><img src="{{ asset('letmeshare/assets/images/arrowNewLeft.png') }}" alt=""></a></li>
											</ul>
											<div class="tab active notAnimate" id="tab1" data-id-tab="1">

												<h3 class="mb-4 mbr-fonts-style mbr-section-title display-6">Выбор музыки и озвучки</h3>

                                                <div class="form-group ">
                                                    <label for="noVoiceFilm">Без озвучки</label>
                                                    <input type="checkbox" id="noVoiceFilm" name="noVoice" value="1">
                                                    <br>
                                                    <label for="noMusicFilm">Без музыки</label>
                                                    <input type="checkbox" id="noMusicFilm" name="noMusic" value="1">
                                                </div>


									<!-- Background music selector -->
										<div class="form-group">
											<label id="chooseMusic">Выберите фоновую музыку</label>
											<select id="select_music" name="movie_{{$data['template_movie_id']}}[bg_music_id]" class="bg_music_id form-control">
												<option value="1" data-music-href="http://sag.devbrother.com/storage/CartoonsAdventureLoopedVersion2.wav">Cartoons Adventure</option>
												<option value="2" data-music-href="http://sag.devbrother.com/storage/NoSopranoSax.wav">No Soprano Sax</option>
												<option value="5" data-music-href="http://sag.devbrother.com/storage/Quirky_second_version.wav">Quirky second version</option>
												<option value="9" data-music-href="http://sag.devbrother.com/storage/FunnyOrchestral.wav">Funny Orchestral</option>
												<option value="10" data-music-href="http://sag.devbrother.com/storage/CartoonsAdventureUnderscore.wav">Cartoons Adventure Underscore</option>
												<option value="11" data-music-href="http://sag.devbrother.com/storage/Comedy_60.wav">Comedy</option>
											</select>
                                            <audio id="player_music" controls="controls">
                                                <source id="mp3_src_music" src="http://sag.devbrother.com/storage/ElectroFuturisticLogoIntroNoMelody.wav" type="audio/mp3" />Your browser does not support the audio element.
                                            </audio>
											<br>

											<label id="chooseVoice">Выберите озвучку</label>
											<select id="select_audio" name="movie_{{$data['template_movie_id']}}[bg_audio_id]" class="bg_music_id form-control">
												<option value="39" data-music-href="http://sag.devbrother.com/storage/audiotags/4/nadoelamebel/1.wav">1 Надоела мебель</option>
												<option value="40" data-music-href="http://sag.devbrother.com/storage/audiotags/4/nadoelamebel/2.wav">2 Надоела мебель</option>
												<option value="41" data-music-href="http://sag.devbrother.com/storage/audiotags/4/nadoelamebel/3.wav">3 Надоела мебель</option>
												<option value="42" data-music-href="http://sag.devbrother.com/storage/audiotags/4/nadoelamebel/4.wav">4 Надоела мебель</option>
												<option value="43" data-music-href="http://sag.devbrother.com/storage/audiotags/4/zadumaliremont/1.wav">1 Задумали ремонт</option>
												<option value="44" data-music-href="http://sag.devbrother.com/storage/audiotags/4/zadumaliremont/2.wav">2 Задумали ремонт</option>
												<option value="45" data-music-href="http://sag.devbrother.com/storage/audiotags/4/zadumaliremont/3.wav">3 Задумали ремонт</option>
												<option value="46" data-music-href="http://sag.devbrother.com/storage/audiotags/4/zadumaliremont/4.wav">4 Задумали ремонт</option>
											</select>

                                            <audio id="player_voice" controls="controls">
                                                <source id="mp3_src_voice" src="http://sag.devbrother.com/storage/audiotags/4/nadoelamebel/1.wav" type="audio/mp3" />Your browser does not support the audio element.
                                            </audio>

                                            <div class="voice-text">
                                                <p id="textVoice39" class="textVoice">
                                                    - Надоела старая мебель?<br>
                                                    - Думаете о том, как сделать все своими силами?<br>
                                                    - Не мучьте себя!<br>
                                                    - Доверьте эту работу профессионалам.<br>
                                                    - Звоните сегодня!
                                                </p>
                                                <p id="textVoice40" class="textVoice">
                                                    - Надоела старая мебель?<br>
                                                    - Хотите сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество, обращайтесь в нашу компанию, мы поможем, звоните сейчас!
                                                </p>
                                                <p id="textVoice41" class="textVoice">
                                                    - Надоела старая мебель?<br>
                                                    - Хотите все сделать сами?<br>
                                                    - Не усложняйте себе жизнь, обратитесь в профессиональную компанию, звоните прямо сейчас
                                                </p>
                                                <p id="textVoice42" class="textVoice">
                                                    - Надоела старая мебель?<br>
                                                    - Думаете о том, как сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество, обращайтесь в нашу компанию, мы поможем, звоните прямо сейчас!
                                                </p>

                                                <p id="textVoice43" class="textVoice">
                                                    - Задумали ремонт?<br>
                                                    - Думаете о том, как сделать все своими силами?<br>
                                                    - Не мучьте себя! Доверьте эту работу профессионалам.<br>
                                                    - Звоните сегодня!
                                                </p>
                                                <p id="textVoice44" class="textVoice">
                                                    - Задумали ремонт?<br>
                                                    - Хотите сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество, обращайтесь в нашу компанию, мы поможем, звоните сейчас!
                                                </p>
                                                <p id="textVoice45" class="textVoice">
                                                    - Задумали ремонт?<br>
                                                    - Хотите все сделать сами?<br>
                                                    - Не усложняйте себе жизнь, обратитесь в профессиональную компанию, звоните прямо сейчас
                                                </p>
                                                <p id="textVoice46" class="textVoice">
                                                    - Задумали ремонт?<br>
                                                    - Думаете о том, как сделать все своими силами?<br>
                                                    - Поберегите свои нервы и имущество, обращайтесь в нашу компанию, мы поможем, звоните прямо сейчас!
                                                </p>
                                            </div>


										</div>
									</div>

									<div class="tab notAnimate" id="tab2" data-id-tab="2">

										<h3 class="mb-4 mbr-fonts-style mbr-section-title display-6">Заполнение основных полей с данными</h3>

										<div class="form-group">
											<label>Слоганы:</label>
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[slogan_1_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:50" name="movie_{{$data['template_movie_id']}}[slogan_1]" class="slogan_1 form-control" value="" />
										</div>
										<div class="form-group">
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[slogan_2_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:50" name="movie_{{$data['template_movie_id']}}[slogan_2]" class="slogan_2 form-control" value="" />
										</div>
										<div class="form-group">
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[slogan_3_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:50" name="movie_{{$data['template_movie_id']}}[slogan_3]" class="slogan_3 form-control" value="" />
										</div>
										<div class="form-group">
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[slogan_4_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:50" name="movie_{{$data['template_movie_id']}}[slogan_4]" class="slogan_4 form-control" value="" />
										</div>
										<div class="form-group">
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[slogan_5_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:50" name="movie_{{$data['template_movie_id']}}[slogan_5]" class="slogan_5 form-control" value="" />
										</div>
										<div class="form-group">
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[slogan_6_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:50" name="movie_{{$data['template_movie_id']}}[slogan_6]" class="slogan_6 form-control" value="" />
										</div>
										<div class="form-group">
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[slogan_7_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:50" name="movie_{{$data['template_movie_id']}}[slogan_7]" class="slogan_7 form-control" value="" />
										</div>

										<div class="form-group">
											<label>Преимущества:</label>
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[advantage_1_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:50" name="movie_{{$data['template_movie_id']}}[advantage_1]" class="advantage_1 form-control" value="" />
										</div>
										<div class="form-group">
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[advantage_2_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:50" name="movie_{{$data['template_movie_id']}}[advantage_2]" class="advantage_2 form-control" value="" />
										</div>
										<div class="form-group">
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[advantage_3_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:50" name="movie_{{$data['template_movie_id']}}[advantage_3]" class="advantage_3 form-control" value="" />
										</div>
										<div class="form-group">
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[advantage_4_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:50" name="movie_{{$data['template_movie_id']}}[advantage_4]" class="advantage_4 form-control" value="" />
										</div>
										<div class="form-group">
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[advantage_5_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:50" name="movie_{{$data['template_movie_id']}}[advantage_5]" class="advantage_5 form-control" value="" />
										</div>

										<div class="form-group">
											<label>Название компании:</label>
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[company][name_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:40" name="movie_{{$data['template_movie_id']}}[company][name]" class="company_name form-control" value="" />
										</div>

										<div class="form-group">
											<label>Сфера деятельности:</label>
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[company][sphere_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:60" name="movie_{{$data['template_movie_id']}}[company][sphere]" class="company_sphere form-control" value="" />
										</div>

										<div class="form-group">
											<label>Сайт:</label>
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[company][site_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:60" name="movie_{{$data['template_movie_id']}}[company][site]" class="company_site form-control" value="" />
										</div>

										<div class="form-group">
											<label>Телефон:</label>
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[company][phone_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:25" name="movie_{{$data['template_movie_id']}}[company][phone]" class="company_phone form-control" value="" />
										</div>

										<div class="form-group">
											<label>Призыв к действию:</label>
											<input type="hidden" name="movie_{{$data['template_movie_id']}}[cta_color]">
											<div class="input-colorpicker" id="color"></div>
											<span class="error"></span>
											<input type="text" data-validate="required|max:60" name="movie_{{$data['template_movie_id']}}[cta]" class="cta form-control" value="" />
										</div>
									</div>

									<div class="tab active notAnimate" id="tab3" data-id-tab="3">

										<h3 class="mb-4 mbr-fonts-style mbr-section-title display-6">Загрузка картинок</h3>

										<div class="form-group form_upload_photo">
											<div class="" id="holder_block_photo_film">
												<span class="string_error_field">@if ($errors->has('imgBase64')) {{ $errors->first('imgBase64') }} @endif</span>
												<span class="user_path_to_photo"></span>
												{!! Form::label('photo', 'Загрузите лого компании', ['class'=>'label_btn_upload_photo btn btn-primary']) !!}
												<input type="file" name="movie_{{$data['template_movie_id']}}[company][logo]" id="photo" class="hidden hidden_image_64" accept="image/*" />
												{!! Form::hidden('imgBase64', old('imgBase64'), ['class'=>'hidden ', 'id'=>'imgBase64']) !!}
												<a id="change_result_cropped" class="btn btn-primary">Изменить позицию</a>
											</div>
										</div>

										<div class="form-group form_upload_photo">
											<div class="" id="">
												<span class="user_path_to_photo"></span>
												{!! Form::label('photo1', 'Загрузите рекламное фото 1', ['class'=>'label_btn_upload_photo btn btn-primary']) !!}
												<input type="file" style="display: none" onchange="encodeImagetoBase64(this)" name="movie_{{$data['template_movie_id']}}[photo_1]" id="photo1" class="hidden hidden_image_64 nocroppie" accept="image/*" />
												{!! Form::hidden('photo1', old('photo1'), ['class'=>'hidden  hidden_photo', 'id'=>'']) !!}
											</div>
										</div>


										<div class="form-group form_upload_photo">
											<div class="" id="">
												<span class="user_path_to_photo"></span>
												{!! Form::label('photo2', 'Загрузите рекламное фото 2', ['class'=>'label_btn_upload_photo btn btn-primary']) !!}
												<input type="file" style="display: none" name="movie_{{$data['template_movie_id']}}[photo_2]" onchange="encodeImagetoBase64(this)" id="photo2" class="hidden hidden_image_64 nocroppie" accept="image/*" />
												{!! Form::hidden('photo2', old('photo2'), ['class'=>'hidden  hidden_photo', 'id'=>'']) !!}
											</div>
										</div>


										<div class="form-group form_upload_photo">
											<div class="" id="">
												<span class="user_path_to_photo"></span>
												{!! Form::label('photo3', 'Загрузите рекламное фото 3', ['class'=>'label_btn_upload_photo btn btn-primary']) !!}
												<input type="file" style="display: none" name="movie_{{$data['template_movie_id']}}[photo_3]" onchange="encodeImagetoBase64(this)" id="photo3" class="hidden hidden_image_64 nocroppie" accept="image/*" />
												{!! Form::hidden('photo3', old('photo3'), ['class'=>'hidden hidden_photo', 'id'=>'']) !!}
											</div>
										</div>

                                        @if($price > 0)
                                            <div class="form-group hiddden" id="formPayment">
                                                <h3 class="mb-4 mbr-fonts-style mbr-section-title display-6">Выберите способ оплаты</h3>
                                                <label for="checkedPaypal">PayPal</label>
                                                <input type="radio" id="checkedPaypal" name="paymentVariant" value="1">
                                                <br>
                                                <label for="checkedYandex">Yandex Money</label>
                                                <input type="radio" id="checkedYandex" name="paymentVariant" value="2">
                                            </div>
                                        @endif

                                        <div class="form-group">
                                            <div class="">
												<span class="error_submit"></span>
                                                {!! Form::submit( $price ? __('Создать фильм ') . $price . '$' : __('Создать фильм '), ['class' => 'btn btn-primary hiddden', 'id' => 'submit_create_film']) !!}
                                            </div>
                                        </div>

                                        <div class="media-container-row">
                                            <div class="col-md-12">
                                                <div class="col-md-12">
                                                    <div class="block_holder_img_preview_film">
                                                        <img src="" alt="" id="result_cropped">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

									</div>
								</div>

								@break

								@endswitch

								{!! Form::close() !!}
							</div>

						</div>
					</div>

					</div>

				<div class="video-create-preview notAnimate">
					<h3 class="mb-4 mbr-fonts-style mbr-section-title display-5">Демо видео</h3>
					<iframe width="100%" height="270" frameborder="0" allowfullscreen="1" src="https://youtube.com/embed/{{  $data['link_demo'] }}?rel=0&enablejsapi=1" id="iframe_demo"></iframe>

				</div>

				</div>

		</div>
	</section>

	<div class="modal notAnimate" tabindex="-1" role="dialog" id="modal_crop_img">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div id="preview_image_uploaded_form_create_film"></div>
					<p><strong>Для изменения масшатаба выбранной области</strong> - наведите курсором на изображение и прокрутите колёсиком мыши или используйте <strong>слайдер</strong></p>
					<button type="button" class="btn" data-dismiss="modal">Ok</button>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('javascript')
	<script>
		{{-- @todo letmeshare: refactor this --}}
		var templateMovieCropConfigs = {
			1: {
				'viewport': {
                    'width': 300,
                    'height': 356
				}
			},
			2: {
				'viewport': {
					'width': 300,
					'height': 255
				}
			},
			3: {
				'viewport': {
                    'width': 300,
                    'height': 305
				}
			},
		};

		var lmsGlobals = {
			'croppie_params': {
				'viewport': {
					'width': templateMovieCropConfigs["{{ $data['template_movie_id'] }}"].viewport.width,
					'height': templateMovieCropConfigs["{{ $data['template_movie_id'] }}"].viewport.height
				},
				'boundary': {
					'width': 400,
					'height': 400
				}
			},
		};

		var gmaps_static_api_key = 'AIzaSyBPZDyUWk1bE0Dx2cPHgQwIKsDA5nOWd-Q';

		function autocompletePlaces() {
			var input = document.getElementById('city');
			if (input) {
				var autocomplete = new google.maps.places.Autocomplete(input);

				google.maps.event.addListener(autocomplete, 'place_changed', function () {
					var place = autocomplete.getPlace();
					var lat = place.geometry.location.lat();
					var lng = place.geometry.location.lng();

					var gmap_url = 'https://maps.googleapis.com/maps/api/staticmap?center=' + lat + ',' + lng + '&zoom=10&size=600x500&key=' + gmaps_static_api_key;
					document.getElementById('gmaps_city_image').setAttribute('value', gmap_url);
				});
			}
		}
	</script>
@endsection

@section('stylesheet')
	<style>
		.pac-container {
			top: 303px !important;
		}
		.panel_content {max-width: 100%; margin-left: 3.5rem; margin-right: 3.5rem; width: auto}
		.hit_create_film {font-size: 18px; font-weight: 500; line-height: 22px;}
		.alert-danger ul li {
			font-size:16px;
		}
	</style>
@endsection