@extends('letmeshare.layouts.master', [
	'title' => 'Мои фильмы',
	'scripts' => [
		['letmeshare/assets/masonry/masonry.pkgd.min.js', ''],
		['letmeshare/assets/imagesloaded/imagesloaded.pkgd.min.js', ''],
		['letmeshare/assets/bootstrapcarouselswipe/bootstrap-carousel-swipe.js', ''],
		['letmeshare/assets/vimeoplayer/jquery.mb.vimeo_player.js', 'async defer'],
		['letmeshare/assets/slidervideo/script.js', ''],
		['letmeshare/assets/gallery/player.min.js', ''],
		['letmeshare/assets/gallery/script.js', '']
	],
	'styles' => [
		'letmeshare/assets/gallery/style.css'
	]
])

@section('content')


	{{-- TODO remove comment --}}

	@if(!$movies->isEmpty())
	<section class="mbr-section content5 cid-qYagwMYfsD" id="content5-s">
		<div class="container">
			<div class="media-container-row">
				<div class="title col-12 col-md-8">
					<h2 class="align-center mbr-bold mbr-white pb-3 mbr-fonts-style display-1">
						Мои видео</h2>
					<h3 class="mbr-section-subtitle align-center mbr-light mbr-white pb-3 mbr-fonts-style display-5">Здесь вы можете скачать созданные вами видео</h3>

				</div>
			</div>
		</div>
	</section>


	<section class="mbr-gallery mbr-slider-carousel cid-qYagZ4TEZD" id="gallery1-u">
		<div class="container">
			<div>
				<!-- Filter -->
				<!-- Gallery -->

				<div class="mbr-gallery-row">
					<div class="mbr-gallery-layout-default">
						<div>
							<div>

								@foreach($movies as $key => $movie)
								<div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="{{ $movie->category_name }}">
									@if($key == 0)
									<div class="item-movie-slide active">
										@else
											<div {{--href="#lb-gallery1-u" data-slide-to="{{ $key }}" data-toggle="modal" --}}class="item-movie-slide">
										@endif
										@if ( isset($movie->details->photo) )
											<img src="{{ asset($movie->details->photo) }}" alt="">
											{{--<span class="icon-focus"></span>--}}
										@endif
										<span class="hidden link_youtube_movie" style="display: none" hidden>{{ $movie->link_youtube }}</span>
											{{--<a href="{{ url('movies/' . $movie->id . '/show') }}">
												<span class="icon-focus"></span>
												<span class="mbr-gallery-title mbr-fonts-style display-7">{{ __('Перейти на страницу фильма') }}</span>
											</a>--}}
									</div>
								</div>
								@endforeach

							</div>

						</div>

						<div class="clearfix"></div>
					</div>
				</div>

				<!-- Lightbox -->
				<div data-app-prevent-settings="" class="mbr-slider modal fade carousel slide" tabindex="-1" data-keyboard="true" data-interval="false" id="lb-gallery1-u">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
								<div class="carousel-inner">
									@foreach($movies as $key => $movie)
										@if ($key == 0)
											<div class="carousel-item active">
										@else
											<div class="carousel-item">
										@endif
											@if ( isset($movie->details->photo) )
												<img src="{{ asset($movie->details->photo) }}" alt="">
											@endif
										</div>
									@endforeach
								</div><a class="carousel-control carousel-control-prev" role="button" data-slide="prev" href="#lb-gallery1-u"><span class="mbri-left mbr-iconfont" aria-hidden="true"></span><span class="sr-only">Previous</span></a><a class="carousel-control carousel-control-next" role="button" data-slide="next" href="#lb-gallery1-u"><span class="mbri-right mbr-iconfont" aria-hidden="true"></span><span class="sr-only">Next</span></a><a class="close" href="#" role="button" data-dismiss="modal"><span class="sr-only">Close</span></a></div>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>

	</section>

		@if (isset($first_film))
		<section class="tabs4 cid-qYaky0Dd2w" id="tabs4-z">
			<div class="container">
				<div class="media-container-row mt-5 pt-3">

					<div class="mbr-figure" style="width: 57%;">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/{{ $first_film }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen="" id="iframe-curr-video"></iframe>
					</div>

					<div class="tabs-container">
						<a class="mbr-fonts-style display-7 nav-link link_download_video" id="link_download_video" href="http://ssyoutube.com/watch?v={{$first_film}}" target="_blank">
								Скачать фильм</a>
						<div class="tab-content">
							<div id="tab1" class="tab-pane in active" role="tabpanel">
								<div class="row">
									<div class="col-md-12">
										<p class="mbr-text py-5 mbr-fonts-style display-7">Скопируйте этот код и вставьте в том месте на сайте, где должно появиться видео.
											<br>
											<br>
											&lt;iframe width="560" height="315" src="https://www.youtube.com/embed/<span id="text-youtube-iframe">{{$first_film}}</span>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen&gt;&lt;/iframe&gt;
										</p>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		@endif

	@else

		<section class="mbr-section content5 cid-qYagwMYfsD" id="content5-s">
			<div class="container">
				<div class="media-container-row">
					<div class="title col-12 col-md-8">
						<h3 class="align-center mbr-bold mbr-white pb-3 mbr-fonts-style display-2" style="color: black">Вы не создали еще ни одного фильма!</h3>
						<a class="btn btn-sm btn-primary display-4" href="{{ url('/#create-movie') }}" style="display: block; text-align: center">
							<span class="mbr-iconfont-btn"></span>{{ __('Тут вы можете создать фильм') }}
						</a>

					</div>
				</div>
			</div>
		</section>

	@endif


@endsection
