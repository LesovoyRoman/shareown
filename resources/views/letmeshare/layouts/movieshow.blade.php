@include('letmeshare.partials.header')
@yield('additionalStyle')

<div id="app">
	@yield('content')

	@yield('message')

	@include('letmeshare.partials.testimonials')

	@include('letmeshare.partials.copyright')

	<div id="scrollToTop" class="scrollToTop mbr-arrow-up"><a style="text-align: center;"><i></i></a></div>
	<input name="animation" type="hidden">
</div>

@include('letmeshare.partials.js')

@yield('javascript')

@include('letmeshare.partials.footer')
