@extends('letmeshare.layouts.master', [
	'title' => 'Мои настройки',
	'scripts' => [],
	'styles' => []
])

@section('content')
	@if(Session::has('message'))
		<div class="alert alert-success" role="alert">
			<p>{{ Session::get('message') }}</p>
		</div>
	@endif

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12">
				<div class="card">
					<div class="header_page_login">{{ __('Настройки') }}</div>

					<div class="panel-body panel_form_login">
						{{ Form::model($user, ['method' => 'post', 'action' => ['UserSettingsController@update', $user->id, ], 'id'=>'update_user_settings']) }}
							<div class="form-group">
								{{ Form::label('email', 'E-Mail') }}
								<span class="string_error_field"></span>
								{{ Form::text('email', null, ['class' => 'form-control']) }}
							</div>

							<div class="form-group">
								{{ Form::label('password', 'Новый пароль') }}
								<span class="string_error_field"></span>
								{{ Form::password('password', ['class' => 'form-control']) }}
							</div>

							<div class="form-group">
								{{ Form::label('password_confirmation', 'Подтвердите новый пароль') }}
								<span class="string_error_field"></span>
								{{ Form::password('password_confirmation', ['class' => 'form-control']) }}
							</div>

							<div class="form-group">
								<div class="col-md-12 text-center">
									{{ Form::submit( __('Сохранить'), ['class' => 'btn btn-primary', 'id'=>'submit_update_user_settings']) }}
								</div>
							</div>
						{{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection