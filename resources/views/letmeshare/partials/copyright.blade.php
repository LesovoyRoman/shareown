{{-- TODO remove commented old html --}}

<section once="" class="cid-qYacoCQl4a" id="footer7-l">

	<div class="container">
		<div class="media-container-row align-center mbr-white">
			<div class="row row-links">
				<ul class="foot-menu">

					<li class="foot-menu-item mbr-fonts-style display-7">ООО "ЛимВидео", Москва, ИНН, КПП</li></ul>
			</div>
			<div class="row social-row">
				<div class="social-list align-right pb-2">

					<div class="soc-item">
						<a href="https://facebook.com/limvideo" target="_blank">
							<span class="mbr-iconfont mbr-iconfont-social socicon-facebook socicon"></span>
						</a>
					</div></div>
			</div>
			<div class="row row-copirayt">
				<p class="mbr-text mb-0 mbr-fonts-style mbr-white align-center display-7">
					© Copyright 2017 ЛимВидео - All Rights Reserved. Правила сайта. Политика конфиденциальности.<br>
				</p>
			</div>
		</div>
	</div>
</section>



{{--

<section once="" class="cid-qLwLxtRFp3" id="footer6-1z">
	<div class="container">
		<div class="media-container-row align-center mbr-white">
			<div class="col-12">
				<a href="{{ url('/privacy') }}" class="mbr-white">Политика конфиденциальности</a>
				<p class="mbr-text mb-0 mbr-fonts-style display-7">© Copyright 2018 Let Me Share - Все права защищены.</p>
			</div>
		</div>
	</div>
</section>
--}}
