{{-- TODO remove commented old html --}}

<section class="menu cid-qY9Kh8b5nL" once="menu" id="menu1-0">

	<nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<div class="hamburger">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
		</button>
		<div class="menu-logo">
			<div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="/">
                         <img src="{{ asset('letmeshare/assets/images/logolimvideo-1-128x122.png') }}" alt="Mobirise" title="" style="height: 3.8rem;">
                    </a>
                </span>
				<span class="navbar-caption-wrap"><a class="navbar-caption text-white display-5" href="/">LimVideo.com</a></span>
			</div>
		</div>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">

			@auth

			<div class="navbar-buttons mbr-section-btn">
				<a class="btn btn-sm btn-primary display-4" href="{{ url('/#create-movie') }}">
					<span class="mbri-play mbr-iconfont mbr-iconfont-btn"></span>{{ __('Создать фильм') }}
				</a>
				<a class="btn btn-sm btn-white-outline display-4" href="{{ url('settings') }}">
					<span class="mbri-user mbr-iconfont mbr-iconfont-btn"></span>{{ __('Настройки') }}
				</a>
			</div>

			<ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
				<li class="nav-item">
					<a class="nav-link link text-white display-4" href="/">{{ __('Главная') }}</a>
				</li>
				<li class="nav-item">
					<a class="nav-link link text-white display-4" href="{{ url('movies') }}">{{ __('Мои фильмы') }}</a>
				</li>
				<li class="nav-item">
					<a class="nav-link link text-white display-4" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Выйти') }}</a>
				</li>

				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					@csrf
				</form>
				{{--<li class="nav-item">
					<a class="nav-link link text-white display-4" href="">ENG</a>
				</li>--}}
			</ul>



			@else

			<div class="navbar-buttons mbr-section-btn">

				<a class="btn btn-sm btn-primary display-4"  href="/">
					<span class="mbr-iconfont mbr-iconfont-btn"></span>{{ __('Главная') }}
				</a>
				<a class="btn btn-sm btn-primary display-4"  href="{{ route('register') }}">
					<span class="mbri-play mbr-iconfont mbr-iconfont-btn"></span>{{ __('Регистрация') }}
				</a>
				<a class="btn btn-sm btn-white-outline display-4" href="{{ route('login') }}">
					<span class="mbri-user mbr-iconfont mbr-iconfont-btn"></span>{{ __('Войти') }}
				</a>
			</div>

			@endauth

		</div>
	</nav>
</section>


{{--
 	OLD
--}}


{{--

<section class="menu cid-qLyHvq560F" once="menu" id="menu1-25">
	<nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
		<div class="menu-logo">
			<div class="navbar-brand">
				<span class="navbar-logo">
					<a href="/">
						 <img src="{{ asset('letmeshare/assets/images/logo11-1-282x280.png') }}" alt="LetMeShare" title="" style="height: 3.8rem;">
					</a>
				</span>
				<span class="navbar-caption-wrap"><a class="navbar-caption text-white display-4" href="/">{{ __('Let Me Share') }}</a></span>
			</div>
		</div>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
				<li class="nav-item">
					<a class="nav-link link text-white display-4" href="/">
						{{ __('Главная') }}
					</a>
				</li>

				@auth
					<li class="nav-item">
						<a class="nav-link link text-white display-4" id="scroll-to-create-movie" href="{{ url('/#create-movie') }}">
							{{ __('Создать фильм') }}
						</a>
					</li>

					<li class="nav-item">
						<a class="nav-link link text-white display-4" href="{{ url('movies') }}">
							{{ __('Мои фильмы') }}
						</a>
					</li>

					<li class="nav-item dropdown open">
						<a class="nav-link link text-white dropdown-toggle display-4" href="" data-toggle="dropdown-submenu" aria-expanded="true">
							{{ __('Мой аккаунт') }}
						</a>
						<div class="dropdown-menu">
							<a class="text-white dropdown-item display-4" href="{{ url('settings') }}">
								{{ __('Настройки') }}
							</a>
							<a class="text-white dropdown-item display-4" href="{{ route('logout') }}"
							   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
								{{ __('Выйти') }}
							</a>
						</div>

						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
							@csrf
						</form>
					</li>
				@else
					<li class="nav-item dropdown open">
						<a class="nav-link link text-white dropdown-toggle display-4" href="" data-toggle="dropdown-submenu" aria-expanded="true">
							{{ __('Мой аккаунт') }}
						</a>
						<div class="dropdown-menu">
							<a class="text-white dropdown-item display-4" href="{{ route('login') }}">{{ __('Войти') }}</a>
							<a class="text-white dropdown-item display-4" href="{{ route('register') }}">{{ __('Зарегистрироваться') }}</a>
						</div>
					</li>
				@endauth
			</ul>
		</div>

		<div id="btn_modal_mobile_menu">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_mobile_menu"></button>
		</div>

		<div class="modal" id="modal_mobile_menu">
			<div class="modal-dialog">
				<div class="modal-content">
					<!-- Modal body -->
					<button type="button" class="close close_modal_mobile_menu" data-dismiss="modal">&times;</button>
					<div class="modal-body">
						<ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true">
							<li class="nav-item">
								<a class="nav-link link text-white display-4" href="/">
									{{ __('Главная') }}
								</a>
							</li>

							@auth
							<li class="nav-item">
								<a class="nav-link link text-white display-4" id="scroll-to-create-movie" href="{{ url('/#create-movie') }}">
									{{ __('Создать фильм') }}
								</a>
							</li>

							<li class="nav-item">
								<a class="nav-link link text-white display-4" href="{{ url('movies') }}">
									{{ __('Мои фильмы') }}
								</a>
							</li>

							<li class="nav-item dropdown open">
								<a class="nav-link link text-white dropdown-toggle display-4" href="" data-toggle="dropdown-submenu" aria-expanded="true">
									{{ __('Мой аккаунт') }}
								</a>
								<div class="dropdown-menu">
									<a class="text-white dropdown-item display-4" href="{{ url('settings') }}">
										{{ __('Настройки') }}
									</a>
									<a class="text-white dropdown-item display-4" href="{{ route('logout') }}"
									   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
										{{ __('Выйти') }}
									</a>
								</div>

								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									@csrf
								</form>
							</li>
							@else
								<li class="nav-item dropdown open">
									<a class="nav-link link text-white dropdown-toggle display-4" href="" data-toggle="dropdown-submenu" aria-expanded="true">
										{{ __('Мой аккаунт') }}
									</a>
									<div class="dropdown-menu">
										<a class="text-white dropdown-item display-4" href="{{ route('login') }}">{{ __('Войти') }}</a>
										<a class="text-white dropdown-item display-4" href="{{ route('register') }}">{{ __('Зарегистрироваться') }}</a>
									</div>
								</li>
								@endauth
						</ul>
					</div>
				</div>
			</div>
		</div>



	</nav>
</section> --}}