{{-- TODO remove commented old html --}}




<section class="testimonials3 cid-qYaaZwqecO" id="testimonials3-f">
    <div class="container">
        <div class="media-container-row">
            <div class="media-content px-3 align-self-center mbr-white py-2">
                <p class="mbr-text testimonial-text mbr-fonts-style display-7">LimVideo - это сервис генерации рекламных видео для разных ниш бизнеса. Всего за 5 минут и $19 вы получите готовый профессионально сделанный рекламный видео ролик с вашим логотипом и контактными данными для размещения на вашем сайте или в рекламе.</p>
                <p class="mbr-author-name pt-4 mb-2 mbr-fonts-style display-7">
                    Создание видео еще никогда не было таким простым</p>
                <p class="mbr-author-desc mbr-fonts-style display-7">Видео подходят для разных ниш бизнеса</p>
            </div>

            <div class="mbr-figure pl-lg-5" style="width: 130%;">
                <img src="{{ asset('letmeshare/assets/images/depositphotos-49367797-l-2015-1162x564.jpg') }}" alt="" title="">
            </div>
        </div>
    </div>
</section>


{{--
<section class="testimonials3 cid-qLww1OCwXw" id="testimonials3-p">
	<div class="container">
		<div class="media-container-row">
			<div class="media-content px-3 align-self-center mbr-white py-2">
				<p class="mbr-text testimonial-text mbr-fonts-style display-7"><span style="font-style: normal;">"LetMeShare.org", "LetMeGive.org" - проекты международной христианской миссии "Каждый дом для Христа"</span></p>
				<p class="mbr-author-name pt-4 mb-2 mbr-fonts-style display-7">
					Евангелие в каждый дом</p>
				<p class="mbr-author-desc mbr-fonts-style display-7">Наша цель - каждый дом для Христа</p>
			</div>

			<div class="mbr-figure pl-lg-5" style="width: 110%;">
				<a href="//letmeshare.org"><img src="{{ asset('letmeshare/assets/images/2018-03-06-photo-4018-1280x512.png') }}" alt="" title=""></a>
			</div>
		</div>
	</div>
</section>--}}
