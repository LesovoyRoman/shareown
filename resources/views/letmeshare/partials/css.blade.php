<!-- Styles -->
<link href="{{ asset('letmeshare/assets/web/assets/mobirise-icons-bold/mobirise-icons-bold.css') }}" rel="stylesheet">
<link href="{{ asset('letmeshare/assets/icons-mind/style.css') }}" rel="stylesheet">
<link href="{{ asset('letmeshare/assets/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('letmeshare/assets/web/assets/mobirise-icons/mobirise-icons.css') }}" rel="stylesheet">
<link href="{{ asset('letmeshare/assets/tether/tether.min.css') }}" rel="stylesheet">
<link href="{{ asset('letmeshare/assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('letmeshare/assets/bootstrap/css/bootstrap-grid.min.css') }}" rel="stylesheet">
<link href="{{ asset('letmeshare/assets/bootstrap/css/bootstrap-reboot.min.css') }}" rel="stylesheet">
<link href="{{ asset('letmeshare/assets/soundcloud-plugin/style.css') }}" rel="stylesheet">
<link href="{{ asset('letmeshare/assets/dropdown/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('letmeshare/assets/animatecss/animate.min.css') }}" rel="stylesheet">
<link href="{{ asset('letmeshare/assets/theme/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('letmeshare/assets/mobirise/css/mbr-additional.css') }}" rel="stylesheet">
<link href="{{ asset('letmeshare/assets/userStyles/css/user.css') }}" rel="stylesheet">
<link href="{{ asset('letmeshare/assets/croppie/croppie.css') }}" rel="stylesheet">
<link href="{{ asset('letmeshare/assets/colorpicker/colorpicker/css/colorpicker.css') }}" rel="stylesheet">

@isset($styles)
	@foreach($styles as $style)
		<link href="{{ asset($style) }}" rel="stylesheet">
	@endforeach
@endisset