<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
	<meta name="description" content="@isset($title) {{ $title }} @else {{ __('Let Me Share - Позволь поделиться') }} @endisset">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>
		@isset($title)
			{{ $title }}
		@else
			SAG
		@endisset
	</title>

	{{--<link rel="shortcut icon" href="{{ asset('letmeshare/assets/images/logo-128x134-1.png') }}" type="image/x-icon">--}}

	<link rel="shortcut icon" href="{{ asset('letmeshare/assets/images/logolimvideo-128x122-1.png') }}" type="image/x-icon">

	@include('letmeshare.partials.css')

	@yield('stylesheet')
</head>

<body>