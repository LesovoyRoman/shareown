<!-- Scripts -->
<script src="{{ asset('letmeshare/assets/web/assets/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('letmeshare/assets/moment/moment.min.js') }}"></script>
<script src="{{ asset('letmeshare/assets/popper/popper.min.js') }}"></script>
<script src="{{ asset('letmeshare/assets/tether/tether.min.js') }}"></script>
<script src="{{ asset('letmeshare/assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('letmeshare/assets/mbr-tabs/mbr-tabs.js') }}"></script>
<script src="{{ asset('letmeshare/assets/viewportchecker/jquery.viewportchecker.js') }}"></script>
<script src="{{ asset('letmeshare/assets/playervimeo/vimeo_player.js') }}"></script>
<script src="{{ asset('letmeshare/assets/dropdown/js/script.min.js') }}"></script>
<script src="{{ asset('letmeshare/assets/touchswipe/jquery.touch-swipe.min.js') }}" async></script>
<script src="{{ asset('letmeshare/assets/parallax/jarallax.min.js') }}"></script>
<script src="{{ asset('letmeshare/assets/smoothscroll/smooth-scroll.js') }}"></script>
<script src="{{ asset('letmeshare/assets/theme/js/script.js') }}" async></script>
<script src="{{ asset('letmeshare/assets/croppie/croppie.js') }}"></script>
<script src="{{ asset('letmeshare/assets/gallery/player.min.js') }}"></script>
<script src="{{ asset('letmeshare/assets/gallery/script.js') }}"></script>
<script src="{{ asset('letmeshare/assets/colorpicker/colorpicker/js/colorpicker.js') }}"></script>


<script src="https://cdn.impossible.io/sdk/fx-sdk-latest.js"></script>

@yield('additionalScripts')

@isset($scripts)
	@foreach($scripts as $script)
		{{-- $script[0] - path to internal script --}}
		{{-- $script[1] - script attributes (async, defer etc.) --}}
		<script src="@if(false !== strpos($script[0], 'http://') || false !== strpos($script[0], 'https://')){{ $script[0] }}@else{{ asset($script[0]) }}@endif" {{ $script[1] }}></script>
	@endforeach
@endisset

<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '208868293055364');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=208868293055364&ev=PageView&noscript=1"/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120123432-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-120123432-1');
</script>

<script type="text/javascript" id="cookieinfo" src="//cookieinfoscript.com/js/cookieinfo.min.js"></script>