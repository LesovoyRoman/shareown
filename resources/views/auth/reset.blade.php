@include('letmeshare.partials.header')

<div id="app">
    @include('letmeshare.partials.topbar')

    {{-- TODO remove comment --}}

    <section class="cid-qYaeazZKDw mbr-fullscreen mbr-parallax-background" id="header15-n">
        <div class="mbr-overlay" style="opacity: 0.8; background-color: rgb(118, 118, 118);"></div>

        <div class="container align-right">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="mbr-white col-lg-8 col-md-7 content-container">
                    <h1 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1">
                        {{ __('Сброс пароля') }}</h1>
                    <p class="mbr-text pb-3 mbr-fonts-style display-5">
                        <br><a href="{{ route('password.request') }}">{{ __('Забыли пароль?') }}</a></p>
                </div>
                <div class="col-lg-4 col-md-5">
                    <div class="form-container">
                        <div class="media-container-column" data-form-type="formoid">
                            <div data-form-alert="" hidden="" class="align-center">
                                Thanks for filling out the form!
                            </div>
                            <form class="mbr-form" action="{{ url('password/reset') }}" method="post" data-form-title="Mobirise Form">
                                <input type="hidden"
                                       name="_token"
                                       value="{{ csrf_token() }}">
                                <input type="hidden" name="token" value="{{ $token }}">

                                @csrf
                                <div data-for="email">
                                    <div class="form-group">
                                        <input type="email"
                                               class="form-control"
                                               name="email"
                                               value="{{ old('email') }}">
                                    </div>
                                </div>
                                <div data-for="password">
                                    <div class="form-group">
                                        <input type="password"
                                               class="form-control"
                                               name="password">
                                    </div>
                                </div>

                                <span class="input-group-btn">
                                    <button href="" type="submit" class="btn btn-form btn-primary display-4 btn_form_auth">{{ __('Отправить ссылку на сброс пароля') }}</button></span>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>





    {{--<div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading header_page_login">{{ trans('quickadmin::auth.reset-reset_password') }}</div>
                    <div class="panel-body panel_form_login">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>{{ trans('quickadmin::auth.whoops') }}</strong> {{ trans('quickadmin::auth.some_problems_with_input') }}
                                <br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal"
                              role="form"
                              method="POST"
                              action="{{ url('password/reset') }}">
                            <input type="hidden"
                                   name="_token"
                                   value="{{ csrf_token() }}">
                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="form-group">
                                <label class="control-label">{{ trans('quickadmin::auth.reset-email') }}</label>

                                <div class="">
                                    <input type="email"
                                           class="form-control"
                                           name="email"
                                           value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">{{ trans('quickadmin::auth.reset-password') }}</label>

                                <div class="">
                                    <input type="password"
                                           class="form-control"
                                           name="password">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">{{ trans('quickadmin::auth.reset-confirm_password') }}</label>

                                <div class="">
                                    <input type="password"
                                           class="form-control"
                                           name="password_confirmation">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="">
                                    <button type="submit"
                                            class="btn btn-primary"
                                            style="margin-right: 15px;">
                                        {{ trans('quickadmin::auth.reset-btnreset_password') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}

    @include('letmeshare.partials.copyright')
</div>

@include('letmeshare.partials.js')

@yield('javascript')
@include('letmeshare.partials.footer')
