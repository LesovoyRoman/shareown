@include('letmeshare.partials.header')

<div id="app" style="padding-bottom: 0;">
    @include('letmeshare.partials.topbar')


    <section class="cid-qYaeazZKDw mbr-fullscreen mbr-parallax-background" id="header15-n">
        <div class="mbr-overlay" style="opacity: 0.8; background-color: rgb(118, 118, 118);"></div>

        <div class="container align-right">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="mbr-white col-lg-8 col-md-7 content-container">
                    <h1 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1">
                        Вход</h1>
                    <p class="mbr-text pb-3 mbr-fonts-style display-5">
                        <br><a href="{{ route('password.request') }}">{{ __('Забыли пароль?') }}</a></p>
                </div>
                <div class="col-lg-4 col-md-5">
                    <div class="form-container">
                        <div class="media-container-column" data-form-type="formoid">
                            <div data-form-alert="" hidden="" class="align-center">
                                Thanks for filling out the form!
                            </div>
                            <form class="mbr-form" action="{{ url('login') }}" method="post" data-form-title="Mobirise Form">
                                <input type="hidden"
                                       name="_token"
                                       value="{{ csrf_token() }}">
                                {{--<input type="hidden" name="email" data-form-email="true" value="rq1oe4Df7oZ4QqihtS5LtMVydFUpzmC073WAElCEn41xOOhDKZjwOL/Cyp1h+LM5/4wt363uknBQFC1I1jBTmhg7z/vG3IlMQzToXh1bPmVdfAqxT1FPZQa1J4/Puhl9" data-form-field="Email">--}}
                                @csrf
                                <div data-for="email">
                                    <div class="form-group">
                                        <input type="email" autofocus class="form-control px-3 {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" data-form-field="Email" required id="email-header15-n" value="{{ old('email') }}" placeholder="{{ old('email') ?  old('email') : __('E-Mail') }}">
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div data-for="password">
                                    <div class="form-group">
                                        <input type="password" class="form-control px-3" name="password" data-form-field="password" placeholder="{{ __('Пароль') }}" id="password">
                                    </div>
                                </div>

                                <span class="input-group-btn">
                                    <button href="" type="submit" class="btn btn-form btn-primary display-4 btn_form_auth">Войти</button></span>
                                <span class="gdpr-block "><label><input type="checkbox" name="gdpr" id="gdpr-header15-n" required="" checked>Продолжая, вы соглашаетесь с использованием <a href="privacy.html">правил сайта</a>.</label></span></form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>



   {{-- <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading header_page_login">Логин</div>
                    <div class="panel-body panel_form_login">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>{{ trans('quickadmin::auth.whoops') }}</strong> <p>{{ trans('quickadmin::auth.some_problems_with_input') }}</p>
                                <br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal"
                              role="form"
                              method="POST"
                              action="{{ url('login') }}">
                            <input type="hidden"
                                   name="_token"
                                   value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="control-label">Email</label>

                                <div class="">
                                    <input type="email"
                                           class="form-control"
                                           name="email"
                                           value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Пароль</label>

                                <div class="">
                                    <input type="password"
                                           class="form-control"
                                           name="password">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="">
                                    <label>
                                        <input type="checkbox"
                                               name="remember" style="margin-right: 5px;">Запомнить меня
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div>
                                    <button type="submit"
                                            class="btn btn-primary"
                                            style="margin-right: 40px;">
                                        Вход
                                    </button>

                                    <a class="btn btn-link" href="{{ route('password.request') }}" style="padding: 0;">
                                        {{ __('Забыли пароль?') }}
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    <section class="mbr-section content5 cid-qLyuSVHhOw mbr-parallax-background" id="content5-23">
        <div class="container">
            <div class="media-container-row">
                <div class="title col-12 col-md-8">

                </div>
            </div>
        </div>
    </section>

    @include('letmeshare.partials.copyright')
</div>

@include('letmeshare.partials.js')

@yield('javascript')
@include('letmeshare.partials.footer')
