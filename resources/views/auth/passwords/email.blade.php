@include('letmeshare.partials.header')


<div id="app">
    @include('letmeshare.partials.topbar')

    {{-- TODO remove comment --}}

    <section class="cid-qYaeazZKDw mbr-fullscreen mbr-parallax-background" id="header15-n">
        <div class="mbr-overlay" style="opacity: 0.8; background-color: rgb(118, 118, 118);"></div>

        <div class="container align-right">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="mbr-white col-lg-8 col-md-7 content-container">
                    <h1 class="mbr-section-title mbr-bold pb-3 mbr-fonts-style display-1">
                        {{ __('Сброс пароля') }}</h1>

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                </div>
                <div class="col-lg-4 col-md-5">
                    <div class="form-container">
                        <div class="media-container-column" data-form-type="formoid">
                            <div data-form-alert="" hidden="" class="align-center">
                                Thanks for filling out the form!
                            </div>
                            <form class="mbr-form" action="{{ route('password.email') }}" method="post" data-form-title="Mobirise Form">
                                <input type="hidden"
                                       name="_token"
                                       value="{{ csrf_token() }}">

                                @csrf
                                <div data-for="email">
                                    <div class="form-group">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') ? old('email') :  __('E-Mail') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <span class="input-group-btn">
                                    <button href="" type="submit" class="btn btn-form btn-primary display-4 btn_form_auth">{{ __('Сброс пароля') }}</button></span>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>



   {{-- <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="header_page_login">{{ __('Сброс пароля') }}</div>

                    <div class="panel-body panel_form_login">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf

                            <div class="form-group">
                                <label for="email" class="control-label">{{ __('E-Mail') }}</label>

                                <div class="">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group mb-0">
                                <div class="">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Отправить ссылку на сброс пароля') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    @include('letmeshare.partials.copyright')
</div>

@include('letmeshare.partials.js')

@yield('javascript')
@include('letmeshare.partials.footer')

