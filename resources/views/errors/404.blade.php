
@extends('letmeshare.layouts.master')

@section('message')

    <canvas id='canvas'></canvas>
    {{--<h2 class="header_404">404</h2>--}}
    <p class="not_found_404">Page not found</p>
    <a class="link_to_main_404" href="/">
        <span class="mbri-home mbr-iconfont mbr-iconfont-btn"> </span>{{ __('На главную') }}
    </a>


@stop

@section('additionalStyle')
    <style>
        #testimonials3-p {display: none}
        #app {padding-bottom: 0}
    </style>
@endsection

@section('additionalScripts')
    <script src="{{ asset('letmeshare/assets/404canvas/canvas.js') }}"></script>
@endsection
