@section('page_content')

    @if($users->count() > 0)
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ __('Пользователи') }}</div>
            </div>
            <div class="portlet-body">
                <table id="datatable" class="table table-striped table-hover table-responsive datatable">
                    <thead>
                    <tr>
                        <th>{{ __('#') }}</th>
                        <th>{{ __('Имя') }}</th>
                        <th>{{ __('E-mail') }}</th>
                        <th>{{ __('Роль') }}</th>
                        <th>{{ __('Дата регистрации') }}</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($users as $key => $user)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>
                                @if(!empty($user->name))
                                    {{ $user->name }}
                                @else
                                    {{ __('Пользователь не указал имя') }}
                                @endif
                            </td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->type }}</td>
                            <td>{{ $user->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @else
        {{ 'Не найдено' }}
    @endif

@endsection