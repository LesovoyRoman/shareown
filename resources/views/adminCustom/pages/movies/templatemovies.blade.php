@section('page_content')

    @if ($templatemovies->count())
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ __('Список типов фильмов') }}</div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-hover table-responsive datatable" id="datatable">
                    <thead>
                    <tr>
                        <th>
                            {!! Form::checkbox('delete_all',1,false,['class' => 'mass']) !!}
                        </th>
                        <th>id</th>
                        <th>Название</th>
                        {{--<th>Миниатюра</th>
                        <th>Демо ссылка</th>
                        <th>Цена</th>--}}
                        <th>Категория</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($templatemovies as $row)
                        <tr>
                            <td>
                                {!! Form::checkbox('del-'.$row->id,1,false,['class' => 'single','data-id'=> $row->id]) !!}
                            </td>
                            <td>{{ $row->id }}</td>
                            <td>{{ $row->title }}</td>
                            {{--<td>{{ $row->path_image }}</td>
                            <td>{{ $row->link_demo }}</td>
                            <td>{{ $row->price }}</td>--}}
                            <td>{{ $row->movie_category_name }}</td>
                            <td>
                                <a href="{{ route('templatemoviesedit', ['id' => $row->id]) }}">Редактировать</a>

                                {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => "return confirm('" . 'Уверены?' . "');",  'route' => array('templatemoviesdestroy', $row->id) )) !!}

                                {!! Form::submit('Удалить', array('class' => 'btn btn-xs btn-danger')) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-xs-6">
                        {!! Form::open(['route' => 'templatemoviesmassdelete', 'onsubmit' => "return confirm('" . 'Уверены?' . "');", 'method' => 'post', 'id' => 'massDelete']) !!}
                        <button class="btn btn-danger" id="delete" type="submit">Удалить отмеченное</button>
                        <input type="hidden" id="send" class="input_to_delete" name="toDelete">
                        {!! Form::close() !!}
                    </div>
                    <div class="col-xs-6">
                        <a href="{{ route('templatemoviescreate') }}" class="btn btn-primary btn_create">Создать</a>
                    </div>
                </div>
            </div>
        </div>

    @endif

@endsection
