@section('page_content')
    @if ($usermovies->count())
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ __('Список пользовательских фильмов') }}</div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-hover table-responsive datatable" id="datatable">
                    <thead>
                    <tr>
                        <th>
                            {!! Form::checkbox('delete_all',1,false,['class' => 'mass']) !!}
                        </th>
                        <th>Пользователь</th>
                        <th>id</th>
                        <th>Название</th>
                        <th>YouTube ссылка</th>
                        <th>Дата создания</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($usermovies as $row)
                        <tr>
                            <td>
                                {!! Form::checkbox('del-'.$row->id,1,false,['class' => 'single','data-id'=> $row->id]) !!}
                            </td>
                            <td>{{ isset($row->user->name) ? $row->user->name : '' }}</td>
                            <td>{{ $row->id }}</td>
                            <td>{{ $row->title }}</td>
                            <td>{{ $row->link_youtube }}</td>
                            <td>{{ $row->created_at }}</td>
                            <td>
                                <a href="{{ route('usermoviesedit', ['id' => $row->id]) }}">Редактировать</a>

                                {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => "return confirm('" . 'Уверены?' . "');",  'route' => array('usermoviesdestroy', $row->id) )) !!}
                                {!! Form::submit('Удалить', array('class' => 'btn btn-xs btn-danger')) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-xs-6">
                        {!! Form::open(['route' => 'usermoviesmassDelete', 'onsubmit' => "return confirm('" . 'Уверены?' . "');", 'method' => 'post', 'id' => 'massDelete']) !!}
                        <button class="btn btn-danger" id="delete" type="submit">Удалить отмеченное</button>
                        <input type="hidden" id="send" class="input_to_delete" name="toDelete">
                        {!! Form::close() !!}
                    </div>
                    <div class="col-xs-6">
                        <a href="{{ route('usermoviescreate') }}" class="btn btn-primary btn_create">Создать</a>
                    </div>
                </div>
            </div>
        </div>

    @endif

@endsection
