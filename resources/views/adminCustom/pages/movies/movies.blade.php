@section('page_content')

    @if ($moviecategories->count())
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ __('Список Категорий') }}</div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-hover table-responsive datatable" id="datatable">
                    <thead>
                    <tr>
                        <th>
                            {!! Form::checkbox('delete_all',1,false,['class' => 'mass']) !!}
                        </th>
                        <th>id</th>
                        <th>Название</th>

                        <th>&nbsp;</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($moviecategories as $row)
                        <tr>
                            <td>
                                {!! Form::checkbox('del-'.$row->id,1,false,['class' => 'single','data-id'=> $row->id]) !!}
                            </td>
                            <td>{{ $row->id }}</td>
                            <td>{{ $row->name }}</td>

                            <td>
                                {!! Form::close() !!}

                                <a href="{{ route('moviesedit', ['id' => $row->id]) }}">Редактировать</a>

                                {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => "return confirm('" . 'Уверены?' . "');",  'route' => array('moviesdestroy', $row->id) )) !!}
                                {!! Form::submit('Удалить', array('class' => 'btn btn-xs btn-danger')) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-xs-6">
                        {!! Form::open(['route' => 'moviesmassdelete', 'onsubmit' => "return confirm('" . 'Уверены?' . "');", 'method' => 'post', 'id' => 'massDelete']) !!}
                        <button class="btn btn-danger" id="delete" type="submit">Удалить отмеченное</button>
                        <input type="hidden" id="send" class="input_to_delete" name="toDelete">
                        {!! Form::close() !!}
                    </div>
                    <div class="col-xs-6">
                        <a href="{{ route('moviescreate') }}" class="btn btn-primary btn_create">Создать</a>
                    </div>
                </div>

            </div>
        </div>

    @else
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ __('Список Пуст') }}</div>
            </div>
            <a href="{{ route('moviescreate') }}" class="btn btn-primary btn_create">Создать</a>
        </div>
    @endif

@endsection
