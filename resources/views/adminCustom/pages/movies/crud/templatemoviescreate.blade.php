@section('page_content')

<div class="row">
    <div class="col-sm-10 col-sm-offset-2">
        <h1>{{ __('Создание') }}</h1>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
            </div>
        @endif
    </div>
</div>

{!! Form::open(array('route' => array('templatemoviesstore'), 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}

<div class="form-group">
    {!! Form::label('title', 'Title*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('title', old('title'), array('class'=>'form-control')) !!}

    </div>
</div><div class="form-group">
    {!! Form::label('description', 'Description', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::textarea('description', old('description'), array('class'=>'form-control')) !!}

    </div>
</div><div class="form-group">
    {!! Form::label('path_image', 'Image', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('path_image', old('path_image'), array('class'=>'form-control')) !!}

    </div>
</div><div class="form-group">
    {!! Form::label('link_demo', 'Demo*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('link_demo', old('link_demo'), array('class'=>'form-control')) !!}

    </div>
</div><div class="form-group">
    {!! Form::label('price', 'Price*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('price', old('price'), array('class'=>'form-control')) !!}

    </div>
</div>

<div class="form-group">
    {!! Form::label('movie_category_id', 'Movie category', ['class'=>'col-sm-2 control-label']) !!}
    <div class="col-sm-12">
        @php $count=0 @endphp
        @foreach($movie_categories as $key => $movie_category)
            @php $count++; @endphp
            <div class="col-md-4">
                {!! Form::label('current_category_' . $count, $movie_category, array('class'=>'control-label')) !!}
                {{ Form::checkbox('current_category_' . $count, $key, false, array('id' => 'current_category_'. $key)) }}
            </div>
        @endforeach
        {!! Form::hidden('number_categories', $count) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        {!! Form::submit( 'Применить' , array('class' => 'btn btn-primary')) !!}
    </div>
</div>

{!! Form::close() !!}

@endsection