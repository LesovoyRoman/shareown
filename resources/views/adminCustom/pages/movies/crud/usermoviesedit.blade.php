@section('page_content')

<div class="row">
    <div class="col-sm-10 col-sm-offset-2">
        <h1>{{ __('Редактирование') }}</h1>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
            </div>
        @endif
    </div>
</div>

{!! Form::model($usermovies, array('class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'PATCH', 'route' => array('usermoviesupdate', $usermovies->id ))) !!}

<div class="form-group">
    {!! Form::label('user_id', 'User*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::select('user_id', $user, old('user_id',$usermovies->user_id), array('class'=>'form-control')) !!}

    </div>
</div><div class="form-group">
    {!! Form::label('title', 'Title*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('title', old('title',$usermovies->title), array('class'=>'form-control')) !!}

    </div>
</div><div class="form-group">
    {!! Form::label('link_youtube', 'YouTube link*', array('class'=>'col-sm-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('link_youtube', old('link_youtube',$usermovies->link_youtube), array('class'=>'form-control')) !!}

    </div>
</div>

<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        {!! Form::submit('Применить', array('class' => 'btn btn-xs btn-primary')) !!}
        {!! link_to_route('admin.usermovies', 'Назад', null, array('class' => 'btn btn-default')) !!}
    </div>
</div>

{!! Form::close() !!}

@endsection