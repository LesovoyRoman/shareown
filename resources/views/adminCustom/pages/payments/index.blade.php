@section('page_content')

    @if ($payments->count())
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ __('Общая сумма продаж: ') }}  {{ $overall_total }}</div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-hover table-responsive datatable" id="datatable">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Пользователь</th>
                        <th>Описание</th>
                        <th>Сумма</th>
                        <th>Статус</th>
                        <th>Дата создания</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($payments as $row)
                        <tr>
                            <td>{{ $row->id }}</td>
                            <td>{{ isset($row->user->name) ? $row->user->name : '' }}</td>
                            <td>{{ $row->description }}</td>
                            <td>{{ $row->total }}</td>
                            <td>{{ $row->status }}</td>
                            <td>{{ $row->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @else
        {{ 'Не найдено' }}
    @endif

@endsection