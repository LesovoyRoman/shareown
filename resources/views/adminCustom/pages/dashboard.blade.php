@section('page_content')
    <style>
        .columnCustom33 {
             width: 33.33333333333333%;
             box-sizing: border-box;
             float: left;
             padding: 10px;
         }
         .columnCustom50 {
              width: 50%;
              box-sizing: border-box;
              float: left;
              padding: 10px;
         }
         .columnCustom16 {
            width: 16.33333333333333%;
            box-sizing: border-box;
            float: left;
            padding: 10px;
         }
    </style>

    <div class="row">
                <div class="col-xl-3 col-sm-6 mb-3">
                  <div class="card text-white bg-primary o-hidden h-100">
                    <div class="card-body admin_card">
                      <div class="card-body-icon">
                        <i class="fas fa-fw fa-film"></i>
                      </div>
                      <div class="mr-5">{{ __('Создано фильмов: ') }}<span class="admin_card_value">{{ $usermoviesCount }}<span></div>
                    </div>

                  </div>
                </div>
                <div class="col-xl-3 col-sm-6 mb-3">
                  <div class="card text-white bg-warning o-hidden h-100">
                    <div class="card-body admin_card">
                      <div class="card-body-icon">
                        <i class="fas fa-fw fa-film"></i>
                      </div>
                      <div class="mr-5">{{ __('Всего темплейт-фильмов: ') }}<span class="admin_card_value">{{ $templatemoviesCount }}<span></div>
                    </div>

                  </div>
                </div>
                <div class="col-xl-3 col-sm-6 mb-3">
                  <div class="card text-white bg-success o-hidden h-100">
                    <div class="card-body admin_card">
                      <div class="card-body-icon">
                        <i class="fas fa-fw fa-shopping-cart"></i>
                      </div>
                      <div class="mr-5">{{ __('Всего оплачено: ') }} <span class="admin_card_value">{{ $overall_total }}$<span></div>
                    </div>

                  </div>
                </div>
                <div class="col-xl-3 col-sm-6 mb-3">
                  <div class="card text-white bg-danger o-hidden h-100">
                    <div class="card-body admin_card">
                      <div class="card-body-icon">
                        <i class="fas fa-fw fa-user"></i>
                      </div>
                      <div class="mr-5">{{ __('Зарегистрировано пользователей: ') }} <span class="admin_card_value">{{ $usersCount }}<span></div>
                    </div>

                  </div>
                </div>
              </div>

@endsection