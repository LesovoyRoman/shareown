@section('javascript')
<script src="{{ asset('letmeshare/assets/web/assets/jquery/jquery.min.js') }}"></script>

<script src="{{ asset('letmeshare/assets/popper/popper.min.js') }}"></script>
<script src="{{ asset('letmeshare/assets/bootstrap/js/bootstrap.min.js') }}"></script>

<script src="{{ asset('adminCustom/dist/assets/javascript/stacked-menu/stacked-menu.min.js') }}"></script>
<script src="{{ asset('adminCustom/dist/assets/javascript/main.min.js') }}"></script>

<script src="{{ asset('adminCustom/dist/assets/javascript/custom.js') }}"></script>
@endsection