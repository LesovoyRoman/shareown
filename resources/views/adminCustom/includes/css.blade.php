@section('css')
<!-- Styles -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">



<link href="{{ asset('letmeshare/assets/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('letmeshare/assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('letmeshare/assets/bootstrap/css/bootstrap-grid.min.css') }}" rel="stylesheet">
<link href="{{ asset('letmeshare/assets/bootstrap/css/bootstrap-reboot.min.css') }}" rel="stylesheet">

<link rel="stylesheet" href="{{ asset('adminCustom/dist/assets/stylesheets/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('adminCustom/dist/assets/stylesheets/custom.css') }}">
<link rel="stylesheet" href="{{ asset('adminCustom/dist/assets/stylesheets/open-iconic/css/open-iconic-bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('adminCustom/dist/assets/stylesheets/additional.css') }}">
<link rel="stylesheet" href="{{ asset('adminCustom/dist/assets/stylesheets/customnew.css') }}">


@endsection