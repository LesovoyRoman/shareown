@section('aside')
<aside class="app-aside">
    <!-- .aside-content -->
    <div class="aside-content">
        <!-- .aside-header -->
        <header class="aside-header d-block d-md-none">
            <!-- .btn-account -->
            <button class="btn-account" type="button" data-toggle="collapse" data-target="#dropdown-aside">
                <div class="user-avatar user-avatar-lg">
                    <img src="{{ asset('adminCustom/dist/assets/images/avatars/profile.jpg') }}" alt="">
                </div>
                <div class="account-icon">
                    <span class="fa fa-caret-down fa-lg"></span>
                </div>
                <div class="account-summary">
                    <p class="account-name">John Doe</p>
                    <p class="account-description">@johndoe</p>
                </div>
            </button>
            <!-- /.btn-account -->

            <!-- .dropdown-aside -->
            <div id="dropdown-aside" class="dropdown-aside collapse">
                <!-- dropdown-items -->
                <div class="pb-3">
                    <a class="dropdown-item" href="#"><span class="dropdown-icon oi oi-person"></span> Profile</a>
                    <a class="dropdown-item" href="#"><span class="dropdown-icon oi oi-account-logout"></span> Logout</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Help Center</a>
                    <a class="dropdown-item" href="#">Ask Forum</a>
                    <a class="dropdown-item" href="#">Keyboard Shortcuts</a>
                </div>
                <!-- /dropdown-items -->
            </div>
            <!-- /.dropdown-aside -->
        </header>
        <!-- /.aside-header -->

        <!-- .aside-menu -->
        <section class="aside-menu has-scrollable">
            <!-- .stacked-menu -->
            <nav id="stacked-menu" class="stacked-menu">
                <!-- .menu -->
                <ul class="menu">
                    <li class="menu-header">Menus</li>
                    <!-- .menu-item -->
                    <li class="menu-item">
                        <!-- .menu-link -->
                        <a href='/admin' class="menu-link" data_nav_link="" id="adminDashboard">
                            <span class="menu-icon oi oi-dashboard"></span>
                            <span class="menu-text">Dashboard</span>
                        </a>
                        <!-- /.menu-link -->
                    </li>
                    <li class="menu-item has-child" style="display: none">
                        <a href="#" class="menu-link">
                            <span class="menu-icon oi oi-puzzle-piece"></span>
                            <span class="menu-text">Components</span>
                            {{--<span class="badge badge-warning">New</span>--}}
                        </a>
                        <!-- child menu -->
                        <ul class="menu">
                            <li class="menu-item">
                                <a href="component-general.html" class="menu-link">General</a>
                            </li>
                            <li class="menu-item">
                                <a href="component-icons.html" class="menu-link">Icons</a>
                            </li>
                            <li class="menu-item">
                                <a href="component-rich-media.html" class="menu-link">Rich Media</a>
                            </li>
                            <li class="menu-item">
                                <a href="component-list-views.html" class="menu-link">List Views</a>
                            </li>
                            <li class="menu-item">
                                <a href="component-sortable-nestable.html" class="menu-link">Sortable & Nestable</a>
                            </li>
                            <li class="menu-item">
                                <a href="component-activity.html" class="menu-link">Activity</a>
                            </li>
                            <li class="menu-item">
                                <a href="component-steps.html" class="menu-link">Steps</a>
                            </li>
                            <li class="menu-item">
                                <a href="component-tasks.html" class="menu-link">Tasks</a>
                            </li>
                            <li class="menu-item">
                                <a href="component-metrics.html" class="menu-link">Metrics</a>
                            </li>
                        </ul>
                        <!-- /child menu -->
                    </li>
                    <!-- /.menu-item -->

                    <li class="menu-item has-child">
                        <a href="#" class="menu-link">
                            <span class="menu-icon oi oi-grid-two-up"></span>
                            <span class="menu-text">Movies</span>
                            {{--<span class="badge badge-warning">New</span>--}}
                        </a>
                        <!-- child menu -->
                        <ul class="menu">
                            <li class="menu-item">
                                <a href="/admin/movies" data_nav_link="/movies" class="menu-link">Categories</a>
                            </li>
                            <li class="menu-item">
                                <a href="/admin/templatemovies" data_nav_link="/templatemovies" class="menu-link">Template Movies</a>
                            </li>
                            <li class="menu-item">
                                <a href="/admin/usermovies" data_nav_link="/usermovies" class="menu-link">User Movies</a>
                            </li>
                        </ul>
                        <!-- /child menu -->
                    </li>

                    <li class="menu-item">
                        <!-- .menu-link -->
                        <a href='/admin/payments' class="menu-link" data_nav_link="/payments">
                            <span class="menu-icon oi oi-dollar"></span>
                            <span class="menu-text">Payments</span>
                        </a>
                        <!-- /.menu-link -->
                    </li>

                    <li class="menu-item">
                        <!-- .menu-link -->
                        <a href='/admin/users' class="menu-link" data_nav_link="/users">
                            <span class="menu-icon oi oi-person"></span>
                            <span class="menu-text">Users</span>
                        </a>
                        <!-- /.menu-link -->
                    </li>

                </ul>
                <!-- /.menu -->
            </nav>
            <!-- /.stacked-menu -->
        </section>
        <!-- /.aside-menu -->
    </div>
    <!-- /.aside-content -->
</aside>
@endsection