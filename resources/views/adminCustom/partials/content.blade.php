@section('content')
@if(isset($view_page_name))
    @include('adminCustom.pages.' . $view_page_name)
@endif
<main class="app-main">
    <div class="wrapper">
        <div class="page">
            <div class="page-inner">
                <div class="page-section">
                    @if(isset($view_page_name))
                        @yield('page_content')
                    @endif
                </div>
            </div>
        </div>
    </div>
</main>
@endsection