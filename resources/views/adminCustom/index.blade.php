<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Dashboard</title>

    @include('adminCustom.includes.css')
    @yield('css')
</head>
<body>
    <div class="app" id="app">
        <!-- .app-header -->
        @include('adminCustom.partials.header')
        @yield('header')
        <!-- /.app-header -->

        <!-- .app-aside -->
        @include('adminCustom.partials.aside')
        @yield('aside')
        <!-- /.app-aside -->

        <!-- .app-main -->
        @include('adminCustom.partials.content')
        @yield('content')
        <!-- /.app-main -->
    </div>

    @include('adminCustom.includes.javascript')
    @yield('javascript')
</body>
</html>
